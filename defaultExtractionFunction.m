%%
%
% Default function that extract from the dataStruct provided an array of single values, 
% one for each signal provided.
% WARNING! There are no strict checks on what is going on... 
%
% March 2013 - Davide Gamba
% 

function out = defaultExtractionFunction(dataStruct, signals)
    out = matlabMonitor.simpleExtractArray(dataStruct, signals);
end
