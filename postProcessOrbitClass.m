classdef postProcessOrbitClass < postProcessClass
    %POSTPROCESSORBITCLASS Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        beamHPositions
        beamVPositions
        beamSValues
        beamSTrasmissions
        
        observablesLabels
        excitatorsLabels
        
        bpmTypes
        correctorTypes
    end
    
    methods
        function obj = postProcessOrbitClass(directoryOrWildcard, startIndex)
            %obj = postProcessOrbitClass(directoryOrWildcard[, startIndex])
            % like postProcessClass, but has some few more calculations
            % done because of different orbitCorrection class respect to
            % linearFeedback.
            %
            
            if nargin == 1
                startIndex = 1;
            end
            
            obj = obj@postProcessClass(directoryOrWildcard, startIndex);
            
            obj.beamHPositions=[obj.dataStructs.lastBeamHPosition];
            obj.beamVPositions=[obj.dataStructs.lastBeamVPosition];
            obj.beamSValues=[obj.dataStructs.lastBeamSValue];
            obj.beamSTrasmissions=[obj.dataStructs.lastBeamSTrasmission];
            
            try
                obj.observablesLabels = obj.dataStructs(1).observablesParameters;
                for i=1:length(obj.observablesLabels)
                    obj.observablesLabels{i} = obj.observablesLabels{i}([1:2,6:13]);
                end
            catch e 
                disp('Impossible to extract observables labels')
                obj.observablesLabels = {};
            end
            try
                obj.excitatorsLabels = obj.dataStructs(1).excitatorsParameters;
                for i=1:length(obj.excitatorsLabels)
                    obj.excitatorsLabels{i} = obj.excitatorsLabels{i}([1:2,4:10]);
                end
            catch e 
                disp('Impossible to extract excitors labels')
                obj.excitatorsLabels = {};
            end
            try
                obj.bpmTypes = obj.dataStructs(1).bpmTypes;
                obj.correctorTypes = obj.dataStructs(1).correctorTypes;
            catch e 
                disp('Impossible to bpms/correctors type')
                obj.bpmTypes = [];
                obj.correctorTypes = [];
            end
        end
        
        function plotApertureConsiderations(obj, indexes)
            %% Consideration on aperture
            if nargin < 2
                indexes=1:length(obj.dataStructs);
            end
            
            subplot(211)
            errorbar(mean(obj.beamSValues(:,indexes),2),std(obj.beamSValues(:,indexes),0,2),'.-')
            xlabel('BPM [a.u.]')
            ylabel('Average current [a.u.]')
            grid
            subplot(212)
            errorbar(mean(obj.beamSTrasmissions(:,indexes),2),std(obj.beamSTrasmissions(:,indexes),0,2),'.-')
            xlabel('BPM [a.u.]')
            ylabel('Average current trasmission [a.u.]')
            grid
        end
        function plotOrbitConsiderations(obj, indexes)
            %% Consideration on orbit
            if nargin < 2
                indexes=1:length(obj.dataStructs);
            end
            
            if min(size(obj.beamHPositions)) ~= 0
                hasHpositions=true;
            else
                hasHpositions=false;
            end
            if min(size(obj.beamVPositions)) ~= 0
                hasVpositions=true;
            else
                hasVpositions=false;
            end
            
            
            if (hasHpositions && hasVpositions)
                subplot(211)
            else
                subplot(111)
            end
            
            if hasHpositions
                errorbar(mean(obj.beamHPositions(:,indexes),2),std(obj.beamHPositions(:,indexes),0,2))
                xlabel('BPM [a.u.]')
                ylabel('Average horizontal position [a.u.]')
                grid
            end
            
            if (hasHpositions && hasVpositions)
                subplot(212)
            end
            
            if hasVpositions
                errorbar(mean(obj.beamVPositions(:,indexes),2),std(obj.beamVPositions(:,indexes),0,2))
                xlabel('BPM [a.u.]')
                ylabel('Average vertical position [a.u.]')
                grid
            end
        end
        
        function plotDispersionConsiderations(obj, indexes)
            %% Consideration on dispersion
            if nargin < 2
                indexes=1:length(obj.dataStructs);
            end
            
            if min(size(obj.beamHPositions)) ~= 0
                hasHpositions=true;
            else
                hasHpositions=false;
            end
            if min(size(obj.beamVPositions)) ~= 0
                hasVpositions=true;
            else
                hasVpositions=false;
            end
            
            if (hasHpositions && hasVpositions)
                nFigCol=2;
            else
                nFigCol=1;
            end
            currentCol=1;
            
            if hasHpositions
                aux=obj.beamHPositions(:,indexes);
                aux=aux-repmat(mean(aux,2),1,size(aux,2));
                subplot(3,nFigCol,(1+(currentCol-1)*3))
                plot(aux,'k:')
                xlabel('BPM [a.u.]')
                title('Horizontal oscillations from mean')
                grid
                subplot(3,nFigCol,(2+(currentCol-1)*3))
                [U S V]=svd(aux);
                plot(diag(S),'x')
                xlabel('Singular value #')
                ylabel('[a.u.]')
                grid
                subplot(3,nFigCol,(3+(currentCol-1)*3))
                plot(U(:,[1,2]))
                xlabel('BPM [a.u.]')
                ylabel('[a.u.]')
                grid
                legend('First mode','Second mode','Location','Best')
            end
            
            if (hasHpositions && hasVpositions)
                currentCol=2;
            end
            
            if hasVpositions
                aux=obj.beamVPositions(:,indexes);
                aux=aux-repmat(mean(aux,2),1,size(aux,2));
                subplot(3,nFigCol,(1+(currentCol-1)*3))
                plot(aux,'k:')
                xlabel('BPM [a.u.]')
                title('Vertical oscillations from mean')
                grid
                subplot(3,nFigCol,(2+(currentCol-1)*3))
                [U S V]=svd(aux);
                plot(diag(S),'x')
                xlabel('Singular value #')
                ylabel('[a.u.]')
                grid
                subplot(3,nFigCol,(3+(currentCol-1)*3))
                plot(U(:,[1,2]))
                xlabel('BPM [a.u.]')
                ylabel('[a.u.]')
                grid
                legend('First mode','Second mode','Location','Best')
            end
        end
        
        function removeData(obj, indexes)
            obj.beamHPositions(:,indexes) = [];
            obj.beamVPositions(:,indexes) = [];
            obj.beamSValues(:,indexes) = [];
            obj.beamSTrasmissions(:,indexes) = [];
            removeData@postProcessClass(obj,indexes);
        end
    end
end

