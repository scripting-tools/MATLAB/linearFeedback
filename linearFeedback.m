classdef linearFeedback < handle
    %
    % This class is intended to ease the pulse-to-pulse linear correction
    % of single pulse machine observables. The approach used is quite
    % general allowing a significant flexibility. The class can be used to
    % compute the response matrix between the observable and the actuators.
    % It allows weighting of the error vectors, limiting of the actuators
    % vectors, improving the respose matrix during correction.
    %
    % Davide Gamba - Guido Sterbini
    % Feb 2013
    % %%%%%%%%%%%%%%%%%%%%
    properties(Constant)
        % if you set randomExcitationStrategy as Gaussian it will generate
        % excitation in a gaussian way, and given gain is the sigma
        % (this is the default strategy)
        randomExcitationStrategyGaussian = 1;
        % if you set randomExcitationStrategy as Square it will generate
        % excitation in a square random way, and given gain is the max abs
        % jitter
        randomExcitationStrategySquare = 2;
        % if you set randomExcitationStrategy as PlusMinus it will generate
        % excitation randomly as -gain 0 +gain
        randomExcitationStrategyPlusMinus = 3;
        % if you set randomExcitationStrategy as GaussianObservation it will
        % generate an excitation whos effect on the observables will be a
        % gaussian with sigma the excitation gain and patterned like the
        % observables weights.
        % This to work will need an initial knowledge of the RM. A
        % completly wrong RM could lead to unpredictable results! Use it
        % with care!
        randomExcitationStrategyGaussianObservation = 4;
        % if you set randomExcitationStrategy as SquareObservation it will
        % be similar to GaussianObservation but square distribution.
        % This to work will need an initial knowledge of the RM. A
        % completly wrong RM could lead to unpredictable results! Use it
        % with care!
        randomExcitationStrategySquareObservation = 5;
        % if you set randomExcitationStrategy as PlusMinusObservation it will
        % try to excite -gain 0 +gain the observables accordingly to their
        % weights.
        % This to work will need an initial knowledge of the RM. A
        % completly wrong RM could lead to unpredictable results! Use it
        % with care!
        randomExcitationStrategyPlusMinusObservation = 6;
    end
    properties (SetObservable)
        % observables limits
        observablesLowerLimits = -Inf;
        observablesUpperLimits = Inf;
        wantedObservation = NaN;
        % weights
        % should be 1 or a vector
        observablesWeights = 1;
        
        % excitator limits
        excitatorsLowerLimits = -Inf;
        excitatorsUpperLimits = Inf;
        wantedExcitation = NaN;
        targetWantedExcitation = false;
        
        % random excitation pattern
        randomExcitationPattern = 1;
        % random excitation strategy (Gaussian, Square, PlusMinus = [1 2 3]...)
        randomExcitationStrategy = linearFeedback.randomExcitationStrategyGaussian;
    end
    properties
        % just what you expect
        verbose=false
        saveData=false
        saveDataPath='.'
        
        % Labels for observables and excitators values
        observablesLabels
        excitatorsLabels
        
        % the constructr will save here the inital status of excitators
        % parameters. This will be used as 'reset condition' to come back
        % to initial values if something goes wrong (i.e. beam lost)
        initialExcitatorsStatusStruct = struct;
        initialExcitatorsStatusArray = [];
        
        % in order to characterize the uncorrected system
        observationsHistorySize = 1;
        observationsHistory = [];
        observationsExcitationHistory = [];
        
        % increase this number if you want to have more consecutive shots
        % in the acquired data structure. This may be useful if for some
        % reason some parameters are updated some shots later or before
        % compared to others... see CTF3 data acquisition system...
        acquisitionBufferSize = 1;
        
        % number of shots to average on (and calculate error)
        nAveragingShots = 1;
        
        
        %% TODO uncomment here to separate enable private variables instead of public
        %end
        % %%%%%%%%%%%%%%%%%%%%
        %properties (Access=protected)
        % cycle used for data acquisition
        cycleName
        
        % signal lists
        observablesParameters
        excitatorsParameters
        excitatorsReadOutParameters
        
        % Function handlers called after an acquisition is done to extract
        % wanted data, or to convert a "response array" in data to set to excitatorsParameters
        %
        observablesExtractionFunction % arguments: dataStructure, signals observablesParameters
        excitatorsSetFillFunction % arguments: dataArray, excitatorsParameters
        excitatorsReadOutExtractionFuntion % arguments: dataStructure, signals excitatorsReadOutParameters
        excitatorsSetExtractionFuntion % arguments: dataStructure, signals excitatorsParameters
        
        % Classes used to do the proper stuff
        myMonitor
        myJapc
        mySolver
        
        % last zero Excitation Set used for matrix improvements when no
        % correction is enabled
        currentZeroExcitationSetForRandomExcitation = [];
        % previous obtained values
        previousObservation = [];
        previousObservationError = [];
        previousExcitationReadOut = [];
        previousExcitationReadOutError = [];
        previousExcitationSetReading = [];
        previousExcitationSetReadingError = [];
        previousExcitationDeltaApplied = [];
        % temporary Values used in coreFunction
        currentTmpDataStruct = struct;
        currentAveragingObservations = [];
        currentAveragingExcitationReadOut = [];
        currentAveragingExcitationSetReading = [];
        currentTmpObservation = [];
        currentTmpObservationError = [];
        currentTmpExcitationReadOut = [];
        currentTmpExcitationReadOutError = [];
        currentTmpExcitationSetReading = [];
        currentTmpExcitationSetReadingError = [];
        currentTmpDeltaObservation = [];
        currentTmpDeltaObservationError = [];
        currentTmpDeltaExcitationReadOut = [];
        currentTmpDeltaExcitationReadOutError = [];
        currentTmpDeltaExcitationSetReading = [];
        currentTmpDeltaExcitationSetReadingError = [];
        
        % Expected orbits
        previousObservationExpected = [];
        previousObservationExpectedError = [];
        futureObservationExpected = [];
        futureObservationExpectedError = [];
        bestObservationExpectedWithLimits = [];
        bestObservationExpectedWithLimitsError = [];
        bestExcitationExpectedWithLimits = [];
        bestObservationExpectedWithoutLimits = [];
        bestObservationExpectedWithoutLimitsError = [];
        bestExcitationExpectedWithoutLimits = [];
        
        %
        nObservables = 0;
        nExcitators = 0;
        
        % gains
        % correction gain. Must be a scalar
        correctionGain = 0;
        % random Excitation Gain
        randomExcitationGain = 0;
        
        % set to true if you always want to improve matrix even if
        % random excitation gain is 0.
        isRMImproveEnabled = false;
        
        % set to true if you want that during correction/excitation the
        % feedback sets back to the previous excitation in case the
        % observation reaches the limits imposed by the user.
        % it might be quite a dangerous option!
        autoResetPreviousEnabled = false;
        
        % Variables used to skip plot/update sometime...
        % to let the dipoles set
        nSkipAfterSet = 5;
        % to let Matlab breath...
        nSkipAlways = 0;
        % control variables
        stillToSkip = 0;
        
        % random pattern scan variables
        randomPatternScanCurrenttIdx = 1;
        randomPatternScanCurrenttEndIdx = 1;
        randomPatternScanAuxDone = 0;
        randomPatternScanNumShotsPerPoint = 1;
        randomPatternScanning = false;
        
    end % private variables
    
    % %%%%%%%%%%%%%%%%%%%%
    methods
        function obj = linearFeedback(cycleName, ...
                observablesParameters, observablesExtractionFunction, ...
                excitatorsParameters, excitatorsSetExtractionFuntion, excitatorsSetFillFunction, ...
                excitatorsReadOutParameters, excitatorsReadOutExtractionFuntion, ...
                initialResponseMatrix, offlineExecution)
            % linearFeedback constructor
            % This is the class constructor. It needs 9 arguments.
            % 1. The cycle name
            % 2. The parameters to monitor.
            % 3. The function handle to convert the monitored parameters in
            % an observable vector. ReadObservable
            % 4. The set properties of the actuators. These properties are
            % readable and writable.
            % 5. ReadSetActuators -> function to read the setting of
            % actuators
            % 6. WriteSetActuators -> function to set the setting of
            % actuators
            % 7. The get properties of the actuators. These properties are
            % only readable.
            % 8. ReadGetActuators -> function to read the acquisition
            % values of the actuators
            % 9. and finally the response matrix that gives the dimensions
            % of the system under consideration.
            % 10. if true (default false), the matlabJAPC and matlabMonitor objects
            % will not be created. This is useful to run your application
            % outside the technical network or where you don't have all the necessary
            % JAPC libraries avaialable (e.g. your personal laptop).
            
            if nargin == 9
                offlineExecution = false;
            end
            
            % cycle name
            obj.cycleName = cycleName;
            % parameters
            obj.observablesParameters = observablesParameters;
            obj.excitatorsParameters = excitatorsParameters;
            obj.excitatorsReadOutParameters = excitatorsReadOutParameters;
            % extraction/fill functions
            obj.observablesExtractionFunction=observablesExtractionFunction;
            obj.excitatorsReadOutExtractionFuntion = excitatorsReadOutExtractionFuntion;
            obj.excitatorsSetFillFunction = excitatorsSetFillFunction;
            obj.excitatorsSetExtractionFuntion = excitatorsSetExtractionFuntion;
            
            % merge read and setReadOut parameters
            paramsToBeMonitored=[obj.observablesParameters(:); ...
                obj.excitatorsReadOutParameters(:); ...
                obj.excitatorsParameters(:)];
            
            % save sizes
            obj.nObservables = size(initialResponseMatrix,1);
            obj.nExcitators = size(initialResponseMatrix,2);
            obj.observablesWeights=ones(obj.nObservables,1);
            obj.randomExcitationPattern=ones(obj.nExcitators,1);
            obj.wantedObservation= zeros(obj.nObservables,1);
            obj.wantedExcitation = zeros(obj.nExcitators,1);
            % set a reasonable history size
            obj.observationsHistorySize = 4*max(obj.nObservables,obj.nExcitators);
            
            % initialize used Matlab Objects
            
            if offlineExecution
                obj.myMonitor = '';
                obj.myJapc = '';
            else
                obj.myMonitor = matlabJapcMonitor(obj.cycleName, paramsToBeMonitored, @(data)obj.coreFunction(data), ...
                    'linearFeedback data');
                obj.myMonitor.synchronizedExternalCalls = true; % only call me if I finished to work, drop otherwise.
                obj.myMonitor.useFastStrategy(1); % use my new custom fastStrategy (but not allowing many pulses per BP)
                
                obj.myJapc = matlabJapc(obj.cycleName);
                
                % save a copy of current excitator status
                obj.initialExcitatorsStatusStruct = obj.myJapc.JGetAll(excitatorsParameters);
                
                % this is only for debug and warning message...
                try
                    obj.initialExcitatorsStatusArray = matlabDataAndSignalsHelper.simpleExtractArray(obj.initialExcitatorsStatusStruct,excitatorsParameters);
                catch e
                    obj.initialExcitatorsStatusArray = [];
                    obj.warningOut(['Impossible to extract initial array of excitors status: ',e.message,'... I'' continue.'])
                end
                if (isempty(obj.initialExcitatorsStatusArray) || ...
                        hasInfNaN(obj.initialExcitatorsStatusArray))
                    obj.warningOut(['Not a good Initial Excitators Status obtained!',...
                        '   !!! It may be unsafe to continue !!!   ']);
                end
            end
            
            % create a solver object
            obj.mySolver = matlabSolver(initialResponseMatrix);
        end
        
        function myNotifyUpdateSettigs(obj,h,e)
            %disp('DEBUG: updateSettings event!');
            obj.notify('updateSettings');
        end
        
        function myNotifyNewBeam(obj,h,e)
            %disp('DEBUG: New BEAM!');
            obj.notify('newBeam');
        end
        
        % Get/Set Gains
        function setRandomExcitationGain(obj, value)
            obj.randomExcitationGain = value;
        end
        function out = getRandomExcitationGain(obj)
            out = obj.randomExcitationGain;
        end
        function setCorrectionGain(obj, value)
            obj.correctionGain = value;
        end
        function out = getCorrectionGain(obj)
            out = obj.correctionGain;
        end
        
        % properly delete objects
        function delete(obj)
            obj.randomExcitationGain=0;
            obj.correctionGain=0;
            obj.stopDataAcquisition;
            obj.mySolver.delete;
            obj.myJapc.delete;
            obj.myMonitor.delete();
        end
        
        % enable|disable debug for Java DataMonitor onject
        function enableDebug(obj)
            %obj.myMonitor.enableDebug();
            obj.myMonitor.verbose=true;
            obj.myJapc.verbose=true;
            obj.mySolver.verbose=true;
            obj.verbose = true;
        end
        function disableDebug(obj)
            %obj.myMonitor.disableDebug();
            obj.myMonitor.verbose=false;
            obj.myJapc.verbose=false;
            obj.mySolver.verbose=false;
            obj.verbose = false;
        end
        
        %% resets some variables and start acquiring data
        function startDataAcquisition(obj)
            obj.currentAveragingObservations = [];
            obj.currentAveragingExcitationReadOut = [];
            obj.currentAveragingExcitationSetReading = [];
            
            obj.previousObservation = [];
            obj.previousExcitationReadOut = [];
            obj.previousExcitationSetReading = [];
            obj.previousObservationError = [];
            obj.previousExcitationReadOutError = [];
            obj.previousExcitationSetReadingError = [];
            obj.previousExcitationDeltaApplied = [];
            obj.currentZeroExcitationSetForRandomExcitation = [];
            
            obj.previousObservationExpected = [];
            obj.previousObservationExpectedError = [];
            obj.futureObservationExpected = [];
            obj.futureObservationExpectedError = [];
            obj.bestObservationExpectedWithLimits = [];
            obj.bestExcitationExpectedWithLimits = [];
            obj.bestObservationExpectedWithoutLimits = [];
            obj.bestExcitationExpectedWithoutLimits = [];
            
            % just to be save.
            obj.autoResetPreviousEnabled = false;
            
            try
                obj.myMonitor.start(obj.acquisitionBufferSize);
            catch e
                obj.errorOut(['Impossible to start monitoring: ',e.message])
            end
        end
        
        %% just stops data acquisition
        function stopDataAcquisition(obj)
            obj.myMonitor.stop();
        end
        
        %% says if my java monitor is acquiring data
        function out = isAcquiringData(obj)
            try
                out = obj.myMonitor.isAcquiringData();
            catch e
                out = false;
                obj.printOut(['Some issue trying to know if you are acquiring data: ', e.message]);
            end
        end
        
        %% reset initial excitators status
        function resetInitialExcitatorStatus(obj)
            obj.randomExcitationGain = 0;
            obj.correctionGain = 0;
            obj.previousObservation = [];
            obj.previousExcitationReadOut = [];
            obj.previousExcitationSetReading = [];
            obj.previousExcitationDeltaApplied = [];
            
            obj.currentAveragingObservations = [];
            obj.currentAveragingExcitationReadOut = [];
            obj.currentAveragingExcitationSetReading = [];
            
            obj.myJapc.JSetAll(obj.excitatorsParameters, obj.initialExcitatorsStatusStruct);
        end
        
        function resetPreviousExcitatorStatus(obj)
            %resetPreviousExcitatorStatus(obj)
            % reset previous excitators status, if available.
            % This method doesn't stop the gains a priori!

            if ~isempty(obj.previousExcitationSetReading)
                obj.sendExcitationToHardware(obj.previousExcitationSetReading);
                
                obj.previousObservation = [];
                obj.previousExcitationReadOut = [];
                obj.previousExcitationSetReading = [];
                obj.previousExcitationDeltaApplied = [];
                
                obj.currentAveragingObservations = [];
                obj.currentAveragingExcitationReadOut = [];
                obj.currentAveragingExcitationSetReading = [];
            else
                obj.errorOut('Impossible to set previous excitation: empty vector. Stop any action.')
                obj.randomExcitationGain = 0;
                obj.correctionGain = 0;
            end
        end
        
        function varargout = getCurrentResponseMatrix(obj)
            % [RM, RMError] = getCurrentResponseMatrix(obj)
            % It returns the response matrix currently stored in the solver
            % object. This is the matrix will be in use in case of a
            % correction.
            % If only one argument is specified, only the RM is given.
            
            varargout{1} = obj.mySolver.responseMatrix;
            if nargout == 2
                varargout{2} = obj.mySolver.responseMatrixErrors;
            end
        end
        
        % random pattern scan functions
        function startScanRandomPattern(obj, nshotsPerPoint, startIdx, stopIdx)
            %startScanRandomPattern(obj, nshotsPerPoint, startIdx, stopIdx)
            % it starts a scan of random excitation pattern...
            
            if nargin == 1
                nshotsPerPoint = 1;
                startIdx = 1;
                stopIdx = obj.nExcitators;
            elseif nargin == 2
                startIdx = 1;
                stopIdx = obj.nExcitators;
            elseif nargin == 3
                stopIdx = obj.nExcitators;
            end
            
            obj.randomPatternScanCurrenttIdx = startIdx;
            obj.randomPatternScanCurrenttEndIdx = stopIdx;
            obj.randomPatternScanAuxDone = 0;
            obj.randomPatternScanNumShotsPerPoint = nshotsPerPoint;
            obj.randomPatternScanning = true;
            
            % set it to pattern
            obj.randomExcitationPattern(:) = 0;
            obj.randomExcitationPattern(obj.randomPatternScanCurrenttIdx) = 1;
            obj.randomPatternScanAuxDone = 0;
        end
        function stopScanRandomPattern(obj)
            obj.randomPatternScanning = false;
        end
        
    end % end public methods
    
    % %%%%%%%%%%%%%%%%
    methods %(Access=protected)
        
        % function called by java DataMonitor object.
        function coreFunction(obj, dataStruct)
            % check if this shot has to be skept. If so, just return.
            if obj.checkIfThisShotHasToBeSkept(dataStruct)
                return;
            end
            
            % try to extract new vectors of values from acquired data
            try
                obj.extractObservedObservationAndExcitation(dataStruct);
            catch exception
                obj.errorOut(exception.message);
                return;
            end
            % if enough data have been collected proceed in averaging it
            if size(obj.currentAveragingObservations,2) >= obj.nAveragingShots
                obj.collectedDataAveragingFunction();
            else
                obj.printOut(['Still didn''t get all the desired shots to average on... (',...
                    num2str(size(obj.currentAveragingObservations,2)),'/',...
                    num2str(obj.nAveragingShots),')']);
                return
            end
            
            % check if we are inside user defined hard limits for the
            % observation
            if (~obj.isInsideLimits(obj.currentTmpObservation))
                if obj.autoResetPreviousEnabled
                    if obj.correctionGain > 0 || obj.randomExcitationGain > 0
                        obj.resetPreviousExcitatorStatus();
                    else
                        % disable this dangerous option.
                        obj.autoResetPreviousEnabled = false;
                    end
                else
                    obj.errorOut('Beam outside defined limits. Stop any action! Operator intervention required!')
                    obj.correctionGain = 0;
                    obj.randomExcitationGain = 0;
                end
            end
            
            % compute deltas with previous observation
            obj.computeDeltas();
            
            % at this point we have those private tmp vectors updated:
            % obj.currentTmpObservation -> contains the current observation
            % obj.currentTmpExcitationReadOut -> contains the excitation related to this observation
            % obj.currentTmpExcitationSetReading -> contains the excitation SET related to this observation
            % obj.currentTmpDeltaObservation -> difference between this one and last observation saved in memory
            % obj.currentTmpDeltaExcitationReadOut -> difference between this one and last excitation saved in memory
            % obj.currentTmpDeltaExcitationSetReading -> same as before, but difference between "set", not acq.
            % and all the relative errors (normally calculated only if
            % averaging over many pulses)
            
            % use previous data to improve response matrix
            obj.improveRMIfRequested();
            
            % prepare delta excitation to apply
            randomExcitationToApply = obj.computeRandomExcitation();
            correctionExcitationToApply = obj.computeGainedCorrectionExcitationAndExpectations();
            
            % define zero point where to add this correction
            auxBaseLevelExcitation = obj.currentTmpExcitationSetReading;
            if (sum(abs(randomExcitationToApply)) > 0 && ...
                    sum(abs(correctionExcitationToApply)) == 0)
                if isempty(obj.currentZeroExcitationSetForRandomExcitation)
                    obj.currentZeroExcitationSetForRandomExcitation = obj.currentTmpExcitationSetReading;
                end
                auxBaseLevelExcitation = obj.currentZeroExcitationSetForRandomExcitation;
            end
            if (obj.randomExcitationGain <= 0)
                obj.currentZeroExcitationSetForRandomExcitation = [];
            end
            
            % total delta to apply
            deltaExcitationToApply = randomExcitationToApply + correctionExcitationToApply;
            % assemble total excitation to apply
            totalExcitationToApply = auxBaseLevelExcitation + deltaExcitationToApply;
            
            % final check that total Excitation is inside limits
            totalExcitationToApply = obj.constrainExcitationInsideLimits(totalExcitationToApply);
            
            %
            % send correction to the hardware (if necessary)
            %
            if norm(deltaExcitationToApply) > 1000*eps*obj.nExcitators
                % we got some Excitation to apply for some reason (training/correcting)
                obj.sendExcitationToHardware(totalExcitationToApply);
                
                % skip some data if needed later...
                obj.stillToSkip = obj.nSkipAfterSet;
            end
            % and in order to skip some data and let Matlab breath
            obj.stillToSkip = obj.stillToSkip+obj.nSkipAlways;
            
            % if randomPatternScanning is enabled, and random excitation,
            % see if we need to update the actuator index to excite...
            obj.updateRandomScanningIndexIfRequired();
            
            % fill expectations
            obj.fillFutureExpectations(deltaExcitationToApply);
            
            % fill history
            tmphistsize = size(obj.observationsHistory, 2);
            if tmphistsize ~= size(obj.observationsExcitationHistory, 2)
                obj.warningOut('Something is worong whit histories sizes')
                obj.observationsHistory = obj.currentTmpObservation(:);
                obj.observationsExcitationHistory = obj.currentTmpExcitationReadOut(:);
            else
                obj.observationsHistory(:,tmphistsize+1) = obj.currentTmpObservation(:);
                obj.observationsExcitationHistory(:,tmphistsize+1) = obj.currentTmpExcitationReadOut(:);
                if (tmphistsize+1 > obj.observationsHistorySize)
                    obj.observationsHistory(:,1:(tmphistsize+1-obj.observationsHistorySize))=[];
                    obj.observationsExcitationHistory(:,1:(tmphistsize+1-obj.observationsHistorySize))=[];
                end
            end

            % save the reading as lastReadings
            obj.previousObservation = obj.currentTmpObservation;
            obj.previousExcitationReadOut = obj.currentTmpExcitationReadOut;
            obj.previousExcitationSetReading = obj.currentTmpExcitationSetReading;
            obj.previousObservationError = obj.currentTmpObservationError;
            obj.previousExcitationReadOutError = obj.currentTmpExcitationReadOutError;
            obj.previousExcitationSetReadingError = obj.currentTmpExcitationSetReadingError;
            %
            obj.previousExcitationDeltaApplied = deltaExcitationToApply;
            
            % finaly save the status of the feedback if requested
            obj.saveDataIfRequested();

            % and reset averaging variables to start a fresh acquisition.
            obj.currentAveragingObservations = [];
            obj.currentAveragingExcitationReadOut = [];
            obj.currentAveragingExcitationSetReading = [];
            
            % at the very end notify that we got a new beam.
            obj.myNotifyNewBeam('','')
            % still not sure if this is the right place where to notify
            % newSettings...
            obj.myNotifyUpdateSettigs('','');
        end
        
        function isToSkip = checkIfThisShotHasToBeSkept(obj, dataStruct)
            if obj.stillToSkip > 0
                isToSkip = true;
                obj.stillToSkip = obj.stillToSkip - 1;
                obj.printOut(['I was asked to skip this data. I will skip other ',num2str(obj.stillToSkip)]);
            else
                isToSkip = false;
            end
        end
        
        function improveRMIfRequested(obj)
            if (obj.isRMImproveEnabled && (norm(obj.currentTmpDeltaExcitationSetReading) > 100*eps*obj.nExcitators))
                obj.mySolver.improve(obj.currentTmpDeltaObservation, ...
                    obj.currentTmpDeltaExcitationReadOut,...
                    obj.currentTmpDeltaObservationError);
            else
                obj.printOut('I should improve the RM but the settings of no excitor did change!');
            end
        end
        
        function extractObservedObservationAndExcitation(obj, dataStruct)
            % Uses extraction functions to obtain arrays of values
            % of my new Observation and my new observed Excitation
            
            if isa(obj.excitatorsReadOutExtractionFuntion, 'function_handle')
                myTmpExcitationReadOut = obj.excitatorsReadOutExtractionFuntion(dataStruct, obj.excitatorsReadOutParameters);
            else
                obj.errorOut('Not a good excitation extraction function provided.');
                return;
            end
            if isa(obj.excitatorsSetExtractionFuntion, 'function_handle')
                myTmpExcitationSetReading = obj.excitatorsSetExtractionFuntion(dataStruct, obj.excitatorsParameters);
            else
                obj.errorOut('Not a good excitation set extraction function provided.');
                return;
            end
            if isa(obj.observablesExtractionFunction, 'function_handle')
                myTmpObservation = obj.observablesExtractionFunction(dataStruct, obj.observablesParameters);
            else
                obj.errorOut('Not a good observable extraction function provided.');
                return;
            end
            
            % check if we can go ahead
            if isempty(myTmpObservation) || ...
                    isempty(myTmpExcitationReadOut) || ...
                    isempty(myTmpExcitationSetReading) || ...
                    hasInfNaN(myTmpObservation) || ...
                    hasInfNaN(myTmpExcitationReadOut) || ...
                    hasInfNaN(myTmpExcitationSetReading)
                exception = MException('linearFeedback:extractObservedObservationAndExcitation', ...
                    'No good acquisition this time: some empty|Inf|NaN data... skiping it!');
                obj.printOut(['currentObservation = ',num2str(myTmpObservation')]);
                obj.printOut(['currentObservedExcitation = ',num2str(myTmpExcitationReadOut')]);
                obj.printOut(['currentSetExcitation = ',num2str(myTmpExcitationSetReading')]);
                throw(exception);
            else
                % save new data in the obj.
                obj.currentTmpDataStruct = dataStruct;
                obj.currentAveragingObservations(:,end+1) = myTmpObservation;
                obj.currentAveragingExcitationReadOut(:,end+1) = myTmpExcitationReadOut;
                obj.currentAveragingExcitationSetReading(:,end+1) = myTmpExcitationSetReading;
            end
        end
        
        function collectedDataAveragingFunction(obj)
            % This function is responsible of averaging the previously
            % collected vectors:
            %   currentAveragingObservations
            %   currentAveragingExcitationReadOut
            %   currentAveragingExcitationSetReading
            % and make
            %   currentTmpObservation
            %   currentTmpObservationError
            %   currentTmpExcitationReadOut
            %   currentTmpExcitationReadOutError
            %   currentTmpExcitationSetReading
            %   currentTmpExcitationSetReadingError
            
            obj.currentTmpObservation = mean(obj.currentAveragingObservations,2);
            obj.currentTmpExcitationReadOut = mean(obj.currentAveragingExcitationReadOut,2);
            obj.currentTmpExcitationSetReading = mean(obj.currentAveragingExcitationSetReading,2);
            %
            obj.currentTmpObservationError = std(obj.currentAveragingObservations,0,2);
            obj.currentTmpExcitationReadOutError = std(obj.currentAveragingExcitationReadOut,0,2);
            obj.currentTmpExcitationSetReadingError = std(obj.currentAveragingExcitationSetReading,0,2);
        end
        
        % check that observation is inside observation limits
        function out = isInsideLimits(obj, observation)
            % Checks that the given observation vector is inside limits.
            % no dimension control! Limits can also be just a single number
            
            if isempty(obj.observablesLowerLimits)
                obj.warningOut('Empty observables lower limits. Set it to -Inf');
                obj.observablesLowerLimits = -Inf;
            end
            if isempty(obj.observablesLowerLimits)
                obj.warningOut('Empty observables upper limits. Set it to +Inf');
                obj.observablesUpperLimits = Inf;
            end
            
            if (sum(observation(:) < obj.observablesLowerLimits(:)) == 0 && ...
                    sum(observation(:) > obj.observablesUpperLimits(:)) == 0)
                out = true;
            else
                out = false;
            end
        end
        
        function computeDeltas(obj)
            % calculating deltas with previous readings
            if (length(obj.currentTmpObservation) ~= length(obj.previousObservation) || ...
                    length(obj.currentTmpExcitationReadOut) ~= length(obj.previousExcitationReadOut) || ...
                    length(obj.currentTmpExcitationSetReading) ~= length(obj.previousExcitationSetReading))
                obj.printOut('Seems to be the first updated data.');
                
                obj.currentTmpDeltaObservation = zeros(length(obj.currentTmpObservation),1);
                obj.currentTmpDeltaExcitationReadOut = zeros(length(obj.currentTmpExcitationReadOut),1);
                obj.currentTmpDeltaExcitationSetReading = zeros(length(obj.currentTmpExcitationSetReading),1);
                %
                obj.currentTmpDeltaObservationError = obj.currentTmpDeltaObservation;
                obj.currentTmpDeltaExcitationReadOutError = obj.currentTmpDeltaExcitationReadOut;
                obj.currentTmpDeltaExcitationSetReadingError = obj.currentTmpDeltaExcitationSetReading;
            else
                obj.currentTmpDeltaObservation = obj.currentTmpObservation - obj.previousObservation;
                obj.currentTmpDeltaExcitationReadOut = obj.currentTmpExcitationReadOut - obj.previousExcitationReadOut;
                obj.currentTmpDeltaExcitationSetReading = obj.currentTmpExcitationSetReading - obj.previousExcitationSetReading;
                %
                obj.currentTmpDeltaObservationError = sqrt(obj.currentTmpObservationError.^2 + ...
                    obj.previousObservationError.^2);
                obj.currentTmpDeltaExcitationReadOutError = sqrt(obj.currentTmpExcitationReadOutError.^2 + ...
                    obj.previousExcitationReadOutError.^2);
                obj.currentTmpDeltaExcitationSetReadingError = sqrt(obj.currentTmpExcitationSetReadingError.^2 + ...
                    obj.previousExcitationSetReadingError.^2);
            end
        end
        
        function randomExcitationToApply = computeRandomExcitation(obj)
            randomExcitationToApply = zeros(obj.nExcitators,1);
            
            switch obj.randomExcitationStrategy
                case {obj.randomExcitationStrategyGaussian, ...
                        obj.randomExcitationStrategySquare, ...
                        obj.randomExcitationStrategyPlusMinus}
                    if (sum(abs(obj.randomExcitationGain.*obj.randomExcitationPattern)) > 0)
                        if (length(obj.randomExcitationPattern) == 1 || ...
                                size(obj.randomExcitationPattern,1) == obj.nExcitators)
                            switch obj.randomExcitationStrategy
                                case obj.randomExcitationStrategyGaussian
                                    auxRandValues = randn(obj.nExcitators,1);
                                case obj.randomExcitationStrategySquare
                                    auxRandValues = 2*rand(obj.nExcitators,1)-1;
                                case obj.randomExcitationStrategyPlusMinus
                                    auxRandValues = floor(3*rand(obj.nExcitators,1))-1;
                            end
                            randomExcitationToApply = obj.randomExcitationGain.*obj.randomExcitationPattern.*auxRandValues;
                        else
                            obj.errorOut('Wrong dimension for randomExcitationPattern. Check your code!');
                        end
                    end
                case {obj.randomExcitationStrategyGaussianObservation, ...
                        obj.randomExcitationStrategySquareObservation, ...
                        obj.randomExcitationStrategyPlusMinusObservation}
                    if (sum(abs(obj.randomExcitationGain.*obj.observablesWeights)) > 0)
                        if (length(obj.observablesWeights) == 1 || ...
                                size(obj.observablesWeights,1) == obj.nObservables)

                            switch obj.randomExcitationStrategy
                                case obj.randomExcitationStrategyGaussianObservation
                                    auxRandObservation = obj.randomExcitationGain*(obj.observablesWeights.*randn(obj.nObservables,1));
                                case obj.randomExcitationStrategySquareObservation
                                    auxRandObservation = obj.randomExcitationGain*(obj.observablesWeights.*(2*rand(obj.nObservables,1)-1));
                                case obj.randomExcitationStrategyPlusMinusObservation
                                    auxRandObservation = obj.randomExcitationGain*(obj.observablesWeights.*(floor(3*rand(obj.nObservables,1))-1));
                            end
                            
                            randomExcitationToApply = obj.computeFullCorrectionExcitation(auxRandObservation);
                        else
                            obj.errorOut('Wrong dimension for observablesWeights. Check your code!');
                        end
                    end
                otherwise
                    obj.errorOut('Unknown random excitation strategy. I will not excite anything!')
            end
        end
        
        function correctionExcitationToApply = computeGainedCorrectionExcitationAndExpectations(obj)
            if (size(obj.wantedObservation,1) ~= obj.nObservables)
                obj.errorOut(['linearFeedback::computeGainedCorrectionExcitationAndExpectations','Not a good "wantedObservation" provided! Check sizes']);
                correctionExcitationToApply = zeros(obj.nExcitators,1);
                correctionExcitationToApplyWithoutLimits = zeros(obj.nExcitators,1);
            else
                [correctionExcitationToApply, correctionExcitationToApplyWithoutLimits ] = ...
                    obj.computeFullCorrectionExcitation(obj.wantedObservation(:) - obj.currentTmpObservation(:));
            end
            
            % fill relevant expectations
            [auxOutcome, auxOutcomeError] = obj.mySolver.computeOutcome(correctionExcitationToApply);
            obj.bestExcitationExpectedWithLimits = obj.currentTmpExcitationSetReading + correctionExcitationToApply;
            obj.bestObservationExpectedWithLimits = obj.currentTmpObservation + auxOutcome;
            obj.bestObservationExpectedWithLimitsError = auxOutcomeError;
            
            [auxOutcome, auxOutcomeError] = obj.mySolver.computeOutcome(correctionExcitationToApplyWithoutLimits);
            obj.bestExcitationExpectedWithoutLimits = obj.currentTmpExcitationSetReading + correctionExcitationToApplyWithoutLimits;
            obj.bestObservationExpectedWithoutLimits = obj.currentTmpObservation + auxOutcome;
            obj.bestObservationExpectedWithoutLimitsError = auxOutcomeError;
            
            % apply correction gain
            correctionExcitationToApply = correctionExcitationToApply*obj.correctionGain;
        end
        
        function varargout = computeFullCorrectionExcitation(obj, deltaObservationRequired)
            % observables weights cannot be negative! so check and update
            % them in case they are < 0
            if sum(obj.observablesWeights < 0) > 0
                obj.warningOut('Some observables weights < 0. Set them to 0');
                obj.observablesWeights(obj.observablesWeights < 0) = 0;
            end
            
            % reset observables weights inside solver
            obj.mySolver.observablesWeights = obj.observablesWeights;
            
            % check reasonable solver limits
            if isempty(obj.excitatorsLowerLimits)
                obj.warningOut('Empty excitors lower limits. Set it to -Inf');
                obj.excitatorsLowerLimits = -Inf;
            end
            if isempty(obj.excitatorsUpperLimits)
                obj.warningOut('Empty excitors upper limits. Set it to +Inf');
                obj.excitatorsUpperLimits = +Inf;
            end  
            
            % define local limits
            tmpLowerLimit = obj.excitatorsLowerLimits(:)-obj.currentTmpExcitationSetReading(:);
            tmpUpperLimit = obj.excitatorsUpperLimits(:)-obj.currentTmpExcitationSetReading(:);
            
            % define best excitation
            if obj.targetWantedExcitation
                tmpBestExcitation = obj.wantedExcitation(:)-obj.currentTmpExcitationSetReading(:);
            else
                tmpBestExcitation = zeros(obj.nExcitators,1);
            end

            % Sept 2014... this should be done already by the solver
            % class
%             % disable some unwanted correctors
%             tmpToDisable = (-tmpLowerLimit < 100*eps & tmpUpperLimit < 100*eps);
%             obj.mySolver.disabledActuatorsIdx = tmpToDisable;
            

            % compute limited excitation to apply
            try
                % calculate full correction with excitator limits
                fullCorrectionExcitationToApply = obj.mySolver.solve(deltaObservationRequired, ...
                    tmpLowerLimit, ...
                    tmpUpperLimit, ...
                    tmpBestExcitation);
            catch e
                obj.errorOut(['linearFeedback::fullCorrectionExcitationToApply: impossible to compute correction excitation:', e.message])
                fullCorrectionExcitationToApply = zeros(obj.nExcitators,1);
            end
            varargout{1} = fullCorrectionExcitationToApply;
            
            % compute also unlimited excitation to apply if required
            if nargout == 2
                try
                    % calculate full correction without limits
                    fullCorrectionExcitationToApplyWithoutLimits = obj.mySolver.solve(deltaObservationRequired);
                catch e
                    obj.errorOut(['linearFeedback::fullCorrectionExcitationToApply: impossible to compute unlimited correction excitation:', e.getReport()])
                    fullCorrectionExcitationToApplyWithoutLimits = zeros(obj.nExcitators,1);
                end
                varargout{2} = fullCorrectionExcitationToApplyWithoutLimits;
            end
        end
        
        function constrainedExcitation = constrainExcitationInsideLimits(obj, unconstrainedExcitation)
            constrainedExcitation = unconstrainedExcitation;
            
            % check sizes
            if length(constrainedExcitation) ~= obj.nExcitators
                error(['constrainExcitationInsideLimits: Wrong vector length: I am expecting ',num2str(obj.nExcitators),...
                    ' but ', num2str(length(constrainedExcitation)), ' given.']);
            end
            
            upperLimits = obj.excitatorsUpperLimits;
            lowerLimits = obj.excitatorsLowerLimits;
            if length(upperLimits) ~= obj.nExcitators
                upperLimits = upperLimits(1)*ones(obj.nExcitators,1);
            end
            if length(lowerLimits) ~= obj.nExcitators
                lowerLimits = lowerLimits(1)*ones(obj.nExcitators,1);
            end
            
            for i=1:obj.nExcitators
                if constrainedExcitation(i) > upperLimits(i)
                    constrainedExcitation(i) = upperLimits(i);
                end
                if constrainedExcitation(i) < lowerLimits(i)
                    constrainedExcitation(i) = lowerLimits(i);
                end
            end
        end
        
        function sendExcitationToHardware(obj, excitationArray)
            % filling a structure
            setDataStruct = obj.excitatorsSetFillFunction(excitationArray, obj.excitatorsParameters);
            
            % filter setDataStruct and parameter list to only set
            % changed excitors
            tmpSetParameters = matlabDataAndSignalsHelper.filterSetParameterList(obj.excitatorsParameters,...
                obj.currentTmpDataStruct,... % oldDataStruct to which make comparison.
                setDataStruct); % newDataStruct -> what I would like now.
            
            % send to hardware
            %%%
            obj.myJapc.JSetAll(tmpSetParameters, setDataStruct);
        end
        
        function updateRandomScanningIndexIfRequired(obj)
            if (obj.randomExcitationGain && obj.randomPatternScanning)
                obj.randomPatternScanAuxDone = obj.randomPatternScanAuxDone + 1;
                if obj.randomPatternScanAuxDone >= obj.randomPatternScanNumShotsPerPoint
                    obj.randomPatternScanCurrenttIdx = obj.randomPatternScanCurrenttIdx + 1;
                    if obj.randomPatternScanCurrenttIdx > obj.nExcitators || obj.randomPatternScanCurrenttIdx > obj.randomPatternScanCurrenttEndIdx
                        % our job is done.
                        obj.randomPatternScanning = false;
                        obj.randomExcitationPattern(:) = 0;
                        return
                    end
                    obj.randomExcitationPattern(:) = 0;
                    obj.randomExcitationPattern(obj.randomPatternScanCurrenttIdx) = 1;
                    obj.randomPatternScanAuxDone = 0;
                end
            end
        end
        
        function fillFutureExpectations(obj, deltaExcitation)
            % general expectation based on current knowledge
            obj.previousObservationExpected = obj.futureObservationExpected;
            obj.previousObservationExpectedError = obj.futureObservationExpectedError;
            if (nargin == 2 && length(deltaExcitation) == obj.nExcitators)
                [auxOutcome, auxOutcomeError] = obj.mySolver.computeOutcome(deltaExcitation);
                obj.futureObservationExpected = obj.currentTmpObservation + auxOutcome;
                obj.futureObservationExpectedError = auxOutcomeError;
            else
                obj.futureObservationExpected = obj.currentTmpObservation;
                obj.futureObservationExpectedError = zeros(obj.nObservables,1);
            end
        end
        
        function saveDataIfRequested(obj)
            if obj.saveData
                data = prepareDataStructToSave(obj);
                save([obj.saveDataPath unixtime2HumanTime( mean([obj.currentTmpDataStruct(end).localTimeStamp_ms])/1000) '.mat'],'data')
                clear data
            end
        end
        
        function data = prepareDataStructToSave(obj)
            data=struct;
            
            % filling structure
            data.cycleName=obj.cycleName;
            data.observablesParameters=obj.observablesParameters;
            data.excitatorsParameters=obj.excitatorsParameters;
            data.excitatorsReadOutParameters=obj.excitatorsReadOutParameters;
            
            data.currentDataStruct=obj.currentTmpDataStruct;
            
            data.observables          = obj.currentTmpObservation;
            data.observablesError     = obj.currentTmpObservationError;
            data.excitorsReading      = obj.currentTmpExcitationReadOut;
            data.excitorsReadingError = obj.currentTmpExcitationReadOutError;
            data.excitorsSetting      = obj.currentTmpExcitationSetReading;
            data.excitorsSettingError = obj.currentTmpExcitationSetReadingError;
            
            data.observablesAveragingData     = obj.currentAveragingObservations;
            data.excitorsReadingAveragingData = obj.currentAveragingExcitationReadOut;
            data.excitorsSettingAveragingData = obj.currentAveragingExcitationSetReading;
            
            data.deltaExcitationFromPrevious          = obj.currentTmpDeltaExcitationReadOut;
            data.deltaExcitationFromPreviousError     = obj.currentTmpDeltaExcitationReadOutError;
            data.deltaSetExcitationFromPrevious       = obj.currentTmpDeltaExcitationSetReading;
            data.deltaSetExcitationFromPreviousError  = obj.currentTmpDeltaExcitationSetReadingError;
            data.deltaObservationFromPrevious         = obj.currentTmpDeltaObservation;
            data.deltaObservationFromPreviousError    = obj.currentTmpDeltaObservationError;
            
            data.CorrectionGain = obj.correctionGain;
            data.ExcitationGain = obj.randomExcitationGain;
            
            data.randomExcitationPattern = obj.randomExcitationPattern;
            data.observablesWeights      = obj.observablesWeights;
            
            data.observablesLowerLimits = obj.observablesLowerLimits;
            data.observablesUpperLimits = obj.observablesUpperLimits;
            data.excitatorsLowerLimits  = obj.excitatorsLowerLimits;
            data.excitatorsUpperLimits  = obj.excitatorsUpperLimits;
            
            data.wantedObservation = obj.wantedObservation;
            data.wantedExcitation  = obj.wantedExcitation;
            
            data.previousObservationExpected      = obj.previousObservationExpected;
            data.previousObservationExpectedError = obj.previousObservationExpectedError;
            
            data.bestObservationExpectedWithLimits      = obj.bestObservationExpectedWithLimits;
            data.bestObservationExpectedWithLimitsError = obj.bestObservationExpectedWithLimitsError;
            data.bestExcitationExpectedWithLimits       = obj.bestExcitationExpectedWithLimits;
            
            data.deltaExcitationToApply     = obj.previousExcitationDeltaApplied; % is saved in previous, but is the one used now.
            data.futureObservationExpected  = obj.futureObservationExpected;
            data.futureObservationExpectedError = obj.futureObservationExpectedError;
            
            data.observationsHistory                = obj.observationsHistory;
            data.observationsExcitationHistory      = obj.observationsExcitationHistory;
            
            % add 10/08/2016 
            data.targetWantedExcitation             = obj.targetWantedExcitation;
            data.solverExcitationOptimizationWeight = obj.mySolver.excitationOptimizationWeight;
            
            data.solverObservationHistory = obj.mySolver.historyTrainingObservations;
            data.solverObservationHistoryError = obj.mySolver.historyTrainingObservationsErrors;
            data.solverExcitationHistory  = obj.mySolver.historyTrainingExcitations;
            
            data.responseMatrix = obj.mySolver.responseMatrix;
            data.responseMatrixShape = obj.mySolver.responseMatrixShape;
            data.responseMatrixErrors = obj.mySolver.responseMatrixErrors;
        end
        
        function loadStatusFromFile(obj, file)
            %loadStatusFromFile(obj, file)
            % loads the status of the feedback from a saved file
            load(file,'data');
            
            obj.myMonitor.lastDataStruct = data.currentDataStruct;
            obj.currentTmpDataStruct     = data.currentDataStruct;
            
            obj.currentTmpObservation          = data.observables;
            try
                obj.currentTmpObservationError = data.observablesError;
            catch e
                obj.currentTmpObservationError = zeros(obj.nObservables,1);
            end
            
            obj.currentTmpExcitationReadOut          = data.excitorsReading;
            try
                obj.currentTmpExcitationReadOutError = data.excitorsReadingError;
            catch e
                obj.currentTmpExcitationReadOutError = zeros(obj.nExcitators,1);
            end
            
            obj.currentTmpExcitationSetReading          = data.excitorsSetting;
            try
                obj.currentTmpExcitationSetReadingError = data.excitorsSettingError;
            catch e
                obj.currentTmpExcitationSetReadingError = zeros(obj.nExcitators,1);
            end    
            
            
            try
                obj.currentAveragingObservations = data.observablesAveragingData;
            catch e
                obj.currentAveragingObservations = [];
            end
            try
                obj.currentAveragingExcitationReadOut = data.excitorsReadingAveragingData;
            catch e
                obj.currentAveragingExcitationReadOut = [];
            end
            try
                obj.currentAveragingExcitationSetReading = data.excitorsSettingAveragingData;
            catch e
                obj.currentAveragingExcitationSetReading = [];
            end

            
            
            obj.currentTmpDeltaExcitationReadOut          = data.deltaExcitationFromPrevious;
            try
                obj.currentTmpDeltaExcitationReadOutError = data.deltaExcitationFromPreviousError;
            catch e
                obj.currentTmpDeltaExcitationReadOutError = zeros(obj.nExcitators,1);
            end

            try
                obj.currentTmpDeltaExcitationSetReading      = data.deltaSetExcitationFromPrevious;
                obj.currentTmpDeltaExcitationSetReadingError = data.deltaSetExcitationFromPreviousError;
            catch e
                obj.currentTmpDeltaExcitationSetReading      = zeros(obj.nExcitators,1);
                obj.currentTmpDeltaExcitationSetReadingError = zeros(obj.nExcitators,1);
            end
            
            obj.currentTmpDeltaObservation            = data.deltaObservationFromPrevious;
            try
                obj.currentTmpDeltaObservationError   = data.deltaObservationFromPreviousError;
            catch e
                obj.currentTmpDeltaObservationError   = zeros(obj.nObservables,1);
            end
            
            
            obj.setCorrectionGain(data.CorrectionGain);
            obj.setRandomExcitationGain(data.ExcitationGain);
            
            obj.randomExcitationPattern= data.randomExcitationPattern;
            obj.observablesWeights     = data.observablesWeights;
            
            obj.observablesLowerLimits = data.observablesLowerLimits;
            obj.observablesUpperLimits = data.observablesUpperLimits;
            obj.excitatorsLowerLimits  = data.excitatorsLowerLimits;
            obj.excitatorsUpperLimits  = data.excitatorsUpperLimits;
            
            obj.wantedObservation=data.wantedObservation;
            try
                obj.wantedExcitation = data.wantedExcitation;
            catch e
                obj.wantedExcitation = zeros(obj.nExcitators,1);
            end
            
            obj.previousObservationExpected = data.previousObservationExpected;
            obj.previousObservationExpectedError = data.previousObservationExpectedError;
            
            obj.bestObservationExpectedWithLimits = data.bestObservationExpectedWithLimits;
            obj.bestObservationExpectedWithLimitsError = data.bestObservationExpectedWithLimitsError;
            obj.bestExcitationExpectedWithLimits = data.bestExcitationExpectedWithLimits;
            
            obj.previousExcitationDeltaApplied = data.deltaExcitationToApply;
            obj.futureObservationExpected = data.futureObservationExpected;
            obj.futureObservationExpectedError = data.futureObservationExpectedError;
            
            obj.observationsHistory = data.observationsHistory;
            try
                obj.observationsExcitationHistory = data.observationsExcitationHistory;
            catch e
                obj.observationsExcitationHistory = nan(obj.nExcitators,size(obj.observationsHistory,2));
            end
            
            try
                % add 10/08/2016 
                obj.targetWantedExcitation = data.solverExcitationOptimizationWeight;
                obj.mySolver.excitationOptimizationWeight = data.solverExcitationOptimizationWeight;
            catch e
                obj.targetWantedExcitation = false;
                obj.mySolver.excitationOptimizationWeight = 0;
            end
            
            obj.mySolver.historyTrainingObservations = data.solverObservationHistory;
            obj.mySolver.historyTrainingObservationsErrors = data.solverObservationHistoryError;
            obj.mySolver.historyTrainingExcitations = data.solverExcitationHistory;
            
            obj.mySolver.responseMatrix=data.responseMatrix;
            obj.mySolver.responseMatrixShape=data.responseMatrixShape;
            obj.mySolver.responseMatrixErrors=data.responseMatrixErrors;
        end
        
        function setWantedOrbitFromReference(obj)
            % setWantedOrbitFromReference(obj)
            % it will get the reference observables from the reference database
            
            for iObs=1:length(obj.observablesParameters)
                try
                    auxVector = obj.myJapc.JGetRefFesaSignal(obj.observablesParameters{iObs});
                    obj.wantedObservation(iObs) = auxVector(1);
                catch e
                    disp(['Impossible to get Ref for ',obj.observablesParameters{iObs},...
                        ': ',e.message]);
                end
            end
        end
        
        function setWantedExcitationFromReference(obj)
            % setWantedExcitationFromReference(obj)
            % it will get the reference observables from the reference database
            
            for iExcit=1:length(obj.excitatorsParameters)
                try
                    auxVector = obj.myJapc.JGetRefFesaSignal(obj.excitatorsParameters{iExcit});
                    obj.wantedExcitation(iExcit) = auxVector(1);
                catch e
                    disp(['Impossible to get Ref for ',obj.excitatorsParameters{iExcit},...
                        ': ',e.message]);
                end
            end
        end
        
        
        %%
        % Internally used functions to display running informations
        function printOut(obj,text)
            if obj.verbose
                disp(text);
            end
        end
        function warningOut(~, text)
            warning(['linearFeedback:WARNING: ', text]);
        end
        function errorOut(~, text)
            disp(['linearFeedback:ERROR: ', text]);
        end
    end % end private methods
    
    methods (Static)
        function scaledVector = scaleVectorInsideCustomLimits(unscaledVector, lowerLimits, upperLimits)
            if numel(unscaledVector) ~= numel(lowerLimits) || numel(unscaledVector) ~= numel(upperLimits)
                error('linearFeedback::scaleVectorInsideCustomLimits: vector and limits have different sizes')
            end
            
            scaledVector = unscaledVector;
            
            [maxSup, idxMaxSup] = max(unscaledVector-upperLimits);
            [maxInf, idxMaxInf] = max(lowerLimits-unscaledVector);
            if max(maxSup,maxInf) > 0
                if maxSup >= maxInf
                    scaledVector = unscaledVector*abs((upperLimits(idxMaxSup)/unscaledVector(idxMaxSup)));
                else
                    scaledVector = unscaledVector*abs((lowerLimits(idxMaxInf)/unscaledVector(idxMaxInf)));
                end
            end
        end
    end
    events
        newBeam
        updateSettings
        newSet
    end
end

