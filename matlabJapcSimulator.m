classdef matlabJapcSimulator < handle
    %MATLABJAPCSIMULATOR 
    %   This simulator can substitute the matlabJapc and matlabJapcMonitor
    %   of a linearFeedback object to take the data from a madxModel
    %   instead than the real machine.
    %   It is supposed to be able to read only magnets currents and BPMs
    %   orbits (in the therm of xeneric sampler signals, H, V, S).
    %   It can be used as example for more complex simulators.
    
    properties
        % matlab structure with last data
        lastDataStruct=struct;
        currentDataStruct=struct;
        recordedCycles = 0;
        
        % The function handle that is called after an acquisition.
        % It must accept an argument, that will contain the lastDataStruct
        % with the data of last acquisition.
        externalFunction
        
        % Timer used to trigger new acquisition
        simulationTimer
        
        % If true, the data delivery is suspended, but the subscription is
        % still ongoing. Use it with care!
        isPaused = false;
        
        % just what you expect
        verbose = false;
        
        % madx2MatlabModel object from which take data
        madxModel
        
        % in the madx2Matlab model there is a conversion database from MADX
        % names to signals. Here I need the opposit... So I generate a new
        % database during the construction of the object...
        madxModelConveretedDatabase
        
        % noise levels, used for timerFunctionAcquisition only
        % (JGetAll function will not be affected by noise!)
        noiseLevelBPMsH = 0;
        noiseLevelBPMsV = 0;
        noiseLevelMagnets = 0;
        
        % the construction signal list
        signalList
        % madx Model Readable elements and fields
        madxBPMSignals % BPMsignals I need to acquire
        madxBPMElements % BPMname
        madxBPMFields % 1=X | 2=Y | 3=S
        madxCurrentSignals % Magnet signals I need to acquire
        madxCurrentParameters % Magnet name
        
        % to add excitation on some parameters, e.g. energy
        madxParametersToExcite = {};
        madxParametersToExciteZeroValues = 0;
        madxParametersToExciteSigmaValues = 0;
    end
    
    methods
        function obj = matlabJapcSimulator(madxMatlabModel, signalList, externalFunction)
            % obj = matlabJapcSimulator(madxMatlabModel, signalList, externalFunction)
            % constructor of this JapcSimulator used to simulate a monitor
            % and a GET/SET object that takes/sets the data to
            % "madxMatlabModel" object.
            % 
            % by default it will give data every 1.2 seconds to simulate
            % the real machine.
            % This time can be changed afterwards.
            
            obj.madxModel = madxMatlabModel;
            obj.externalFunction = externalFunction;
            obj.signalList = signalList;
            
            obj.simulationTimer = timer('TimerFcn',@(mTimer,e)obj.myTimerFunction,...
                'ExecutionMode','fixedRate',...
                'Period',1.2,...
                'BusyMode','drop');
            
            obj.madxModelConveretedDatabase = matlabJapcSimulator.convertElementsDatabase(obj.madxModel.conversionMapDatabase);
            
            obj.parseSignalListToMADXParameters(signalList);
        end
    
        %
        % matlabMonitor function needed for linear feedback simulations
        %
        
        function start(obj, bufferSize)
            if bufferSize > 1
                disp('Warning: bufferSize is not supported on this JapcSimulator');
            end
            start(obj.simulationTimer);
        end
        function stop(obj)
            stop(obj.simulationTimer);
        end
        function pauseOn(obj)
            obj.isPaused = true;
        end
        function pauseOff(obj)
            obj.isPaused = false;
        end
        
        function out = isAcquiringData(obj)
            out = strcmp(obj.simulationTimer.Running,'on');
        end
        
        function enableDebug(obj)
            obj.verbose=true;
        end
        function disableDebug(obj)
            obj.verbose=false;
        end
        
        function delete(obj)
            delete(obj.simulationTimer);
        end

        %
        % matlabJAPC function needed for linear feedback simulations
        %
        
        function outputStruct = JGetAll(obj, getSignals)
            % JGetAll(obj, getSignals)
            % it get the current value for every signal specified in
            % getSignals. The outputStruct is a standard matlabMonitor
            % structure that can be passed to JSetAll. 
            
            % normally JGetAll and JSetLAll try to get data from the
            % parameters of the model... so I'm look only there...
            auxMADXelements = convertSignalListToModelParameter(obj, getSignals);
            auxValues = obj.madxModel.getModelParameters(auxMADXelements);

            % generate output structure
            outputStruct = struct;
            for i=1:length(getSignals)
                outputStruct = matlabDataAndSignalsHelper.simpleSetvalue2Struct(getSignals{i}, auxValues(i), outputStruct);
            end
        end
                
        function JSetAll(obj, setSignals, setValuesDataset)
            % JSetAll(obj, setSignals, setValuesDataset)
            % It sets the new values included in the setValuesDaset structure to all setSignals signals
            % The setValuesDataset structure is supposed to be compatible with the one used in the class
            % matlabMonitor!!!
            
            % normally JGetAll and JSetLAll try to get data from the
            % parameters of the model... so I'm look only there...
            auxMADXelements = convertSignalListToModelParameter(obj, setSignals);
            auxNewValues = NaN(length(auxMADXelements),1);
            
            for i=1:length(setSignals)
                auxNewValues(i) = matlabDataAndSignalsHelper.simpleExtractSingleSignal(setValuesDataset, setSignals{i});
            end
            obj.madxModel.setModelParameters(auxMADXelements, auxNewValues);
        end

        %
        % specific function of the simulator
        %
        function parseSignalListToMADXParameters(obj, signalList)
            % parseSignalListToMADXParameters(obj, signalList)
            % we suppose in signalList there might be BPMs signal in the
            % form of xenericSampler (/MeansAtCursor#means) and magnets
            % get/set signals.
            
            obj.madxBPMSignals = cell(0);
            obj.madxBPMElements = cell(0);
            obj.madxBPMFields = [];
            obj.madxCurrentSignals = cell(0);
            obj.madxCurrentParameters = cell(0);
            
            auxNsignals = length(signalList);
            for i=1:auxNsignals
                if isempty(regexpi(signalList{i},'MeansAtCursor#means','once'))
                    % it must be a magnet (hopefully)
                     obj.madxCurrentSignals{end+1} = signalList{i};
                     try
                         obj.madxCurrentParameters{end+1} = cell2mat(obj.convertSignalListToModelParameter(signalList(i)));
                     catch e
                         error(['matlabJapcSimulator: impossible to continue: I don''t know what ',signalList{i},' is.. : ',e.message]);
                     end
                else
                    % it is a BPM
                    auxString = regexp(signalList{i},'/','split');
                    auxString = auxString{1};
                    
                    obj.madxBPMSignals{end+1} = signalList{i};
                    obj.madxBPMElements{end+1} = auxString([1:3 6:end-1]);
                    
                    % last char is the plane
                    if strcmpi(auxString(end),'H')
                        obj.madxBPMFields(end+1) = 1;
                    elseif strcmpi(auxString(end),'V')
                        obj.madxBPMFields(end+1) = 2;
                    elseif strcmpi(auxString(end),'S')
                        obj.madxBPMFields(end+1) = 3;
                    else
                        error(['matlabJapcSimulator: impossible to continue: I don''t know what ',signalList{i},' is..']);
                    end
                end
            end
        end
        
        function madxElementList = convertSignalListToModelParameter(obj, signalList)
            %madxElementList = convertSignalListToModelParameter(obj, signalList)
            % using the current mad2Matlab object and its conversion
            % database, it translates a signal to a an element name or
            % parameter defined in the MADX model of the machine
            
            madxElementList = cell(0);
            for i=1:length(signalList)
                try
                    madxElementList{i} = obj.madxModelConveretedDatabase(signalList{i});
                catch e
                    disp(['No signal "',signalList{i},'" defined in current model2machine MapDatabase. Check your code!']);
                    madxElementList{i} = '';
                    continue
                end
            end
        end
    
        function setTimerPeriod(obj, period)
            %setTimerPeriod(obj, period)
            % it sets the internal timer period to a different value than
            % the default (1.2 second like a basic period)
            
            if obj.isAcquiringData()
                obj.stop();
                set(obj.simulationTimer,'Period',period);
                obj.start(1);
            else
                set(obj.simulationTimer,'Period',period);
            end
        end
        
        function startExcitingParameters(obj, madxParameters, zeroValues, sigmaValues)
            % startExcitingParameters(obj, madxParameters, zeroValues, sigmaValues)
            %  it enables for each computed optics to excite some
            %  parameters randomly...
            if ischar(madxParameters)
                obj.madxParametersToExcite = {madxParameters};
            else
                obj.madxParametersToExcite = madxParameters;
            end
            obj.madxParametersToExciteZeroValues = zeroValues;
            obj.madxParametersToExciteSigmaValues = sigmaValues;
        end
        function stopExcitingParameters(obj)
            % stopExcitingParameters(obj)
            % it stops exciting parameters, i.e. it sets back the initial
            % values and remove any information...
            
            obj.madxModel.setModelParameters(obj.madxParametersToExcite, obj.madxParametersToExciteZeroValues);
            obj.madxParametersToExcite = [];
            obj.madxParametersToExciteZeroValues = [];
            obj.madxParametersToExciteSigmaValues = [];
        end
    end
    
    
    methods(Access=protected)
        
        % internally used function to display running informations
        function printOut(obj,text)
            if obj.verbose
                disp(text);
            end
        end
        
        function myTimerFunction(obj)
            % check if we are not paused
            if obj.isPaused
                return
            end

            % see if I have some parameters to excite
            if ~isempty(obj.madxParametersToExcite)
                auxValuesToSet = obj.madxParametersToExciteZeroValues(:) + obj.madxParametersToExciteSigmaValues.*randn(length(obj.madxParametersToExcite),1);
                obj.madxModel.setModelParameters(obj.madxParametersToExcite, auxValuesToSet);
            end
            
            % start generating the structure
            obj.currentDataStruct = struct;
            obj.currentDataStruct.parameters = obj.signalList;
            obj.currentDataStruct.comment = 'Simulated data';
            % timestamps
            obj.currentDataStruct.localTimeStamp_s = etime(clock,[1970 1 1 0 0 0]);
            obj.currentDataStruct.localTimeStamp_ms = obj.currentDataStruct.localTimeStamp_s*1000;
            obj.currentDataStruct.headerTimeStamps = zeros(length(obj.signalList),1);
            obj.currentDataStruct.headerCycleStamps = zeros(length(obj.signalList),1);
            
            %
            % compute a new optics from the model to extract beam position
            %
            auxOptics = obj.madxModel.computeOptics();
            % extract all the BPMs information
            
            allBPMsValues = madx2matlab.extractOpticFieldsValues(auxOptics, obj.madxBPMElements, {'X','Y'});
            allBPMsValues = reshape(allBPMsValues, length(allBPMsValues)/2, 2);
            allBPMsValues = allBPMsValues.*1000; % convert in mm
            % add some noise
            allBPMsValues = allBPMsValues + ...
                [obj.noiseLevelBPMsH*randn(size(allBPMsValues,1),1),...
                obj.noiseLevelBPMsV*randn(size(allBPMsValues,1),1)];
            %
            % add just a number for the current value of each BPM. One could
            % actually define some trasmissione given the machine aperture
            % and optics functions... one day... ;)
            allBPMsValues(:,end+1) = -4;

            for i=1:length(obj.madxBPMSignals)
                myStr = matlabDataAndSignalsHelper.convertSignalToStructPath(obj.madxBPMSignals{i});
                myStr = strsplit(myStr,'.');
                auxValue = allBPMsValues(i,obj.madxBPMFields(i));
                
                obj.currentDataStruct = setfield(obj.currentDataStruct, {1}, myStr{:}, 'value', auxValue);
                obj.currentDataStruct = setfield(obj.currentDataStruct, {1}, myStr{:}, 'timeStamp', 0);
                obj.currentDataStruct = setfield(obj.currentDataStruct, {1}, myStr{:}, 'cycleStamp', 0);
                obj.currentDataStruct = setfield(obj.currentDataStruct, {1}, myStr{:}, 'error', '');
            end      

            %
            % extract other magnets data
            %
            allMagnetsValues = obj.madxModel.getModelParameters(obj.madxCurrentParameters);
            % add some noise
            allMagnetsValues = allMagnetsValues(:) + obj.noiseLevelMagnets*randn(length(allMagnetsValues),1);
            % add them to the structure
            for i=1:length(obj.madxCurrentSignals)
                myStr = matlabDataAndSignalsHelper.convertSignalToStructPath(obj.madxCurrentSignals{i});
                myStr = strsplit(myStr,'.');
                obj.currentDataStruct = setfield(obj.currentDataStruct, {1}, myStr{:}, 'value', allMagnetsValues(i));
                obj.currentDataStruct = setfield(obj.currentDataStruct, {1}, myStr{:}, 'timeStamp', 0);
                obj.currentDataStruct = setfield(obj.currentDataStruct, {1}, myStr{:}, 'cycleStamp', 0);
                obj.currentDataStruct = setfield(obj.currentDataStruct, {1}, myStr{:}, 'error', '');
            end

            % sequential number saved inside.
            obj.currentDataStruct(end).seqNumber=obj.recordedCycles;
            obj.recordedCycles = obj.recordedCycles + 1;

            % create a copy of currentDataStruct
            myDataStruct = obj.currentDataStruct;
            
            % record new data inside obj itself
            obj.lastDataStruct=myDataStruct;

            % calling externalFunction if present
            if isa(obj.externalFunction, 'function_handle')
                % externalFunction is a handle
               obj.externalFunction(myDataStruct);
            else
                obj.printOut('No extraction function defined.');
            end
            
            % external notification of newBeam event
            notify(obj,'newBeam')
            obj.printOut('newBeam event emitted.');
        end
    end
    methods(Static)
        function convertedMap = convertElementsDatabase(elementsDatabaseMap)
            convertedMap = containers.Map();
            
            auxKeys = elementsDatabaseMap.keys;
            auxValues = cell2mat(elementsDatabaseMap.values);

            % both get and set are matched to the same key.
            for i=1:length(auxKeys)
                if ~isempty(auxValues(i).getSignal)
                    convertedMap(auxValues(i).getSignal) = auxKeys{i};
                end
                if ~isempty(auxValues(i).setSignal)
                    convertedMap(auxValues(i).setSignal) = auxKeys{i};
                end
            end
        end
    end
    events
        newBeam
    end
end

