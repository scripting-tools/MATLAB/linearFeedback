classdef dispersionCorrection < linearFeedback
    %DISPERSIONCORRECTION Summary of this class goes here
    %   Detailed explanation goes here
    
    properties        
        bpmCursorIndex = 1;
        bpmLogCursorIndex = 1;
                
        maxAbsQuadPercentChange = 0.05; % normally you don't want to change your quads more than 5 percent the initial value
        minMagnetChange = 0.001; % if the change of a magnet is lower than this, don't move it.

        % when computing the best excitation (dispersion free) it only
        % corrects this percent of variance. Too high values will drive
        % your system into noise. To low value will just partially correct
        % dispersion.
        dispersionFreeRetainedVariance = 0.9;
        
        bpmParameters;
        bpmLogParameters;
        correctorsGetParameters;
        correctorsSetParameters;
        quadrupolesGetParameters;
        quadrupolesSetParameters;
        
        nBPMParameters = 0;
        nBPMLogParameters = 0;
        nCorrectors = 0;
        nQuadrupoles = 0;
        
        initialQuadrupoleSetValues;
        initialQuadrupoleSetStruct;
        initialQuadrupoleGetValues;
        initialQuadrupoleGetStruct;
        
        % some variables used for logging purpose
        internalLoggedBpmValues = [];
        internalLoggedBpmStds = [];
    end
    
    methods
        % constructorfullFinalMatrixShape
        function obj = dispersionCorrection(...
                bpmParameters, ...
                bpmLogParameters, ...
                correctorsGetParameters, correctorsSetParameters,...
                quadrupolesGetParameters, quadrupolesSetParameters,...
                simpleMatrixShape,...
                offlineExecution)
            
            % small check
            if length(correctorsGetParameters) ~= length(correctorsSetParameters)
                error('Different length of correctorsGetParameters and correctorsSetParameters parameters.')
            end
            if length(quadrupolesGetParameters) ~= length(quadrupolesSetParameters)
                error('Different length of quadrupolesGetParameters and quadrupolesSetParameters parameters.')
            end
            if size(simpleMatrixShape,1) ~= length(bpmParameters) || size(simpleMatrixShape,2) ~= length(correctorsGetParameters)
                error('Given simple response matrix shape is not consistent with provided signals.')                
            end
            
            % assemble matrix shapes needed 
            fullInternalMatrixShape = [ones(size(simpleMatrixShape,1),1), simpleMatrixShape, simpleMatrixShape];
            fullInternalInitialMatrix = zeros(size(fullInternalMatrixShape));
            % 
            
            % create the object, i.e. the linearFeedback
            obj = obj@linearFeedback('SCT.USER.SETUP', ...
                [bpmParameters, bpmLogParameters], '', ...
                [correctorsSetParameters, quadrupolesSetParameters], @orbitCorrection.generalCorrectorsExtractionFuntion, '', ...
                [correctorsGetParameters, quadrupolesGetParameters], @orbitCorrection.generalCorrectorsExtractionFuntion, ...
                fullInternalInitialMatrix, offlineExecution);

            % set matrix shape
            obj.mySolver.responseMatrixShape = fullInternalMatrixShape;
            
            % store in the newly created object the list of parameters
            obj.bpmParameters = bpmParameters;
            obj.bpmLogParameters = bpmLogParameters;
            obj.correctorsGetParameters = correctorsGetParameters;
            obj.correctorsSetParameters = correctorsSetParameters;
            obj.quadrupolesGetParameters = quadrupolesGetParameters;
            obj.quadrupolesSetParameters = quadrupolesSetParameters;
            
            % lengths
            obj.nBPMParameters = length(bpmParameters);
            obj.nBPMLogParameters = length(bpmLogParameters);
            obj.nCorrectors = length(correctorsGetParameters);
            obj.nQuadrupoles = length(quadrupolesGetParameters);
            
            % define the proper extraction functions
            obj.observablesExtractionFunction=@obj.bpmExtractionFunction;
            obj.excitatorsSetFillFunction=@obj.correctorFillFunction;

            % define reasonable excitation weights
            obj.randomExcitationPattern = zeros((2*obj.nCorrectors + 1),1);
            obj.randomExcitationPattern(1) = 0.01; % this is the scaling of all the quadrupoles! should not be higher than 1 %!
            obj.randomExcitationPattern(2:(obj.nCorrectors+1)) = 1; % those are the correctors themself. all the reset is not indipendently excitable!

            % read initial quadrupoles settings
            if offlineExecution
                obj.printOut('You are starting the class in offline mode.. be carefull!')
            else
                % get initial quadrupoles settings
                obj.reGetInitialQuadrupoleSettings();
            end
            
            % set some labels
            obj.observablesLabels = cell(obj.nBPMParameters,1);
            for i=1:obj.nBPMParameters
                obj.observablesLabels{i} = matlabDataAndSignalsHelper.decomposeSignal(obj.bpmParameters{i});
            end
            %
            obj.excitatorsLabels = cell(1+2*obj.nCorrectors,1);
            obj.excitatorsLabels{1} = 'QuadScaling';
            for i=1:obj.nCorrectors
                obj.excitatorsLabels{i+1} = matlabDataAndSignalsHelper.decomposeSignal(obj.correctorsSetParameters{i});
                obj.excitatorsLabels{i+1+obj.nCorrectors} = [obj.excitatorsLabels{i+1} 'xQuadScaling'];
            end
        end
        
        function output = bpmExtractionFunction(obj, dataStruct, ~)
            output = NaN(obj.nBPMParameters+obj.nBPMLogParameters, 1);
            
            for isignal=1:length(obj.bpmParameters)
                tmparray = matlabDataAndSignalsHelper.simpleExtractSingleSignal(dataStruct, obj.bpmParameters(isignal));
                try
                    output(isignal) = tmparray(obj.bpmCursorIndex);
                catch e
                    obj.errorOut(['Didn''t have good data for ', obj.bpmParameters{iSignal}]);
                    output(isignal) = NaN;
                end
            end
            
            for isignal=1:length(obj.bpmLogParameters)
                tmparray = matlabDataAndSignalsHelper.simpleExtractSingleSignal(dataStruct, obj.bpmLogParameters(isignal));
                try
                    output(obj.nBPMParameters+isignal) = tmparray(obj.bpmLogCursorIndex);
                catch e
                    obj.errorOut(['Didn''t have good data for ', obj.bpmLogParameters{iSignal}]);
                    output(obj.nBPMParameters+isignal) = NaN;
                end
            end
        end
        
        function collectedDataAveragingFunction(obj)
            % This function is responsible of averaging the previously
            % collected vectors:
            %   currentAveragingObservations
            %   currentAveragingExcitationReadOut
            %   currentAveragingExcitationSetReading
            % and make
            %   currentTmpObservation
            %   currentTmpObservationError
            %   currentTmpExcitationReadOut
            %   currentTmpExcitationReadOutError
            %   currentTmpExcitationSetReading
            %   currentTmpExcitationSetReadingError
            
            auxMeanBpms = mean(obj.currentAveragingObservations,2);
            auxMeanMagnetsGets = mean(obj.currentAveragingExcitationReadOut,2);
            auxMeanMagnetsSets = mean(obj.currentAveragingExcitationSetReading,2);
            
            auxStdBpms = std(obj.currentAveragingObservations,0,2);
            auxStdMagnetsGets = std(obj.currentAveragingExcitationReadOut,0,2);
            auxStdMagnetsSets = std(obj.currentAveragingExcitationSetReading,0,2);
            
            % split information
            auxMeanOrbit = auxMeanBpms(1:obj.nBPMParameters);
            auxStdOrbit = auxStdBpms(1:obj.nBPMParameters);
            auxMeanLog = auxMeanBpms((obj.nBPMParameters+1):end);
            auxStdLog = auxStdBpms((obj.nBPMParameters+1):end);
            %
            auxMeanCorrectorsGets = auxMeanMagnetsGets(1:obj.nCorrectors);
            auxStdCorrectorsGets = auxStdMagnetsGets(1:obj.nCorrectors);
            auxMeanQuadrupolesGets = auxMeanMagnetsGets((obj.nCorrectors+1):end);
            auxStdQuadrupolesGets = auxStdMagnetsGets((obj.nCorrectors+1):end);
            %
            auxMeanCorrectorsSets = auxMeanMagnetsSets(1:obj.nCorrectors);
            auxStdCorrectorsSets = auxStdMagnetsSets(1:obj.nCorrectors);
            auxMeanQuadrupolesSets = auxMeanMagnetsSets((obj.nCorrectors+1):end);
            auxStdQuadrupolesSets = auxStdMagnetsSets((obj.nCorrectors+1):end);
            
            % compute averaging scaling of the magnets respect to the
            % initial value
            auxQuadScalingGet = mean((auxMeanQuadrupolesGets - obj.initialQuadrupoleGetValues)./obj.initialQuadrupoleGetValues);
            auxQuadScalingGetStd = std((auxMeanQuadrupolesGets - obj.initialQuadrupoleGetValues)./obj.initialQuadrupoleGetValues);
            auxQuadScalingSet = mean((auxMeanQuadrupolesSets - obj.initialQuadrupoleSetValues)./obj.initialQuadrupoleSetValues);
            auxQuadScalingSetStd = std((auxMeanQuadrupolesSets - obj.initialQuadrupoleSetValues)./obj.initialQuadrupoleSetValues);

            % internally store some data for logging
            obj.internalLoggedBpmValues = auxMeanLog;
            obj.internalLoggedBpmStds = auxStdLog;
            
            % assemble output
            obj.currentTmpObservation = auxMeanOrbit(:); 
            obj.currentTmpExcitationReadOut = [auxQuadScalingGet; auxMeanCorrectorsGets(:); auxQuadScalingGet*auxMeanCorrectorsGets(:)];
            obj.currentTmpExcitationSetReading = [auxQuadScalingSet; auxMeanCorrectorsSets(:); auxQuadScalingSet*auxMeanCorrectorsSets(:)];
            %
            obj.currentTmpObservationError = auxStdOrbit(:);
            obj.currentTmpExcitationReadOutError = [auxQuadScalingGetStd; auxStdCorrectorsGets(:); sqrt(auxQuadScalingGetStd^2 + auxStdCorrectorsGets(:).^2)];
            obj.currentTmpExcitationSetReadingError = [auxQuadScalingSetStd; auxStdCorrectorsSets(:); sqrt(auxQuadScalingSetStd^2 + auxStdCorrectorsSets(:).^2)];
        end
        
        
        function outStruct = correctorFillFunction(obj, dataArray, ~)
            % the first value is the scaling of all the quadrupoles
            if obj.randomExcitationGain <= 0
                % reset initial quad values
                newQuadValues = obj.initialQuadrupoleSetValues;
            elseif abs(dataArray(1)) < obj.maxAbsQuadPercentChange
                newQuadValues = (dataArray(1)+1)*obj.initialQuadrupoleSetValues;
            else
                obj.warningOut(['Warning: you are trying to scale you quads more than ',...
                    num2str(obj.maxAbsQuadPercentChange),...
                    '%! I''l just set it to maximum allowed']);
                newQuadValues = (obj.maxAbsQuadPercentChange*sign(dataArray(1))+1)*obj.initialQuadrupoleSetValues;
            end
            
            % the new correctors values are stored afterwards
            newCorrValues = dataArray(2:(1+obj.nCorrectors));
            
            % all the rest should be 0... or not important... ;)
            
            % generate output structure now
            outStruct = struct;
            for i=1:obj.nQuadrupoles
                % leave unchanged correctors that didn't change enought.
                if abs(newQuadValues(i)-obj.initialQuadrupoleSetValues(i)) < obj.minMagnetChange
                    newQuadValues(i) = obj.initialQuadrupoleSetValues(i);
                end
                
                outStruct = matlabDataAndSignalsHelper.simpleSetvalue2Struct(obj.quadrupolesSetParameters(i), newQuadValues(i), outStruct);
            end
            for i=1:obj.nCorrectors
                % leave unchanged correctors that didn't change enought.
                if abs(newCorrValues(i)-obj.currentTmpExcitationSetReading(1+i)) < obj.minMagnetChange
                    newCorrValues(i) = obj.currentTmpExcitationSetReading(1+i);
                end
                
                outStruct = matlabDataAndSignalsHelper.simpleSetvalue2Struct(obj.correctorsSetParameters(i), newCorrValues(i), outStruct);
            end
        end     
        
        % personalize the computation of a correction excitation
        function varargout = computeFullCorrectionExcitation(obj, deltaObservationRequired)
            
            % in this case we don't use the linearFeedback solver, but the
            % locally defined on. Still, all the information to compute the
            % correction comes from the linearFeedback solver that has
            % been trained during the acquisition of the pulses.
            
            % redefine observation required adding "dispersion"
            % measurement            

            % reset observables weights inside solver      
            obj.mySolver.observablesWeights = obj.observablesWeights;

            % define local correctors limits
            tmpLowerLimit = obj.excitatorsLowerLimits-obj.currentTmpExcitationSetReading;
            tmpUpperLimit = obj.excitatorsUpperLimits-obj.currentTmpExcitationSetReading;
            
            % define best excitation
            % my best excitation here is the one that cancel dispersion.
            if obj.targetWantedExcitation
                
                auxObservation = -obj.mySolver.responseMatrix(:,1);
                auxRM = obj.mySolver.responseMatrix(:,2:(obj.nCorrectors+1)) + ...
                    obj.mySolver.responseMatrix(:,(obj.nCorrectors+2):end);
                
                % filter RM with only first two SV
                [au, as, av] = svd(auxRM);
                auxSumS=sum(diag(as));
                auxRetained = 0;
                for i=1:min(size(as))   
                    if auxRetained > obj.dispersionFreeRetainedVariance
                        as(i,i) = 0;
                    else
                        auxRetained = auxRetained + as(i,i)/auxSumS;
                    end
                end
                disp(['Retained ',num2str(auxRetained)]);
                
                auxSolver = matlabSolver(au*as*av');
                auxSolver.observablesWeights = obj.observablesWeights;
                auxSolver.excitationOptimizationWeight = 1;
                
                tmpBestExcitation = zeros(obj.nExcitators,1);
                try
                    tmpBestExcitation(2:(obj.nCorrectors+1)) = auxSolver.solve(auxObservation, ...
                        tmpLowerLimit(2:(obj.nCorrectors+1)), ...
                        tmpUpperLimit(2:(obj.nCorrectors+1)));
                catch e
                    obj.errorOut(['dispersionCorrection::computeFullCorrectionExcitation: impossible to compute target dispersion excitation:', e.message])
                end
                obj.wantedExcitation = tmpBestExcitation;
                tmpBestExcitation(2:(obj.nCorrectors+1)) = tmpBestExcitation(2:(obj.nCorrectors+1)) - obj.currentTmpExcitationSetReading(2:(obj.nCorrectors+1));
            else
                tmpBestExcitation = zeros(obj.nExcitators,1);
            end

            % disable now the other actuators
            obj.mySolver.disabledActuatorsIdx = [1, (obj.nCorrectors+2):(2*obj.nCorrectors+1)];
            
            % compute limited excitation to apply
            try
                % calculate full correction with excitator limits
                fullCorrectionExcitationToApply = obj.mySolver.solve(deltaObservationRequired, ...
                    tmpLowerLimit, ...
                    tmpUpperLimit, ...
                    tmpBestExcitation);
            catch e
                obj.errorOut(['dispersionCorrection::fullCorrectionExcitationToApply: impossible to compute correction excitation:', e.message])
                fullCorrectionExcitationToApply = zeros(obj.nExcitators,1);
            end
            varargout{1} = fullCorrectionExcitationToApply;
            
            % compute also unlimited excitation to apply if required
            if nargout == 2
                try
                    % calculate full correction without limits
                    fullCorrectionExcitationToApplyWithoutLimits = obj.mySolver.solve(deltaObservationRequired);
                catch e
                    obj.errorOut(['dispersionCorrection::fullCorrectionExcitationToApply: impossible to compute unlimited correction excitation:', e.message])
                    fullCorrectionExcitationToApplyWithoutLimits = zeros(obj.nExcitators,1);
                end
                varargout{2} = fullCorrectionExcitationToApplyWithoutLimits;
            end
        end
        
        % partially personalize the computation of a random excitation
        function randomExcitationToApply = computeRandomExcitation(obj)
            % standard with default function
            randomExcitationToApply = computeRandomExcitation@linearFeedback(obj);
            % in case of observables
            switch obj.randomExcitationStrategy
                case {obj.randomExcitationStrategyGaussianObservation, ...
                        obj.randomExcitationStrategySquareObservation, ...
                        obj.randomExcitationStrategyPlusMinusObservation}
                    % in this particular cases, normally the excitation
                    % would be always zero for the quad scaling factors...
                    % I will then adopt here a standard
                    % "randomExcitationStrategySquare" for those cases.
                    randomExcitationToApply(1) = obj.randomExcitationGain*obj.randomExcitationPattern(1)*2*(rand(1)-0.5);
            end
        end
        
        % personalize the preparation of datastruct to save data
        function data = prepareDataStructToSave(obj)
            % standard fields from linearFeedback class
            data = prepareDataStructToSave@linearFeedback(obj);
            
            % adding new fields
            data.internalLoggedBpmValues = obj.internalLoggedBpmValues;
            data.internalLoggedBpmStds = obj.internalLoggedBpmStds;
        end
        
        
        function reGetInitialQuadrupoleSettings(obj)
            auxGetStruct = obj.myJapc.JGetAll(obj.quadrupolesGetParameters);
            auxGetArray = orbitCorrection.generalCorrectorsExtractionFuntion(auxGetStruct, obj.quadrupolesGetParameters);
            auxSetStruct = obj.myJapc.JGetAll(obj.quadrupolesSetParameters);
            auxSetArray = orbitCorrection.generalCorrectorsExtractionFuntion(auxSetStruct, obj.quadrupolesSetParameters);
            
            % check that there are no null values
            if hasInfNaN(auxGetArray) || hasInfNaN(auxSetArray)
                error('Some error getting initial/reference quadrupoles settings.. Impossible to continue!');
            else
                obj.initialQuadrupoleGetStruct = auxGetStruct;
                obj.initialQuadrupoleGetValues = auxGetArray;
                obj.initialQuadrupoleSetStruct = auxSetStruct;
                obj.initialQuadrupoleSetValues = auxSetArray;
            end
        end
        function reSetInitialQuadrupoleSettings(obj)
            obj.myJapc.JSetAll(obj.quadrupolesSetParameters, obj.initialQuadrupoleSetStruct);
            pause(1)
            obj.myJapc.JSetAll(obj.quadrupolesSetParameters, obj.initialQuadrupoleSetStruct);
        end
    end
    
end

