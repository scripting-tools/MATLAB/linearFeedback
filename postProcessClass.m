classdef postProcessClass < handle
    %POSTPROCESSCLASS Summary of this class goes here
    %
    %obj=postProcessClass(directoryOrWildcard [, startIndex])
    % It creates a postProcessClass object from all the files
    % specified in directoryOrWildcard.
    % If startIndex is provided, then it will start taking only
    % files from this given index
    
    properties
        % where all the information is
        dataFiles
        dataStructs
        
        % observables related
        observations
        deltaObservationsFromPrevious
        wantedObservations
        
        % excitors related
        excitorsSettings
        excitorsReadings
        deltaExcitationReadingsFromPrevious
        
        % acquisition time in [ms]
        times
        
        % feedback status
        correctionGains
        excitationGains
    end
    
    methods
        function obj=postProcessClass(directoryOrWildcard, startIndex, endIndex)
            %obj=postProcessClass(directoryOrWildcard [, startIndex [, endIndex]])
            % It creates a postProcessClass object from all the files
            % specified in directoryOrWildcard.
            % If startIndex is provided, then it will start taking only
            % files from this given index (alphabetic order)
            % If endIndex is provided, then it will end to load at the
            % given file index.
            
            if nargin < 2
                startIndex = 1;
            end
            if nargin < 3
                endIndex = Inf;
            end
            
            % define list of files to read
            if isdir(directoryOrWildcard)
                obj.dataFiles=dir(fullfile(directoryOrWildcard,filesep,'*.mat'));
                dirpath=directoryOrWildcard;
            else
                obj.dataFiles=dir(fullfile(directoryOrWildcard));
                dirpath=fileparts(directoryOrWildcard);
            end
  
            % read all the files
            jFile=0;
            faultFileIndexes=[];
            
            if endIndex > length(obj.dataFiles)
                endIndex = length(obj.dataFiles);
            end
            for i=startIndex:endIndex
                try
                    aux=load(fullfile(dirpath, obj.dataFiles(i).name),'data');
                    jFile=jFile+1;
                    tmpAllStructs(jFile)=aux.data;
                catch e
                    disp(['Unable to extract "data" from ',obj.dataFiles(i).name,': ',...
                        e.message]);
                    faultFileIndexes(length(faultFileIndexes)+1)=i;
                end
            end
            
            if ~isempty(faultFileIndexes)
                obj.dataFiles(faultFileIndexes) = [];
            end
            
            disp([num2str(jFile), ' files loaded.']);
            obj.dataStructs = tmpAllStructs;
            
            % extract some data for convenience
            for i=1:jFile
                try
                    obj.observations(:,i)=obj.dataStructs(i).observables;
                    obj.excitorsReadings(:,i)=obj.dataStructs(i).excitorsReading;
                    obj.wantedObservations(:,i)=obj.dataStructs(i).wantedObservation;
                    obj.excitorsSettings(:,i)=obj.dataStructs(i).excitorsSetting;
                catch e
                    disp(['Something different happens from structure ',num2str(i-1),':'])
                    disp(e.message)
                    disp(['Using only the first ',num2str(i-1), ' structures/files'])
                    obj.dataStructs(i:end)=[];
                    obj.dataFiles(i:end)=[];
                    break
                end
            end
            
            %
            % extract some other stuff...
            %
            
            % some delta values
            obj.deltaObservationsFromPrevious = [obj.dataStructs.deltaObservationFromPrevious];
            obj.deltaExcitationReadingsFromPrevious = [obj.dataStructs.deltaExcitationFromPrevious];
            % some times
            obj.times = NaN(1,length(obj.dataStructs));
            for i=1:length(obj.dataStructs)
                obj.times(i) = obj.dataStructs(i).currentDataStruct.localTimeStamp_ms;
            end
            % some gains
            obj.correctionGains = [obj.dataStructs.CorrectionGain];
            obj.excitationGains = [obj.dataStructs.ExcitationGain];
        end
        
        function plotFeedbackGains(obj, indexes)
            if nargin < 2
                indexes=1:length(obj.dataStructs);
            end
            mytimes = (obj.times(indexes) - obj.times(indexes(1)))/60000;
            plot(mytimes, [obj.excitationGains(indexes)' obj.correctionGains(indexes)'],'.-');
            legend('Excitation','CorrectionGain');
            xlabel(['time from ',unixtime2HumanTime(obj.times(indexes(1))/1000),' [min]']);
            ylabel('Gains [a.u.]');
            grid
        end
        
        function plotFeedbackGainsOverIdx(obj, indexes)
            if nargin < 2
                indexes=1:length(obj.dataStructs);
            end
            plot(indexes, [obj.excitationGains(indexes)' obj.correctionGains(indexes)'],'.-');
            legend('Excitation','CorrectionGain');
            xlabel('Data idx [#]');
            ylabel('Gains [a.u.]');
            grid
        end
        
        function plotTimeEvolution(obj, objProperty, indexes)
            if nargin < 3
                indexes=1:length(obj.dataStructs);
            end
            
            % extract interesting matrix
            myMatrix = eval(['obj.',objProperty,'(:,[',num2str(indexes),']);']);
            mytimes = (obj.times(indexes) - obj.times(indexes(1)))/60000;

            % nicely plot it
            postProcessClass.plotEvolutionMatrix(mytimes, myMatrix)
            
            % set proper labels
            ylabel([objProperty,' idx [#]']);
            xlabel(['time from ',unixtime2HumanTime(obj.times(indexes(1))/1000),' [min]']);
        end
        
        function plotTimeEvolutionOverIdx(obj, objProperty, indexes)
            if nargin < 3
                indexes=1:length(obj.dataStructs);
            end
            
            % extract interesting matrix
            myMatrix = eval(['obj.',objProperty,'(:,[',num2str(indexes),']);']);
            mytimes = indexes;

            % nicely plot it
            postProcessClass.plotEvolutionMatrix(mytimes, myMatrix)
            
            % set proper labels
            ylabel([objProperty,' idx [#]']);
            xlabel('Data idx [#]');
        end
        
        function indexes = extractExcitedIndexes(obj)
            indexes = find(obj.excitationGains ~= 0);
        end
        function indexes = extractCorrectingIndexes(obj)
            indexes = find(obj.correctionGains ~= 0);
        end
        function indexes = extractObservationIndexes(obj)
            indexes = (obj.correctionGains == 0);
            indexes = find(obj.excitationGains(indexes) == 0);
        end
        
        function removeData(obj, indexes)
            obj.dataFiles(indexes) = [];
            obj.dataStructs(indexes) = [];
            
            obj.observations(:,indexes) = [];
            obj.wantedObservations(:,indexes) = [];
            
            obj.excitorsSettings(:,indexes) = [];
            obj.excitorsReadings(:,indexes) = [];
            
            obj.times(indexes) = [];
            
            obj.correctionGains(indexes) = [];
            obj.excitationGains(indexes) = [];
        end
    end
    
    methods (Static)
        function plotEvolutionMatrix(time, matrix)
            if (size(matrix,2) ~= length(time))
                error('plotEvolutionMatrix: I expect size(matrix)=[n,m] and length(time)=m.')
            end
            
            myy = 1:(size(matrix,1)+1);
            % extend matrix to correctly see it with pcolor
            matrix(end+1,:)=0;
            matrix(:,end+1)=0;
            
            overtime=mean(diff(time));
            time(end+1)=max(time)+overtime;
            
            pcolor(time, myy, matrix);
            set(gca,'YDir','reverse');
            shading flat;
            
            ylabel('idx #');
            xlabel('time [a.u.]');
            colorbar
        end
    end
end

