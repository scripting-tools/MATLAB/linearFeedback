classdef orbitCorrection < linearFeedback
    %orbitCorrection
    %   Child class of linearFeedback tuned for orbit correction
    
    properties(Constant)
        bpmTypeHorizontal = 1;
        bpmTypeVertical = 2;
        bpmTypeCurrent = 3;
        
        bpmTypeHorizontalStepDispersion = 4;
        bpmTypeVerticalStepDispersion = 5;
        
        bpmTypeHorizontalJitterDispersion = 6;
        bpmTypeVerticalJitterDispersion = 7;
        
        correctorTypeHorizontal = 1;
        correctorTypeVertical = 2;
        correctorTypeHorizontalAndVertical = 3;
    end
    
    properties
        bpmPositions = [];
        bpmTypes = [];
        
        bpmBeamHorizontalIdx = [];
        bpmBeamVerticalIdx = [];
        %
        bpmBeamHorizontalStepDispersionIdx = [];
        bpmBeamVerticalStepDispersionIdx = [];
        %
        bpmBeamHorizontalJitterDispersionIdx = [];
        bpmBeamVerticalJitterDispersionIdx = [];
        %
        bpmBeamCurrentIdx = [];
        
        correctorPositions = [];
        correctorTypes = [];
        correctorOnlyHorizontalIdx = [];
        correctorOnlyVerticalIdx = [];
        correctorHorizontalOrBothIdx = [];
        correctorVerticalOrBothIdx = [];
        correctorHorizontalAndVerticalIdx = [];
                
        nPositionBpm
        nPositionHBpm
        nPositionVBpm
        nStepDispersionBpm
        nStepDispersionHBpm
        nStepDispersionVBpm
        nJitterDispersionBpm
        nJitterDispersionHBpm
        nJitterDispersionVBpm
        nPositionAndDispersionBpm
        nCurrentBpm
        nAllKindBpm
        nCorrector
        
        % during data extraction, I'm taking into account this number of
        % points (usualy is the number of cursors...)
        nPointsToTakeIntoAccount = 8;
        
        % index point to use
        % they can be a single value or a vector of the same size as the
        % number of observables.
        %
        idxToUseNormalBPM = 1;
        idxToUseStepDispersionEnergyLow = 1;
        idxToUseStepDispersionEnergyHigh = 2;
        idxToUseJitterDispersion = 1;
        % new form of idx -> is a cell array of the same size as
        % nAllKindBpm, containing the indexes to use (with sign) for all of
        % them
        idxToUseBPMs = {};
        
        % used as reference for jitter dispersion measurements
        idxOfReferenceHorizontalJitterDispersionBPM = [];
        idxOfReferenceVerticalJitterDispersionBPM = [];
        nominalHorizontalDispersionAtReferenceJitterDispersionBPM = 1;
        nominalVerticalDispersionAtReferenceJitterDispersionBPM = 1;
        referenceValueAtReferenceHorizontalJitterDispersionBPM = NaN;
        referenceValueAtReferenceVerticalJitterDispersionBPM = NaN;
        
        % number of bpms to use for averaging (and necessary > 1 to have
        % dispersion from jitter!
        %nShotsToAverageAndCalculateJitterDispersion = 1;
        
        % max value of current bpms (abs value!) to consider it a good shot
        minBpmCurrentTollerated = 1.5;
        
        % corrector min delta to apply
        % it will not set a value to a corrector that is not different from
        % previous acquisition value
        minCorrectorChange = 0.01;
        
        % non-linear dispersion fit order
        % If > 0 then instead of doing a linear fit it does a non linear
        % one (polynomial of specified order) and gives the derivative in
        % zero as dispersion.
        jitterDispersionFitOrder = 1;
        
        % when doing averaging, sometimes there are outliers shots that we
        % don't want to take into account.
        % Normally outside 3 sigma is just a bad shot, but you can change
        % this value.
        nSigmaOutliers = 3;
        
        % last beam positions and current trasmission
        lastBeamHPosition = [];
        lastBeamHStepDispersion = [];
        lastBeamHJitterDispersion = [];
        lastBeamVPosition = [];
        lastBeamVStepDispersion = [];
        lastBeamVJitterDispersion = [];
        lastBeamSValue = [];
        lastBeamSTrasmission = [];
    end
    
    properties(Access=private)
    end
    
    methods
        % constructor
        function obj = orbitCorrection( ...
                bpmSignals, bpmPositions, bpmTypes, ...
                correctorSetSignals, correctorAcqSignals, correctorPositions, correctorTypes, ...
                offlineExecution)
            
            % new feature is this simulationModel... but not needed
            % normally
            if nargin == 7
                offlineExecution = false;
            end
            
            % check input sizes
            if length(bpmSignals) ~= length(bpmPositions)
                error('Wrong number of "bpmSignals" or "bpmPositions"')
            end
            if length(bpmSignals) ~= length(bpmTypes)
                error('Wrong number of "bpmSignals" or "bpmTypes"')
            end
            if length(correctorSetSignals) ~= length(correctorAcqSignals)
                error('Wrong number of "correctorSetSignals" or "correctorAcqSignals"')
            end
            if length(correctorSetSignals) ~= length(correctorPositions)
                error('Wrong number of "correctorSetSignals" or "correctorPositions"')
            end
            if length(correctorSetSignals) ~= length(correctorTypes)
                error('Wrong number of "correctorSetSignals" or "correctorTypes"')
            end
            
            % generate all indexes
            myCorrectorOnlyHorizontalIdx = find(correctorTypes == orbitCorrection.correctorTypeHorizontal);
            myCorrectorOnlyVerticalIdx = find(correctorTypes == orbitCorrection.correctorTypeVertical);
            myCorrectorHorizontalAndVerticalIdx = find(correctorTypes == orbitCorrection.correctorTypeHorizontalAndVertical);
            myCorrectorHorizontalOrBothIdx = find((correctorTypes == orbitCorrection.correctorTypeHorizontal) | ...
                (correctorTypes == orbitCorrection.correctorTypeHorizontalAndVertical));
            myCorrectorVerticalOrBothIdx = find((correctorTypes == orbitCorrection.correctorTypeVertical) | ...
                (correctorTypes == orbitCorrection.correctorTypeHorizontalAndVertical));
            
            myBpmBeamHorizontalIdx = find(bpmTypes == orbitCorrection.bpmTypeHorizontal);
            myBpmBeamVerticalIdx = find(bpmTypes == orbitCorrection.bpmTypeVertical);
            %
            mybpmBeamHorizontalStepDispersionIdx = find(bpmTypes == orbitCorrection.bpmTypeHorizontalStepDispersion);
            mybpmBeamVerticalStepDispersionIdx = find(bpmTypes == orbitCorrection.bpmTypeVerticalStepDispersion);
            %
            mybpmBeamHorizontalJitterDispersionIdx = find(bpmTypes == orbitCorrection.bpmTypeHorizontalJitterDispersion);
            mybpmBeamVerticalJitterDispersionIdx = find(bpmTypes == orbitCorrection.bpmTypeVerticalJitterDispersion);
            %
            myBpmBeamCurrentIdx = find(bpmTypes == orbitCorrection.bpmTypeCurrent);


            %
            % define some convenient number of elements
            %
            myNPositionHBpm = length(myBpmBeamHorizontalIdx);
            myNPositionVBpm = length(myBpmBeamVerticalIdx);
            myNPositionBpm = myNPositionHBpm + myNPositionVBpm;
            %
            myNStepDispersionHBpm = length(mybpmBeamHorizontalStepDispersionIdx);
            myNStepDispersionVBpm = length(mybpmBeamVerticalStepDispersionIdx);
            myNStepDispersionBpm = myNStepDispersionHBpm + myNStepDispersionVBpm;
            %
            myNJitterDispersionHBpm = length(mybpmBeamHorizontalJitterDispersionIdx);
            myNJitterDispersionVBpm = length(mybpmBeamVerticalJitterDispersionIdx);
            myNJitterDispersionBpm = myNJitterDispersionHBpm + myNJitterDispersionVBpm;
            %
            myNCurrentBpm = length(myBpmBeamCurrentIdx);
            %
            myNPositionAndDispersionBpm = myNPositionBpm + myNStepDispersionBpm + myNJitterDispersionBpm;
            myNAllKindBpm = myNPositionAndDispersionBpm + myNCurrentBpm;
            %
            myNCorrector = length(correctorPositions);
            
            
            %
            % build up the RM shape
            %
            myShapePosition = orbitCorrection.complexMatrixShapeGenerator(...
                correctorPositions, myCorrectorOnlyHorizontalIdx, myCorrectorOnlyVerticalIdx, ...
                bpmPositions, myBpmBeamHorizontalIdx, myBpmBeamVerticalIdx);
            %
            myShapeStepDispersion = orbitCorrection.complexMatrixShapeGenerator(...
                correctorPositions, myCorrectorOnlyHorizontalIdx, myCorrectorOnlyVerticalIdx, ...
                bpmPositions, mybpmBeamHorizontalStepDispersionIdx, mybpmBeamVerticalStepDispersionIdx);
            %
            myShapeJitterDispersion = orbitCorrection.complexMatrixShapeGenerator(...
                correctorPositions, myCorrectorOnlyHorizontalIdx, myCorrectorOnlyVerticalIdx, ...
                bpmPositions, mybpmBeamHorizontalJitterDispersionIdx, mybpmBeamVerticalJitterDispersionIdx);
            %
            % full RM shape,just the sum of three. If some values are
            % bigger than one.. something is wrong!
            %
            myShape = myShapePosition + myShapeStepDispersion + myShapeJitterDispersion;
            if max(max(myShape)) > 1
                imagesc(myShape)
                colorbar
                error('Something strange happening while building the RM shape. code BUG!')
            end
            
            myResponseMatrix = zeros(size(myShape));

            
            %
            % start real construction of the object.
            %
            obj=obj@linearFeedback('SCT.USER.SETUP', ...
                bpmSignals, '', ...
                ...
                correctorSetSignals, @orbitCorrection.generalCorrectorsExtractionFuntion, '', ...
                ...
                correctorAcqSignals, @orbitCorrection.generalCorrectorsExtractionFuntion, ...
                ...
                myResponseMatrix,...
                offlineExecution);
            
            
            % set proper matrixShape
            obj.mySolver.responseMatrixShape = myShape;
            
            % set proper handling functions (very specific to the object
            % itself
            obj.observablesExtractionFunction=@obj.bpmExtractionFunction;
            obj.excitatorsSetFillFunction=@obj.correctorFillFunction;
            
            % specify a custom monitor object:
            % it will have one java monitor for the observables
            % one for the set
% LET'S try to keep the default (with fastStrategy enabled!)            
%             if ~offlineExecution
%                 paramsToBeMonitored=[obj.observablesParameters(:);...
%                     obj.excitatorsReadOutParameters(:);...
%                     obj.excitatorsParameters(:)];
%                 myMonitorsIdxs=[...
%                     1*ones(length(obj.observablesParameters),1);...
%                     2*ones(length(obj.excitatorsReadOutParameters),1);...
%                     3*ones(length(obj.excitatorsParameters),1);...
%                     ];
%                 obj.myMonitor = matlabJapcMonitor(obj.cycleName, paramsToBeMonitored, @obj.coreFunction, ...
%                     'orbitCorrection data',myMonitorsIdxs);
%             end
            obj.myMonitor.useFastStrategy(1);

            % defining a standard wantedOrbit
            obj.wantedObservation=zeros(myNAllKindBpm,1);
            
            % define standard observalbes limits
            obj.observablesLowerLimits = -15*ones(myNAllKindBpm,1);
            obj.observablesUpperLimits = 15*ones(myNAllKindBpm,1);
            % for current BPMs, normally we need to have current < 0
            obj.observablesUpperLimits(myBpmBeamCurrentIdx) = 0;
            
            % defining a standard correctors limits
            obj.excitatorsLowerLimits = -10*ones(myNCorrector,1);
            obj.excitatorsUpperLimits = 10*ones(myNCorrector,1);
            
            % define standard weights and randomPattern
            obj.randomExcitationPattern = ones(myNCorrector,1);
            obj.observablesWeights = ones(myNAllKindBpm,1);
            obj.observablesWeights(myBpmBeamCurrentIdx) = 0;
            
            % save all initial variables...
            obj.bpmPositions = bpmPositions;
            obj.bpmTypes = bpmTypes;
            obj.correctorPositions = correctorPositions;
            obj.correctorTypes = correctorTypes;
            
            obj.bpmBeamHorizontalIdx = myBpmBeamHorizontalIdx;
            obj.bpmBeamVerticalIdx = myBpmBeamVerticalIdx;
            %
            obj.bpmBeamHorizontalStepDispersionIdx = mybpmBeamHorizontalStepDispersionIdx;
            obj.bpmBeamVerticalStepDispersionIdx = mybpmBeamVerticalStepDispersionIdx;
            %
            obj.bpmBeamHorizontalJitterDispersionIdx = mybpmBeamHorizontalJitterDispersionIdx;
            obj.bpmBeamVerticalJitterDispersionIdx = mybpmBeamVerticalJitterDispersionIdx;
            %
            obj.bpmBeamCurrentIdx = myBpmBeamCurrentIdx;
            %
            obj.correctorOnlyHorizontalIdx = myCorrectorOnlyHorizontalIdx;
            obj.correctorOnlyVerticalIdx = myCorrectorOnlyVerticalIdx;
            obj.correctorHorizontalOrBothIdx = myCorrectorHorizontalOrBothIdx;
            obj.correctorVerticalOrBothIdx = myCorrectorVerticalOrBothIdx;
            obj.correctorHorizontalAndVerticalIdx = myCorrectorHorizontalAndVerticalIdx;
            
            obj.nPositionBpm = myNPositionBpm;
            obj.nPositionHBpm = myNPositionHBpm;
            obj.nPositionVBpm = myNPositionVBpm;
            obj.nStepDispersionBpm = myNStepDispersionBpm;
            obj.nStepDispersionHBpm = myNStepDispersionHBpm;
            obj.nStepDispersionVBpm = myNStepDispersionVBpm;
            obj.nJitterDispersionBpm = myNJitterDispersionBpm;
            obj.nJitterDispersionHBpm = myNJitterDispersionHBpm;
            obj.nJitterDispersionVBpm = myNJitterDispersionVBpm;
            obj.nPositionAndDispersionBpm = myNPositionAndDispersionBpm;
            obj.nCurrentBpm = myNCurrentBpm;
            obj.nCorrector = myNCorrector;
            obj.nAllKindBpm = myNAllKindBpm;
            
            
            % set some labels
            obj.observablesLabels = cell(myNAllKindBpm,1);
            for i=1:myNAllKindBpm
                obj.observablesLabels{i} = matlabDataAndSignalsHelper.decomposeSignal(bpmSignals{i});
            end
            %
            obj.excitatorsLabels = cell(myNCorrector,1);
            for i=1:myNCorrector
                obj.excitatorsLabels{i} = matlabDataAndSignalsHelper.decomposeSignal(correctorSetSignals{i});
            end
        end

        function data = prepareDataStructToSave(obj)
            % standard fields from linearFeedback class
            data = prepareDataStructToSave@linearFeedback(obj);
            
            % adding new fields
            data.bpmPositions = obj.bpmPositions;
            data.bpmTypes = obj.bpmTypes;
            data.correctorPositions = obj.correctorPositions;
            data.correctorTypes = obj.correctorTypes;
            data.minCorrectorChange = obj.minCorrectorChange;
            
            data.lastBeamHPosition = obj.lastBeamHPosition;
            data.lastBeamHStepDispersion = obj.lastBeamHStepDispersion;
            data.lastBeamHJitterDispersion = obj.lastBeamHJitterDispersion;
            data.lastBeamVPosition = obj.lastBeamVPosition;
            data.lastBeamVStepDispersion = obj.lastBeamVStepDispersion;
            data.lastBeamVJitterDispersion = obj.lastBeamVJitterDispersion;
            
            data.lastBeamSValue = obj.lastBeamSValue;
            data.lastBeamSTrasmission = obj.lastBeamSTrasmission;
        end
        
        function collectedDataAveragingFunction(obj)
            % This function needs to be rewritten compared to standard
            % behavior. Instead of only doing a mean over acquired data, it
            % actually extract the proper orbits, currents, dispersions,
            % etc...

            %
            % for magnets it is the same... so let's just do it.
            %
            obj.currentTmpExcitationReadOut = mean(obj.currentAveragingExcitationReadOut,2);
            obj.currentTmpExcitationSetReading = mean(obj.currentAveragingExcitationSetReading,2);
            
            obj.currentTmpExcitationReadOutError = std(obj.currentAveragingExcitationReadOut,0,2);
            obj.currentTmpExcitationSetReadingError = std(obj.currentAveragingExcitationSetReading,0,2);
            
            
            %
            % for observables (BPMs/dispersion) not!
            %
            % so not anymore like:
            %obj.currentTmpObservation = mean(obj.currentAveragingObservations,2);
            %obj.currentTmpObservationError = std(obj.currentAveragingObservations,0,2);
            % but....
            
            %
            % first we cut outliers, i.e. more than 3 sigmas away shots
            % 
            auxAllAveragingObservations = obj.currentAveragingObservations;
            auxMEAN = mean(auxAllAveragingObservations,2);
            auxSTD  = std(auxAllAveragingObservations,1,2);
            auxNotGoodIDX  = abs(auxAllAveragingObservations - repmat(auxMEAN(:),1,size(auxAllAveragingObservations,2))) ...
                > repmat( obj.nSigmaOutliers * auxSTD(:),1,size(auxAllAveragingObservations,2));
            auxNotGoodIDX  = sum(auxNotGoodIDX) > 0;
            auxAllAveragingObservations(:,auxNotGoodIDX) = [];
            if (isempty(auxAllAveragingObservations))
                disp('All the observations are considered as outliers for some reason...');
                auxAllAveragingObservations = NaN(size(obj.currentAveragingObservations,1),1);
            end
            
            % generate empty outputs.
            auxObservationOutput = NaN(obj.nAllKindBpm, 1);
            auxObservationOutputError = NaN(obj.nAllKindBpm, 1);
            
            % beam positions and step dispersion I just need to do an
            % averaging
            auxObservationOutput(obj.bpmBeamHorizontalIdx) = mean(auxAllAveragingObservations(obj.bpmBeamHorizontalIdx,:),2);
            auxObservationOutput(obj.bpmBeamVerticalIdx) = mean(auxAllAveragingObservations(obj.bpmBeamVerticalIdx,:),2);
            auxObservationOutput(obj.bpmBeamHorizontalStepDispersionIdx) = mean(auxAllAveragingObservations(obj.bpmBeamHorizontalStepDispersionIdx,:),2);
            auxObservationOutput(obj.bpmBeamVerticalStepDispersionIdx) = mean(auxAllAveragingObservations(obj.bpmBeamVerticalStepDispersionIdx,:),2);
            %
            auxObservationOutputError(obj.bpmBeamHorizontalIdx) = std(auxAllAveragingObservations(obj.bpmBeamHorizontalIdx,:),0,2);
            auxObservationOutputError(obj.bpmBeamVerticalIdx) = std(auxAllAveragingObservations(obj.bpmBeamVerticalIdx,:),0,2);
            auxObservationOutputError(obj.bpmBeamHorizontalStepDispersionIdx) = std(auxAllAveragingObservations(obj.bpmBeamHorizontalStepDispersionIdx,:),0,2);
            auxObservationOutputError(obj.bpmBeamVerticalStepDispersionIdx) = std(auxAllAveragingObservations(obj.bpmBeamVerticalStepDispersionIdx,:),0,2);
            
            % similar is for the beam current
            auxObservationOutput(obj.bpmBeamCurrentIdx) = mean(auxAllAveragingObservations(obj.bpmBeamCurrentIdx,:),2);
            auxObservationOutputError(obj.bpmBeamCurrentIdx) = std(auxAllAveragingObservations(obj.bpmBeamCurrentIdx,:),0,2);

            
            %
            % for the jitter dispersion it is a bit more tricky
            %
            % prepare the jitter only matrixes
            auxJitterH = auxAllAveragingObservations(obj.bpmBeamHorizontalJitterDispersionIdx,:);
            auxJitterV = auxAllAveragingObservations(obj.bpmBeamVerticalJitterDispersionIdx,:);
            
            % mean normalization is always needed.
            auxMeanHJitter = mean(auxJitterH,2);
            auxMeanVJitter = mean(auxJitterV,2);
            auxJitterH = auxJitterH - repmat(auxMeanHJitter,1,size(auxJitterH,2));
            auxJitterV = auxJitterV - repmat(auxMeanVJitter,1,size(auxJitterV,2));

            
            
            % Horizontal
            if ~isempty(obj.bpmBeamHorizontalJitterDispersionIdx)
                if ~isempty(obj.idxOfReferenceHorizontalJitterDispersionBPM)
                    % use same as onlineDispersion class.
                    auxRefJitter = auxJitterH(obj.idxOfReferenceHorizontalJitterDispersionBPM,:);
                    if obj.jitterDispersionFitOrder <= 1
                        [jitterDispersionH, jitterDispersionHError, retainedVariance] = ...
                            onlineDispersion.computeMatrixToArrayFit(auxJitterH, auxRefJitter);
                    else
                        % for second order it might become important also
                        % "where" the derivative is give.
                        if hasInfNaN(obj.referenceValueAtReferenceHorizontalJitterDispersionBPM)
                            % standard
                            [jitterDispersionH, jitterDispersionHError, retainedVariance] = ...
                            onlineDispersion.computeMatrixToArrayNonLinToLinFit(auxJitterH, auxRefJitter, ...
                            obj.jitterDispersionFitOrder);
                        else
                            % set back the mean position at ref position (it matters!)
                            auxRefJitter = auxRefJitter + ...
                                auxMeanHJitter(obj.idxOfReferenceHorizontalJitterDispersionBPM);
                            [jitterDispersionH, jitterDispersionHError, retainedVariance] = ...
                                onlineDispersion.computeMatrixToArrayNonLinToLinFit(auxJitterH, auxRefJitter, ...
                                obj.jitterDispersionFitOrder, ...
                                obj.referenceValueAtReferenceHorizontalJitterDispersionBPM);
                        end
                    end
                    % normalise
                    auxNormalisation = obj.nominalHorizontalDispersionAtReferenceJitterDispersionBPM/jitterDispersionH(obj.idxOfReferenceHorizontalJitterDispersionBPM);
                    jitterDispersionH = jitterDispersionH*auxNormalisation;
                    jitterDispersionHError = jitterDispersionHError*auxNormalisation;
                else
                    % do a Principal Component Analysis of the jitter, and keep
                    % only one eigendirection
                    obj.warningOut('no reference BPM selected for H Jitter dispersion. Using PCA')
                    
                    % mean normalization already done.
                    [jitterDispersionH, jitterDispersionHError, ~, retainedVariance] = ...
                        onlineDispersion.computeDispersionAsPCA(auxJitterH);
                    
                    % assuming the jitter is due to a 5/1000 energy
                    % jitter. We also want the measurement in [m], so we
                    % moltiply per 10^-3 (cause our jitter is in mm so dispersion comes out in mm!).
                    jitterDispersionH = jitterDispersionH.*(10^-3/0.005);
                    
                    % plus, since the sign is not well defined, I hope the mean
                    % dispersion is well defined + or -...
                    jitterDispersionH = jitterDispersionH*sign(mean(jitterDispersionH));
                    
                    % so the error
                    jitterDispersionHError = jitterDispersionHError.*(10^-3/0.005);
                end
                %
                if retainedVariance < 0.5
                    obj.warningOut('The horizontal jitter dispersion measurement is NOT good! More than 50% variance is coming for a different source than energy!');
                end
                %
                auxObservationOutput(obj.bpmBeamHorizontalJitterDispersionIdx) = jitterDispersionH;
                auxObservationOutputError(obj.bpmBeamHorizontalJitterDispersionIdx) = jitterDispersionHError;
            end
            
            %
            % Vertical -> same as horizontal, but for cleanliness I avoid comments
            %
            if ~isempty(obj.bpmBeamVerticalJitterDispersionIdx)
                if ~isempty(obj.idxOfReferenceVerticalJitterDispersionBPM)
                    % use same as onlineDispersion class.
                    auxRefJitter = auxJitterV(obj.idxOfReferenceVerticalJitterDispersionBPM,:);
                    if obj.jitterDispersionFitOrder <= 1
                        [jitterDispersionV, jitterDispersionVError, retainedVariance] = ...
                            onlineDispersion.computeMatrixToArrayFit(auxJitterV, auxRefJitter);
                    else
                        if hasInfNaN(obj.referenceValueAtReferenceVerticalJitterDispersionBPM)
                            [jitterDispersionV, jitterDispersionVError, retainedVariance] = ...
                            onlineDispersion.computeMatrixToArrayNonLinToLinFit(auxJitterV, auxRefJitter, obj.jitterDispersionFitOrder);
                        else
                            % set back the mean position at ref position (it matters!)
                            auxRefJitter = auxRefJitter + ...
                                auxMeanVJitter(obj.idxOfReferenceVerticalJitterDispersionBPM);
                            [jitterDispersionV, jitterDispersionVError, retainedVariance] = ...
                                onlineDispersion.computeMatrixToArrayNonLinToLinFit(auxJitterV, auxRefJitter, ...
                                obj.jitterDispersionFitOrder, ...
                                obj.referenceValueAtReferenceVerticalJitterDispersionBPM);
                        end
                    end
                    % normalise
                    auxNormalisation = obj.nominalVerticalDispersionAtReferenceJitterDispersionBPM/jitterDispersionH(obj.idxOfReferenceVerticalJitterDispersionBPM);
                    jitterDispersionV = jitterDispersionV*auxNormalisation;
                    jitterDispersionVError = jitterDispersionVError*auxNormalisation;
                else
                    obj.warningOut('no reference BPM selected for V Jitter dispersion. Using PCA')
                    [jitterDispersionV, jitterDispersionVError, ~, retainedVariance] = onlineDispersion.computeDispersionAsPCA(auxJitterV);
                    jitterDispersionV = jitterDispersionV.*(10^-3/0.005);
                    jitterDispersionVError = jitterDispersionVError.*(10^-3/0.005);
                    jitterDispersionV = jitterDispersionV*sign(mean(jitterDispersionV));
                end
                %
                if retainedVariance < 0.5
                    obj.warningOut('The vertical jitter dispersion measurement is NOT good! More than 50% variance is coming for a different source than energy!');
                end
                %
                auxObservationOutput(obj.bpmBeamVerticalJitterDispersionIdx) = jitterDispersionV;
                auxObservationOutputError(obj.bpmBeamVerticalJitterDispersionIdx) = jitterDispersionVError;
            end
            
            %
            % save some sub-arrays in appropriate vectors of this class
            %
            obj.lastBeamHPosition = auxObservationOutput(obj.bpmBeamHorizontalIdx);
            obj.lastBeamVPosition = auxObservationOutput(obj.bpmBeamVerticalIdx);
            obj.lastBeamHStepDispersion = auxObservationOutput(obj.bpmBeamHorizontalStepDispersionIdx);
            obj.lastBeamVStepDispersion = auxObservationOutput(obj.bpmBeamVerticalStepDispersionIdx);
            obj.lastBeamHJitterDispersion = auxObservationOutput(obj.bpmBeamHorizontalJitterDispersionIdx);
            obj.lastBeamVJitterDispersion = auxObservationOutput(obj.bpmBeamVerticalJitterDispersionIdx);
            %
            obj.lastBeamSValue = auxObservationOutput(obj.bpmBeamCurrentIdx);
            obj.lastBeamSTrasmission = orbitCorrection.computeTrasmission(obj.lastBeamSValue, obj.bpmPositions(obj.bpmBeamCurrentIdx));
            
            % save current observation in the right place...
            obj.currentTmpObservation = auxObservationOutput;
            obj.currentTmpObservationError = auxObservationOutputError;
        end
        
        function outStruct = correctorFillFunction(obj, dataArray, signals)
            outStruct = struct;
            if (length(dataArray) ~= (length(signals)))
                error('Error correctorFill function: different length of signals and dataArray')
            end
            for i=1:length(signals)
                % leave unchanged correctors that didn't change enought.
                if abs(dataArray(i)-obj.currentTmpExcitationSetReading(i)) < obj.minCorrectorChange
                    dataArray(i) = obj.currentTmpExcitationSetReading(i);
                end
                
                outStruct = matlabDataAndSignalsHelper.simpleSetvalue2Struct(signals(i), dataArray(i), outStruct);
            end
        end
        
        function output = bpmExtractionFunction( obj, dataStruct, signals)
            % default output is just a NaN if something is wrong... 
            output = NaN;
            
            % extract all BPMs data
            auxBPMReadings = obj.simpleBpmExtractAllFunction(dataStruct, signals);
            
            % really compute new data
            if isempty(obj.idxToUseBPMs) || length(obj.idxToUseBPMs) ~= obj.nAllKindBpm
                % use old fashion
                auxArrayH = mean(auxBPMReadings(obj.bpmBeamHorizontalIdx, obj.idxToUseNormalBPM),2);
                auxArrayV = mean(auxBPMReadings(obj.bpmBeamVerticalIdx, obj.idxToUseNormalBPM),2);
                auxArrayHLow = mean(auxBPMReadings(obj.bpmBeamHorizontalStepDispersionIdx, obj.idxToUseStepDispersionEnergyLow),2);
                auxArrayHHigh = mean(auxBPMReadings(obj.bpmBeamHorizontalStepDispersionIdx, obj.idxToUseStepDispersionEnergyHigh),2);
                auxArrayVLow = mean(auxBPMReadings(obj.bpmBeamVerticalStepDispersionIdx, obj.idxToUseStepDispersionEnergyLow),2);
                auxArrayVHigh = mean(auxBPMReadings(obj.bpmBeamVerticalStepDispersionIdx, obj.idxToUseStepDispersionEnergyHigh),2);
                auxArrayHJitter = mean(auxBPMReadings(obj.bpmBeamHorizontalJitterDispersionIdx, obj.idxToUseJitterDispersion),2);
                auxArrayVJitter = mean(auxBPMReadings(obj.bpmBeamVerticalJitterDispersionIdx, obj.idxToUseJitterDispersion),2);
                auxArrayS = mean(auxBPMReadings(obj.bpmBeamCurrentIdx, obj.idxToUseNormalBPM),2);
            else
                %disp('debug: new way of extracting data!')
                auxBPMReadingsElaborated = nan(length(obj.nAllKindBpm),1);
                for i=1:obj.nAllKindBpm
                    auxIdxes = obj.idxToUseBPMs{i};
                    auxIdxPlus = auxIdxes(auxIdxes > 0);
                    auxIdxMinus = -auxIdxes(auxIdxes < 0);
                    if ~isempty(auxIdxPlus)
                        auxBPMReadingsElaborated(i) = mean(auxBPMReadings(i,auxIdxPlus));
                    end
                    if ~isempty(auxIdxMinus)
                        auxBPMReadingsElaborated(i) = auxBPMReadingsElaborated(i) - mean(auxBPMReadings(i,auxIdxMinus));
                    end
                end
                % now store everything as old way
                auxArrayH = auxBPMReadingsElaborated(obj.bpmBeamHorizontalIdx);
                auxArrayV = auxBPMReadingsElaborated(obj.bpmBeamVerticalIdx);
                auxArrayHHigh = auxBPMReadingsElaborated(obj.bpmBeamHorizontalStepDispersionIdx);
                auxArrayHLow = zeros(size(auxArrayHHigh));
                auxArrayVHigh = auxBPMReadingsElaborated(obj.bpmBeamVerticalStepDispersionIdx);
                auxArrayVLow = zeros(size(auxArrayVHigh));
                auxArrayHJitter = auxBPMReadingsElaborated(obj.bpmBeamHorizontalJitterDispersionIdx);
                auxArrayVJitter = auxBPMReadingsElaborated(obj.bpmBeamVerticalJitterDispersionIdx);
                auxArrayS = auxBPMReadingsElaborated(obj.bpmBeamCurrentIdx);
            end
            
            % check InF/NaN
            if hasInfNaN(auxArrayH)
                auxidxnan = isnan(auxArrayH);
                auxNames = signals(obj.bpmBeamHorizontalIdx(auxidxnan));
                
                obj.errorOut('There are some NaN/Inf in this shot on some normal H position bpm. Skip data:');
                obj.errorOut(['First is: ',auxNames{1}]);
                return
            end
            
            if hasInfNaN(auxArrayV)
                auxidxnan = isnan(auxArrayV);
                auxNames = signals(obj.bpmBeamVerticalIdx(auxidxnan));
                
                obj.errorOut('There are some NaN/Inf in this shot on some normal V position bpm. Skip data:');
                obj.errorOut(['First is: ',auxNames{1}]);
                return
            end
            %
            if hasInfNaN(auxArrayHLow) || hasInfNaN(auxArrayHHigh)
                auxidxnan = isnan(auxArrayHLow) | ...
                    isnan(auxArrayHHigh);
                auxNames = signals(obj.bpmBeamHorizontalStepDispersionIdx(auxidxnan));
                
                obj.errorOut('There are some NaN/Inf in this shot on some H dispersion-step bpm. Skip data.');
                obj.errorOut(['First is: ',auxNames{1}]);
                return
            end
            if hasInfNaN(auxArrayVLow) || hasInfNaN(auxArrayVHigh)
                auxidxnan = isnan(auxArrayVLow) | ...
                    isnan(auxArrayVHigh);
                auxNames = signals(obj.bpmBeamVerticalStepDispersionIdx(auxidxnan));
                
                obj.errorOut('There are some NaN/Inf in this shot on some H dispersion-step bpm. Skip data.');
                obj.errorOut(['First is: ',auxNames{1}]);
                return
            end
            %
            if hasInfNaN(auxArrayHJitter)
                auxidxnan = isnan(auxArrayHJitter);
                auxNames = signals(obj.bpmBeamHorizontalJitterDispersionIdx(auxidxnan));
                
                obj.errorOut('There are some NaN/Inf in this shot on some H dispersion-jitter bpm. Skip data.');
                obj.errorOut(['First is: ',auxNames{1}]);
                return
            end
            if hasInfNaN(auxArrayVJitter)
                auxidxnan = isnan(auxArrayVJitter);
                auxNames = signals(obj.bpmBeamVerticalJitterDispersionIdx(auxidxnan));
                
                obj.errorOut('There are some NaN/Inf in this shot on some V dispersion-jitter bpm. Skip data.');
                obj.errorOut(['First is: ',auxNames{1}]);
                return
            end

            % check beam current
            for i=1:length(auxArrayS)
                if (abs(auxArrayS(i)) < abs(obj.minBpmCurrentTollerated))
                    obj.warningOut(['Not enought current at BPM ',signals{obj.bpmBeamCurrentIdx(i)},': ', num2str(abs(auxArrayS(i))), ...
                        ' < ', num2str(abs(obj.minBpmCurrentTollerated))]);
                    return
                end
            end
            
            % if we have enabled jitterDispersion measurement, we need
            % linearFeedback to be set to have a minimum of few shots (10?!) to
            % compute dispersion. So force it!
            if obj.nJitterDispersionBpm > 0 && obj.nAveragingShots < 10
                obj.nAveragingShots = 10;
                obj.warningOut('Since you want to compute some "jitterDispersion" I need at least 10 "averagingShots". I''ll set it.');
            end
            
            output = NaN(obj.nAllKindBpm,1);
            output(obj.bpmBeamCurrentIdx) = auxArrayS;
            %
            output(obj.bpmBeamHorizontalIdx) = auxArrayH;
            output(obj.bpmBeamVerticalIdx) = auxArrayV;
            %
            output(obj.bpmBeamHorizontalStepDispersionIdx) = auxArrayHHigh - auxArrayHLow;
            output(obj.bpmBeamVerticalStepDispersionIdx) = auxArrayVHigh - auxArrayVLow;
            %
            output(obj.bpmBeamHorizontalJitterDispersionIdx) = auxArrayHJitter;
            output(obj.bpmBeamVerticalJitterDispersionIdx) = auxArrayVJitter;
            %
        end
        
        function output = simpleBpmExtractAllFunction( obj, dataStruct, signals )
            output = NaN(length(signals), obj.nPointsToTakeIntoAccount);
            
            for isignal=1:length(signals)
                tmparray = matlabDataAndSignalsHelper.simpleExtractSingleSignal(dataStruct, signals(isignal));
                tmpLength = length(tmparray);
                
                for iturn=1:obj.nPointsToTakeIntoAccount
                    if tmpLength >= iturn
                        output(isignal,iturn)=tmparray(iturn);
                    else
                        obj.printOut(['Not enought points for ',signals(isignal)]);
                    end
                end
            end
        end
        
        function setWantedOrbitFromReference(obj)
            % it will get the reference orbit from the reference database
            
            for iBPM=[obj.bpmBeamHorizontalIdx(:); obj.bpmBeamVerticalIdx(:); obj.bpmBeamCurrentIdx(:)]'
                try
                    auxVector = obj.myJapc.JGetRefFesaSignal(obj.observablesParameters{iBPM});
                    obj.wantedObservation(iBPM) = auxVector(1);
                catch e
                    disp(['Impossible to get Ref orbit for ',obj.observablesParameters{iBPM},...
                        ': ',e.message]);
                end
            end
        end
    end
    
    methods(Static)
        function shape = simpleMatrixShapeGenerator(correctorPositions, bpmPositions)
            % simpleMatrixShapeGenerator(correctorPositions, bpmPositions)
            % it just create a matrix such that correctors acts only on
            % later BPMs.
            shape = zeros(length(bpmPositions), length(correctorPositions));
            for i=1:length(bpmPositions)
                shape(i,:) = correctorPositions < bpmPositions(i);
            end
        end
        
        function shape = complexMatrixShapeGenerator(...
                correctorPositions, hOnlyCorrectorIdx, vOnlyCorrectorIdx, ...
                bpmPositions, hBpmIdx, vBpmIdx)
            %
            shape = zeros(length(bpmPositions), length(correctorPositions));
                       
            % horizontal bpm plane
            for i=1:length(bpmPositions(hBpmIdx))
                auxLogicalIndex = correctorPositions < bpmPositions(hBpmIdx(i));
                auxLogicalIndex(vOnlyCorrectorIdx) = 0; % vertical only correctors don't interact with horizontal bpms
                shape(hBpmIdx(i),auxLogicalIndex) = 1;
            end
            
            % vertical bpm plane
            for i=1:length(bpmPositions(vBpmIdx))
                auxLogicalIndex = correctorPositions < bpmPositions(vBpmIdx(i));
                auxLogicalIndex(hOnlyCorrectorIdx) = 0; % horizontal only correctors don't interact with vertical bpms
                shape(vBpmIdx(i),auxLogicalIndex) = 1;
            end
        end
        
        function responseMatrix = oneToOneResponseMatrixGenerator(matrixShape)
            responseMatrix = matrixShape;
            for icor=1:(size(responseMatrix,2))
                for ibpm=1:(size(responseMatrix,1)-1)
                    if responseMatrix(ibpm,icor) ~= 0
                        responseMatrix((ibpm+1):end,icor) = 0;
                        break
                    end
                end
            end
        end
        
        function output = generalCorrectorsExtractionFuntion(dataStruct, signals)
            aux = zeros(length(signals),1);
            for i=1:length(signals)
                tmparray = matlabDataAndSignalsHelper.simpleExtractSingleSignal(dataStruct, signals(i));
                if length(tmparray) > 1
                    disp(['Strange! I obtained an array of values for corrector: ',...
                        signals(i),'. Check what you are doing: I am taking the first value.']);
                end
                aux(i) = tmparray(1);
            end
            output = aux;
            
            % NaN check
            if hasInfNaN(output)
                auxidxnan = isnan(output);
                auxNames = signals(auxidxnan); 
                auxValues = output(auxidxnan); 
   
                disp('No good value for some corrector acquisition. Skip data.');
                disp(['  First is: ',auxNames{1}, ' [',num2str(auxValues(1)),']']);
                
                output = NaN;
                return
            end
        end
        
        function trasmission = computeTrasmission(currentValues, currentBpmPositions)
            % if no positions are given, means that currentValues is
            % already ordered
            if nargin == 1
                currentBpmPositions = 1:length(currentValues);
            end
            
            if length(currentValues) ~= length(currentBpmPositions)
                error('currentValues length and bpmPositions length must agree!')
            end
            
            trasmission = ones(length(currentBpmPositions),1);
            [~,idx] = sort(currentBpmPositions);
            
            for i=1:(length(idx)-1) % for the last bpm, the trasmission is 1
                trasmission(idx(i)) = currentValues(idx(i+1))/currentValues(idx(i));
            end
        end
    end
end

