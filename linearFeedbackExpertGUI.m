function varargout = linearFeedbackExpertGUI(varargin)
% LINEARFEEDBACKEXPERTGUI MATLAB code for linearFeedbackExpertGUI.fig
%      LINEARFEEDBACKEXPERTGUI, by itself, creates a new LINEARFEEDBACKEXPERTGUI or raises the existing
%      singleton*.
%
%      H = LINEARFEEDBACKEXPERTGUI returns the handle to a new LINEARFEEDBACKEXPERTGUI or the handle to
%      the existing singleton*.
%
%      LINEARFEEDBACKEXPERTGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in LINEARFEEDBACKEXPERTGUI.M with the given input arguments.
%
%      LINEARFEEDBACKEXPERTGUI('Property','Value',...) creates a new LINEARFEEDBACKEXPERTGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before linearFeedbackExpertGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to linearFeedbackExpertGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help linearFeedbackExpertGUI

% Last Modified by GUIDE v2.5 13-Mar-2015 13:27:01

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @linearFeedbackExpertGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @linearFeedbackExpertGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before linearFeedbackExpertGUI is made visible.
function linearFeedbackExpertGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to linearFeedbackExpertGUI (see VARARGIN)

%%%%%%%%%%%% BEGIN my code
handles = guidata(hObject);
% initialize table
if ~isempty(varargin) && ...
        isobject(varargin{1}) && iscell(varargin{2})
    handles.myReferenceObject = varargin{1};
    handles.myParameters = varargin{2};
else
    error(['Error! Wrong number of inputs for linearFeedbackExpertGUI!',...
        'I expect a reference to a class (isobject = true) as first argument, ',...
        'a cell array of properties as second argument.'])
end
%populateParametersTable
% set table rows
set(handles.parametersTable, 'RowName', handles.myParameters);

% update values inside table
updateParametersTable(handles);

%%%%%%%%%%%% END my code

% Choose default command line output for linearFeedbackExpertGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes linearFeedbackExpertGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


function updateParametersTable(handles)
    % updateParametersTable(handles)
    % it gets the parameters from the given
    auxN = length(handles.myParameters);
    auxValues = cell(auxN,1);
    for i=1:auxN
        try
            auxSplitString = strsplit(handles.myParameters{i},'.');
            aux = getfield(handles.myReferenceObject, auxSplitString{:});
            %aux = handles.myReferenceObject.(handles.myParameters{i});
            if isnumeric(aux) || islogical(aux)
                auxValues{i} = num2str(aux);
            else
                error(['Unsupported value type for ',handles.myParameters{i}]);
            end
        catch e
            auxValues{i} = ['Error: ', e.message];
        end
    end
    set(handles.parametersTable, 'Data', auxValues);
    
function updatedObjectParameters(handles)
    %updatedObjectParameters(handles)
    % given the values in the parameter table, it will try to set it to the
    % object
    auxN = length(handles.myParameters);
    auxTableValues = get(handles.parametersTable, 'Data');
    for i=1:auxN
        try
            aux = str2num(auxTableValues{i});
            if isempty(aux)
                error('Empty value or numeric conversion impossible.');
            end
            
            auxSplitString = strsplit(handles.myParameters{i},'.');
            handles.myReferenceObject = setfield(handles.myReferenceObject, auxSplitString{:}, aux);
            %handles.myReferenceObject.(handles.myParameters{i}) = aux;
        catch e
            errordlg(['Impossible to set ', handles.myParameters{i},': ',...
                e.message])
        end
        
    end


% --- Outputs from this function are returned to the command line.
function varargout = linearFeedbackExpertGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in closeButton.
function closeButton_Callback(hObject, eventdata, handles)
% hObject    handle to closeButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(handles.output);


% --- Executes on button press in setButton.
function setButton_Callback(hObject, eventdata, handles)
% hObject    handle to setButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
updatedObjectParameters(handles);

% --- Executes on button press in updateButton.
function updateButton_Callback(hObject, eventdata, handles)
% hObject    handle to updateButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
updateParametersTable(handles);
