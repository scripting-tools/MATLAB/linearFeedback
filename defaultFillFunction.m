%%
%
% This function outputs a structure on the model of matlabMonitor data structures with
% the new .value field for each signals taking the data from the correspondent entry
% in dataArray
%
% March 2013 - Davide Gamba

function outStruct = defaultFillFunction(dataArray, signals)
outStruct = struct;
% it is suppposed for this function that dataArray and signals lenght are the same!
if (length(dataArray) ~= length(signals))
    disp('ERROR:defaultFillFunction: dataArray and signals have different length');
    return
end
for i=1:length(signals)
    outStruct = matlabMonitor.simpleSetvalue2Struct(signals(i), dataArray(i), outStruct);
end
end
