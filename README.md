linearFeedback is a MATLAB class that allows to create simple shot-to-shot feedbacks 
using any kind of observables and actuators accessible via JAPC.

It requires matlabJAPC libraries:
https://gitlab.cern.ch/scripting-tools/MATLAB/matlabJAPC.git

PS: ported to GIT from https://svn.cern.ch/reps/linearFeedback/publicTools/linearFeedback/trunk 

jul 2014 - davide.gamba@cern.ch
