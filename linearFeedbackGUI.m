function varargout = linearFeedbackGUI(varargin)
% linearFeedbackGUI MATLAB code for linearFeedbackGUI.fig
%      linearFeedbackGUI, by itself, creates setZerosWeightsButton new linearFeedbackGUI or raises the existing
%      singleton*.
%
%      H = linearFeedbackGUI returns the handle to setZerosWeightsButton new linearFeedbackGUI or the handle to
%      the existing singleton*.
%
%      linearFeedbackGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in linearFeedbackGUI.M with the given input arguments.
%
%      linearFeedbackGUI('Property','Value',...) creates setZerosWeightsButton new linearFeedbackGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the linearFeedbackGUI before linearFeedbackGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to linearFeedbackGUI_OpeningFcn via varargin.
%
%      *See linearFeedbackGUI Options on GUIDE's Tools menu.  Choose "linearFeedbackGUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help linearFeedbackGUI

% Last Modified by GUIDE v2.5 02-Apr-2015 15:40:17

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @linearFeedbackGUI_OpeningFcn, ...
    'gui_OutputFcn',  @linearFeedbackGUI_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

function figure1_DeleteFcn(hObject, eventdata, handles)

% --- Executes just before linearFeedbackGUI is made visible.
function out=linearFeedbackGUI_OpeningFcn(hObject, eventdata, hhObjectandles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in setZerosWeightsButton future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to linearFeedbackGUI (see VARARGIN)

handles = guidata(hObject);
% initialize myFeedbackObject related to the linearFeedbackGUI
if ~isempty(varargin) && isa(varargin{1}, 'linearFeedback')
    handles.myFeedbackObject=varargin{1};
else
    error('Error! No linear feedback object provided when opening gui!')
end

% see if we want a "read only" (faster, but less control)
handles.viewOnlyGUI = false;
if numel(varargin) > 1
    if ischar(varargin{2}) && strcmp(varargin{2},'ViewOnly')
        handles.viewOnlyGUI = true;
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% populate plots
handles=updateGUIData(hObject, '', handles);

% set a default expertPanelOpeningFunction handler in the handles.
% This can be changed by particular linearFeedback uses afterwards.
handles.expertPanelOpeningFunction = @(feedbackObject)defaultOpenExpertPanelFunction(feedbackObject);
handles.expertPanelHandler = '';

% I also want to keep here track of an help page (so other projects can
% point to different pages if they want.
handles.helpWebPage = 'https://wikis.cern.ch/display/CTF3OP/Matlab+linearFeedback';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Choose default command line output for linearFeedbackGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
out=handles;


% UIWAIT makes linearFeedbackGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = linearFeedbackGUI_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in setZerosWeightsButton future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles;



% --------------------------------------------------------------------
function out=updateGUIData(~,~,handles)
%
% create graphs
hold(handles.OrbitAxes, 'on');
hold(handles.ExcitationAxes, 'on');
if handles.viewOnlyGUI
    handles.plotWantedObservation=linkedPlotReadable(handles.OrbitAxes,handles.myFeedbackObject,'wantedObservation','updateSettings');
    handles.plotObservablesLowerLimits=linkedPlotReadable(handles.OrbitAxes,handles.myFeedbackObject,'observablesLowerLimits','updateSettings');
    handles.plotObservablesUpperLimits=linkedPlotReadable(handles.OrbitAxes,handles.myFeedbackObject,'observablesUpperLimits','updateSettings');
    
    handles.plotExcitatorsUpperLimits=linkedPlotReadable(handles.ExcitationAxes,handles.myFeedbackObject,'excitatorsUpperLimits','updateSettings');
    handles.plotExcitatorsLowerLimits=linkedPlotReadable(handles.ExcitationAxes,handles.myFeedbackObject,'excitatorsLowerLimits','updateSettings');
    handles.plotWantedExcitation=linkedPlotReadable(handles.ExcitationAxes,handles.myFeedbackObject,'wantedExcitation','updateSettings');

    handles.plotRandomExcitationPattern=linkedPlotReadable(handles.ExcitationPattern,handles.myFeedbackObject,'randomExcitationPattern','updateSettings');
    
    handles.plotObservablesWeights=linkedPlotReadable(handles.WeightsAxes,handles.myFeedbackObject,'observablesWeights','updateSettings');
else
    handles.plotWantedObservation=linkedPlotWritable(handles.OrbitAxes,handles.myFeedbackObject,'wantedObservation','updateSettings');
    handles.plotObservablesLowerLimits=linkedPlotWritable(handles.OrbitAxes,handles.myFeedbackObject,'observablesLowerLimits','updateSettings');
    handles.plotObservablesUpperLimits=linkedPlotWritable(handles.OrbitAxes,handles.myFeedbackObject,'observablesUpperLimits','updateSettings');
    
    handles.plotExcitatorsUpperLimits=linkedPlotWritable(handles.ExcitationAxes,handles.myFeedbackObject,'excitatorsUpperLimits','updateSettings');
    handles.plotExcitatorsLowerLimits=linkedPlotWritable(handles.ExcitationAxes,handles.myFeedbackObject,'excitatorsLowerLimits','updateSettings');
    handles.plotWantedExcitation=linkedPlotWritable(handles.ExcitationAxes,handles.myFeedbackObject,'wantedExcitation','updateSettings');

    handles.plotRandomExcitationPattern=linkedPlotWritable(handles.ExcitationPattern,handles.myFeedbackObject,'randomExcitationPattern','updateSettings');
    
    handles.plotObservablesWeights=linkedPlotWritable(handles.WeightsAxes,handles.myFeedbackObject,'observablesWeights','updateSettings');
end

handles.plotCurrentTmpObservation=linkedPlotReadable(handles.OrbitAxes,handles.myFeedbackObject,'currentTmpObservation','newBeam');
handles.plotBestObservationExpectedWithLimits=linkedPlotReadableWithError(handles.OrbitAxes,handles.myFeedbackObject,'bestObservationExpectedWithLimits','bestObservationExpectedWithLimitsError','updateSettings');

handles.plotCurrentTmpExcitationSetReading=linkedPlotReadable(handles.ExcitationAxes,handles.myFeedbackObject,'currentTmpExcitationSetReading','newBeam');
handles.plotCurrentTmpExcitationReadOut=linkedPlotReadable(handles.ExcitationAxes,handles.myFeedbackObject,'currentTmpExcitationReadOut','newBeam');
handles.plotBestExcitationExpectedWithLimits=linkedPlotReadable(handles.ExcitationAxes,handles.myFeedbackObject,'bestExcitationExpectedWithLimits','newBeam');

handles.plotMyTmpMat=linkedMatrixReadable(handles.SurfMatrixAxes, handles.myFeedbackObject,'getCurrentResponseMatrix','updateSettings');
handles.plotObservationsHistory=linkedMatrixReadable(handles.SurfHistoryAxes, handles.myFeedbackObject,'observationsHistory','newBeam');
handles.plotObservationsExcitationHistory=linkedMatrixReadable(handles.SurfHistoryExcitAxes, handles.myFeedbackObject,'observationsExcitationHistory','newBeam');


% stop holdings
hold(handles.OrbitAxes, 'off');
hold(handles.ExcitationAxes, 'off');


% set some labels and fonts
set(handles.OrbitAxes,'XTick',[]);
set(handles.ExcitationAxes,'XTick',[]);
%set(handles.WeightsAxes,'XTick',[]);
%set(handles.ExcitationPattern,'XTick',[]);
set(handles.SurfMatrixAxes,'XTick',[]);
set(handles.SurfMatrixAxes,'YTick',[]);
set(handles.SurfHistoryAxes,'XTick',[]);
set(handles.SurfHistoryAxes,'YTick',[]);
set(handles.SurfHistoryExcitAxes,'XTick',[]);
set(handles.SurfHistoryExcitAxes,'YTick',[]);

set(handles.OrbitAxes,'FontSize',10);
set(handles.ExcitationAxes,'FontSize',10);
set(handles.WeightsAxes,'FontSize',10);
set(handles.ExcitationPattern,'FontSize',10);


ylabel(handles.WeightsAxes, 'Weight [a.u]');
ylabel(handles.ExcitationPattern, 'Pattern [a.u]');
ylabel(handles.OrbitAxes,'value [a.u.]');
ylabel(handles.ExcitationAxes, 'value [a.u.]');

xlabel(handles.WeightsAxes,'observable # [-]');
xlabel(handles.ExcitationPattern, 'Excitator # [-]');


% link some axes
linkaxes([handles.OrbitAxes handles.WeightsAxes],'x');
linkaxes([handles.ExcitationAxes handles.ExcitationPattern],'x');

% some limits
xlim(handles.OrbitAxes,[.5 (handles.myFeedbackObject.nObservables)+.5]);
xlim(handles.WeightsAxes,[.5 (handles.myFeedbackObject.nObservables)+.5]);
xlim(handles.ExcitationAxes,[.5 (handles.myFeedbackObject.nExcitators)+.5]);
xlim(handles.ExcitationPattern,[.5 (handles.myFeedbackObject.nExcitators)+.5]);
%
ylim(handles.WeightsAxes,[0 2]);
ylim(handles.ExcitationPattern,[0 2]);


% turn on grids
grid(handles.OrbitAxes,'on');
grid(handles.ExcitationAxes,'on');
grid(handles.WeightsAxes,'on');
grid(handles.ExcitationPattern,'On');

% some style
handles.plotObservablesLowerLimits.setColor('b');
handles.plotObservablesUpperLimits.setColor('r');
handles.plotExcitatorsUpperLimits.setColor('r');
handles.plotExcitatorsLowerLimits.setColor('b');
handles.plotWantedObservation.setColor('k');
handles.plotWantedExcitation.setColor('k');
handles.plotObservablesWeights.setColor('k');
handles.plotRandomExcitationPattern.setColor('k');
handles.plotBestObservationExpectedWithLimits.setColor([0.1 0.7 0.1]);
handles.plotBestObservationExpectedWithLimits.setLineStyle('--');
handles.plotBestExcitationExpectedWithLimits.setColor([0.1 0.7 0.1]);
handles.plotBestExcitationExpectedWithLimits.setLineStyle('--');
handles.plotCurrentTmpObservation.setColor('m');
handles.plotCurrentTmpObservation.setLineStyle('-');
handles.plotCurrentTmpObservation.setLineWidth(2);
handles.plotCurrentTmpExcitationSetReading.setColor('m');
handles.plotCurrentTmpExcitationSetReading.setLineStyle('-');
handles.plotCurrentTmpExcitationReadOut.setColor('m');
handles.plotCurrentTmpExcitationReadOut.setLineWidth(2);

%some legend
handles.plotCurrentTmpObservation.setDisplayName('Current Observation');
handles.plotBestObservationExpectedWithLimits.setDisplayName('Best Observation expected');
%
handles.plotCurrentTmpExcitationReadOut.setDisplayName('Current Excitation ReadOut');
handles.plotCurrentTmpExcitationSetReading.setDisplayName('Current Excitation Set-ReadOut');
handles.plotBestExcitationExpectedWithLimits.setDisplayName('Best Excitation expected (Set)');
%
handles.plotWantedObservation.setDisplayName('Wanted Observation');
handles.plotObservablesLowerLimits.setDisplayName('Observables lower limits');
handles.plotObservablesUpperLimits.setDisplayName('Observables upper limits');

handles.plotExcitatorsLowerLimits.setDisplayName('Excitors lower limits');
handles.plotExcitatorsUpperLimits.setDisplayName('Excitors upper limits');
handles.plotWantedExcitation.setDisplayName('Target Excitation');

handles.plotRandomExcitationPattern.setDisplayName('');
handles.plotObservablesWeights.setDisplayName('');

% listener to update some timing around whene there is a new beam coming
handles.myListenerForDataStamp=addlistener(handles.myFeedbackObject,'newBeam', @(h,e)updateTimeStamp(h,e,handles));

% create a local right click menu
handles.menuOrbitAxes1=uimenu(handles.menuOrbitAxes, 'Label', 'Fill labels!');
handles.menuOrbitAxes2=uimenu(handles.menuOrbitAxes, 'Label', 'Set Y axes limit','Callback',@(h,e)SetAxes(h,e, hObject));
handles.menuOrbitAxes3=uimenu(handles.menuOrbitAxes, 'Label', 'Reset Y Axes Limits', 'Callback',@(h,e)set(hObject,'YLimMode','auto'));


updateTimeStamp(0,0,handles)
handles.myFile='';
handles.myFiles='';
out=handles;


function GUIhandler = defaultOpenExpertPanelFunction(feedbackObject)
% This is the function used by default to open the expert parameter panel.
% By default I am using a "table" GUI with all the requested parameters.
% The same GUI can be used also for more complex linearFeedback systems
% (see for example the orbit correction). In this case it will be the user
% to set an appropriate opening function
auxDefaultParameters = {...
    'nAveragingShots',...
    'nSkipAfterSet',...
    'nSkipAlways',...
    ...
    'mySolver.learningRegolarizationWeight',...
    'mySolver.maxHistorySize',...
    'mySolver.decayTime'
    };
GUIhandler = linearFeedbackExpertGUI(feedbackObject, auxDefaultParameters);


% --- Executes on button press in startstopbutton.add
function startstopbutton_Callback(hObject, eventdata, handles)
% hObject    handle to startstopbutton (see GCBO)
% eventdata  reserved - to be defined in setZerosWeightsButton future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of startstopbutton

if ~handles.myFeedbackObject.isAcquiringData()
    set(hObject,'String','Stop');
    set(hObject,'BackgroundColor',[0 0.7 0]);
    handles.myFeedbackObject.startDataAcquisition();   
    set(handles.forwardButton,'Enable','Off')
    set(handles.backButton,'Enable','Off')
    set(handles.loadSlider,'Enable','Off')
    set(handles.send2HW,'Enable','Off')
else
    set(hObject,'String','Start');
    set(hObject,'BackgroundColor',[1 0.6 0.1]);
    handles.myFeedbackObject.stopDataAcquisition();   
end

% --- Executes on slider movement.
function gainSlider_Callback(hObject, eventdata, handles)
% hObject    handle to gainSlider (see GCBO)
% eventdata  reserved - to be defined in setZerosWeightsButton future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
handles.myFeedbackObject.setCorrectionGain(get(hObject,'Value'));
set(handles.labelFeedback,'String', ['Feedback Gain: ' sprintf('%.2f',get(hObject,'Value'))]);

% --- Executes during object creation, after setting all properties.
function gainSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to gainSlider (see GCBO)
% eventdata  reserved - to be defined in setZerosWeightsButton future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have setZerosWeightsButton light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function randomExcitationSlider_Callback(hObject, eventdata, handles)
% hObject    handle to randomExcitationSlider (see GCBO)
% eventdata  reserved - to be defined in setZerosWeightsButton future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
handles.myFeedbackObject.setRandomExcitationGain(get(hObject,'Value'));
set(handles.labelExcitation,'String', ['Excitation Gain: ' sprintf('%.2f',get(hObject,'Value'))]);


% --- Executes during object creation, after setting all properties.
function randomExcitationSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to randomExcitationSlider (see GCBO)
% eventdata  reserved - to be defined in setZerosWeightsButton future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have setZerosWeightsButton light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end




% --- Executes on button press in backButton.
function backButton_Callback(hObject, eventdata, handles)
% hObject    handle to backButton (see GCBO)
% eventdata  reserved - to be defined in setZerosWeightsButton future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.fileIndex>1
    handles.fileIndex=handles.fileIndex-1;
    handles.myFile=handles.myFiles(handles.fileIndex).name;
    set(handles.loadSlider,'Value',handles.fileIndex);
    guidata(hObject,handles);
    refreshGUIfromFile(handles);
else
    warndlg('Files ended!','Warning','modal')
end

% --- Executes on button press in forwardButton.
function forwardButton_Callback(hObject, eventdata, handles)
% hObject    handle to forwardButton (see GCBO)
% eventdata  reserved - to be defined in setZerosWeightsButton future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.fileIndex<length(handles.myFiles)
    handles.fileIndex=handles.fileIndex+1;
    handles.myFile=handles.myFiles(handles.fileIndex).name;
    set(handles.loadSlider,'Value',handles.fileIndex);
    guidata(hObject,handles);
    refreshGUIfromFile(handles);
else
    warndlg('Files ended!','Warning','modal')
end

% --- Executes on button press in improveButton.
function improveButton_Callback(hObject, eventdata, handles)
% hObject    handle to improveButton (see GCBO)
% eventdata  reserved - to be defined in setZerosWeightsButton future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if (get(hObject,'Value') == 0)
    handles.myFeedbackObject.isRMImproveEnabled = false;
    %set(hObject,'String','Improve RM');
    set(hObject,'BackgroundColor',[1 0.6 0.1]);
else
    handles.myFeedbackObject.isRMImproveEnabled = true;
    %set(hObject,'String','Stop improve RM');
    set(hObject,'BackgroundColor',[0 0.7 0]);
end


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in setZerosWeightsButton future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.plotWantedObservation.delete
handles.plotWantedExcitation.delete
handles.plotObservablesLowerLimits.delete
handles.plotObservablesUpperLimits.delete
handles.plotBestObservationExpectedWithLimits.delete
handles.plotCurrentTmpExcitationSetReading.delete
handles.plotCurrentTmpExcitationReadOut.delete
handles.plotExcitatorsUpperLimits.delete
handles.plotExcitatorsLowerLimits.delete
handles.plotObservablesWeights.delete
handles.plotMyTmpMat.delete
handles.plotObservationsHistory.delete
handles.plotObservationsExcitationHistory.delete
handles.plotCurrentTmpObservation.delete
handles.plotRandomExcitationPattern.delete
handles.plotBestExcitationExpectedWithLimits.delete
delete(handles.myListenerForDataStamp);

% delete also expertPanel if open
try
    close(handles.expertPanelHandler);
catch e
    % nothing to do. I hope it has been alredy closed.
end

% Hint: delete(hObject) closes the figure
delete(hObject);


function updateTimeStamp(hObject, eventdata, handles)
if any(strcmp('localTimeStamp_ms',fieldnames(handles.myFeedbackObject.currentTmpDataStruct(end))))
    aux=handles.myFeedbackObject.currentTmpDataStruct(end).localTimeStamp_ms;
    set(handles.timeStamp,'String',datestr(unixtime2mat(aux(1)/1000)));
else
    set(handles.timeStamp,'String','---');
end

% update sliders
set(handles.gainSlider,'Value', handles.myFeedbackObject.correctionGain);
set(handles.labelFeedback,'String', ['Feedback Gain: ' sprintf('%.2f',get(handles.gainSlider,'Value'))]);

set(handles.randomExcitationSlider,'Value', handles.myFeedbackObject.randomExcitationGain);
set(handles.labelExcitation,'String', ['Excitation Gain: ' sprintf('%.2f',get(handles.randomExcitationSlider,'Value'))]);

set(handles.regularizationWeightSlider,'Value', handles.myFeedbackObject.mySolver.trustResponseMatrixWeight);
set(handles.labelRegularizationWeight,'String', ['Old RM Improve Weight: ' sprintf('%.2f',handles.myFeedbackObject.mySolver.trustResponseMatrixWeight)]);

set(handles.minCorrectionSlider,'Value', handles.myFeedbackObject.mySolver.excitationOptimizationWeight);
set(handles.labelMinCorrection,'String', ['Opt. correction Weight: ' sprintf('%.2f',get(handles.minCorrectionSlider,'Value'))]);

%%Update limits excitors
try
    if length(handles.myFeedbackObject.excitatorsLowerLimits) == ...
            legnth(handles.myFeedbackObject.currentTmpExcitationSetReading)
        handles.myFeedbackObject.excitatorsLowerLimits=min(handles.myFeedbackObject.excitatorsLowerLimits(:),handles.myFeedbackObject.currentTmpExcitationSetReading(:));
    end
    if length(handles.myFeedbackObject.excitatorsUpperLimits) == ...
            legnth(handles.myFeedbackObject.currentTmpExcitationSetReading)
        handles.myFeedbackObject.excitatorsUpperLimits=max(handles.myFeedbackObject.excitatorsUpperLimits(:),handles.myFeedbackObject.currentTmpExcitationSetReading(:));
    end
    handles.myFeedbackObject.observablesWeights=max(zeros(size(handles.myFeedbackObject.observablesWeights(:))),handles.myFeedbackObject.observablesWeights(:));
    handles.myFeedbackObject.observablesWeights=min(2*ones(size(handles.myFeedbackObject.observablesWeights(:))),handles.myFeedbackObject.observablesWeights(:));
    handles.myFeedbackObject.randomExcitationPattern=max(zeros(size(handles.myFeedbackObject.randomExcitationPattern(:))),handles.myFeedbackObject.randomExcitationPattern(:));
    handles.myFeedbackObject.randomExcitationPattern=min(2*ones(size(handles.myFeedbackObject.randomExcitationPattern(:))),handles.myFeedbackObject.randomExcitationPattern(:));
catch e
    % don't do anything
end
% update buttons and other GUI elements
% start/stop
if handles.myFeedbackObject.isAcquiringData()
    set(handles.startstopbutton,'String','Stop');
    set(handles.startstopbutton,'Value', 1);
    set(handles.startstopbutton,'BackgroundColor',[0 0.7 0]);
    set(handles.loadSlider,'Enable','off')
else
    set(handles.startstopbutton,'String','Start');
    set(handles.startstopbutton,'Value', 0);
    set(handles.startstopbutton,'BackgroundColor',[1 0.6 0.1]);
end
% verbose
try
    if handles.myFeedbackObject.myMonitor.verbose
        handles.myFeedbackObject.myJapc.verbose = true;
        set(handles.verbose_menu, 'Checked', 'on')
    end
catch e
    % just don't do anything
end
% save data
if handles.myFeedbackObject.saveData
    set(handles.Savedata_menu, 'Checked', 'on');
end
% improve RM
if handles.myFeedbackObject.isRMImproveEnabled;
    set(handles.improveButton, 'Value', 1);
    set(handles.improveButton, 'BackgroundColor',[0 0.7 0]);
else
    set(handles.improveButton, 'Value', 0);
    set(handles.improveButton, 'BackgroundColor',[1 0.6 0.1]);
end
% target correction
if handles.myFeedbackObject.targetWantedExcitation;
    set(handles.targetExcitationButton, 'Value', 1);
    set(handles.targetExcitationButton, 'BackgroundColor',[0 0.7 0]);
else
    set(handles.targetExcitationButton, 'Value', 0);
    set(handles.targetExcitationButton, 'BackgroundColor',[1 0.6 0.1]);
end
% auto reset previous
if handles.myFeedbackObject.autoResetPreviousEnabled;
    set(handles.autoResetPreviousTag, 'Value', 1);
    set(handles.autoResetPreviousTag, 'BackgroundColor',[0 0.7 0]);
else
    set(handles.autoResetPreviousTag, 'Value', 0);
    set(handles.autoResetPreviousTag, 'BackgroundColor',[1 0.6 0.1]);
end
% excitation strategy buttons
switch handles.myFeedbackObject.randomExcitationStrategy
    case handles.myFeedbackObject.randomExcitationStrategyGaussian
        set(handles.excitRandGausTog,'Value',1);
        %
        set(handles.excitRandGausTog,'BackgroundColor',[0 0.7 0]);
        set(handles.excitRandSquareTog,'BackgroundColor',[1 0.6 0.1]);
        set(handles.excitPlusMinusTog,'BackgroundColor',[1 0.6 0.1]);
        set(handles.obsRandGausTog,'BackgroundColor',[1 0.6 0.1]);
        set(handles.obsRandSquareTog,'BackgroundColor',[1 0.6 0.1]);
        set(handles.obsPlusMinusTog,'BackgroundColor',[1 0.6 0.1]);
    case handles.myFeedbackObject.randomExcitationStrategySquare
        set(handles.excitRandSquareTog,'Value',1);
        %
        set(handles.excitRandGausTog,'BackgroundColor',[1 0.6 0.1]);
        set(handles.excitRandSquareTog,'BackgroundColor',[0 0.7 0]);
        set(handles.excitPlusMinusTog,'BackgroundColor',[1 0.6 0.1]);
        set(handles.obsRandGausTog,'BackgroundColor',[1 0.6 0.1]);
        set(handles.obsRandSquareTog,'BackgroundColor',[1 0.6 0.1]);
        set(handles.obsPlusMinusTog,'BackgroundColor',[1 0.6 0.1]);
    case handles.myFeedbackObject.randomExcitationStrategyPlusMinus
        set(handles.excitPlusMinusTog,'Value',1);
        %
        set(handles.excitRandGausTog,'BackgroundColor',[1 0.6 0.1]);
        set(handles.excitRandSquareTog,'BackgroundColor',[1 0.6 0.1]);
        set(handles.excitPlusMinusTog,'BackgroundColor',[0 0.7 0]);
        set(handles.obsRandGausTog,'BackgroundColor',[1 0.6 0.1]);
        set(handles.obsRandSquareTog,'BackgroundColor',[1 0.6 0.1]);
        set(handles.obsPlusMinusTog,'BackgroundColor',[1 0.6 0.1]);
    case handles.myFeedbackObject.randomExcitationStrategyGaussianObservation
        set(handles.obsRandGausTog,'Value',1);
        %
        set(handles.excitRandGausTog,'BackgroundColor',[1 0.6 0.1]);
        set(handles.excitRandSquareTog,'BackgroundColor',[1 0.6 0.1]);
        set(handles.excitPlusMinusTog,'BackgroundColor',[1 0.6 0.1]);
        set(handles.obsRandGausTog,'BackgroundColor',[0 0.7 0]);
        set(handles.obsRandSquareTog,'BackgroundColor',[1 0.6 0.1]);
        set(handles.obsPlusMinusTog,'BackgroundColor',[1 0.6 0.1]);
    case handles.myFeedbackObject.randomExcitationStrategySquareObservation
        set(handles.obsRandSquareTog,'Value',1);
        %
        set(handles.excitRandGausTog,'BackgroundColor',[1 0.6 0.1]);
        set(handles.excitRandSquareTog,'BackgroundColor',[1 0.6 0.1]);
        set(handles.excitPlusMinusTog,'BackgroundColor',[1 0.6 0.1]);
        set(handles.obsRandGausTog,'BackgroundColor',[1 0.6 0.1]);
        set(handles.obsRandSquareTog,'BackgroundColor',[0 0.7 0]);
        set(handles.obsPlusMinusTog,'BackgroundColor',[1 0.6 0.1]);
    case handles.myFeedbackObject.randomExcitationStrategyPlusMinusObservation
        set(handles.obsPlusMinusTog,'Value',1);
        %
        set(handles.excitRandGausTog,'BackgroundColor',[1 0.6 0.1]);
        set(handles.excitRandSquareTog,'BackgroundColor',[1 0.6 0.1]);
        set(handles.excitPlusMinusTog,'BackgroundColor',[1 0.6 0.1]);
        set(handles.obsRandGausTog,'BackgroundColor',[1 0.6 0.1]);
        set(handles.obsRandSquareTog,'BackgroundColor',[1 0.6 0.1]);
        set(handles.obsPlusMinusTog,'BackgroundColor',[0 0.7 0]);
end




function refreshGUIfromFile(handles)
    handles.myFeedbackObject.loadStatusFromFile([handles.myPathName handles.myFile]);
    try
        disp(['loaded file ',handles.myFile, ' idx=',num2str(handles.fileIndex)])
    catch e
        % there is some error somethimes.
    end
    
    notify(handles.myFeedbackObject,'updateSettings')
    notify(handles.myFeedbackObject,'newBeam')


% --- Executes on button press in redraw.
function redraw_Callback(hObject, eventdata, handles)
% hObject    handle to redraw (see GCBO)
% eventdata  reserved - to be defined in setZerosWeightsButton future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
try
    handles.myFeedbackObject.computeGainedCorrectionExcitationAndExpectations();
catch e
    disp('Impossible to re-compute gained excitations and expectations for this re-draw...');
end
notify(handles.myFeedbackObject,'newBeam')
notify(handles.myFeedbackObject,'updateSettings')



% --- Executes on slider movement.
function loadSlider_Callback(hObject, eventdata, handles)
% hObject    handle to loadSlider (see GCBO)
% eventdata  reserved - to be defined in setZerosWeightsButton future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
    handles.fileIndex=round(get(hObject,'Value'));
    handles.myFile=handles.myFiles(handles.fileIndex).name;
    guidata(hObject,handles);
    refreshGUIfromFile(handles);

% --- Executes during object creation, after setting all properties.
function loadSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to loadSlider (see GCBO)
% eventdata  reserved - to be defined in setZerosWeightsButton future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have setZerosWeightsButton light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in send2HW.
function send2HW_Callback(hObject, eventdata, handles)
% hObject    handle to send2HW (see GCBO)
% eventdata  reserved - to be defined in setZerosWeightsButton future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%disp('to be uncommented.')
ButtonName =questdlg('Should I launch the correction?', ...
    'Are you sure?', ...
    'Yes', 'No','No');
switch ButtonName,
    case 'Yes'
        handles.myFeedbackObject.myJapc.JSetAll(handles.myFeedbackObject.excitatorsParameters,handles.myFeedbackObject.currentTmpDataStruct)
end
% --- Executes on mouse press over axes background.
function OrbitAxes_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to OrbitAxes (see GCBO)
% eventdata  reserved - to be defined in setZerosWeightsButton future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
aux=get(hObject,'CurrentPoint');
auxIndex = round(aux(1));
try
    auxLabel = ['(',num2str(auxIndex),')', handles.myFeedbackObject.observablesLabels{auxIndex}];
catch e
    disp(['Impossible to set label for this point ', num2str(auxIndex),':', e.message]);
    auxLabel = ['(',num2str(auxIndex),')'];
end
set(handles.menuOrbitAxes1, 'Label', auxLabel);
set(handles.menuOrbitAxes2, 'Callback',@(h,e)SetAxes(h,e, hObject));
set(handles.menuOrbitAxes3, 'Callback',@(h,e)set(hObject,'YLimMode','auto'));


% --- Executes on mouse press over axes background.
function WeightsAxes_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to WeightsAxes (see GCBO)
% eventdata  reserved - to be defined in setZerosWeightsButton future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
aux=get(hObject,'CurrentPoint');
auxIndex = round(aux(1));
try
    auxLabel = ['(',num2str(auxIndex),')', handles.myFeedbackObject.observablesLabels{auxIndex}];
catch e
    disp(['Impossible to set label for this point ', num2str(auxIndex),':', e.message]);
    auxLabel = ['(',num2str(auxIndex),')'];
end
set(handles.menuOrbitAxes1, 'Label', auxLabel);
set(handles.menuOrbitAxes2, 'Callback',@(h,e)SetAxes(h,e, hObject));
set(handles.menuOrbitAxes3, 'Callback',@(h,e)set(hObject,'YLimMode','auto'));


% --- Executes on mouse press over axes background.
function ExcitationAxes_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to ExcitationAxes (see GCBO)
% eventdata  reserved - to be defined in setZerosWeightsButton future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
aux=get(hObject,'CurrentPoint');
auxIndex = round(aux(1));
try
    auxLabel = ['(',num2str(auxIndex),')', handles.myFeedbackObject.excitatorsLabels{auxIndex}];
catch e
    disp(['Impossible to set label for this point ', num2str(auxIndex),':', e.message]);
    auxLabel = ['(',num2str(auxIndex),')'];
end
set(handles.menuOrbitAxes1, 'Label', auxLabel);
set(handles.menuOrbitAxes2, 'Callback',@(h,e)SetAxes(h,e, hObject));
set(handles.menuOrbitAxes3, 'Callback',@(h,e)set(hObject,'YLimMode','auto'));



% --- Executes on mouse press over axes background.
function ExcitationPattern_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to ExcitationPattern (see GCBO)
% eventdata  reserved - to be defined in setZerosWeightsButton future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
aux=get(hObject,'CurrentPoint');
auxIndex = round(aux(1));
try
    auxLabel = ['(',num2str(auxIndex),')', handles.myFeedbackObject.excitatorsLabels{auxIndex}];
catch e
    disp(['Impossible to set label for this point ', num2str(auxIndex),':', e.message]);
    auxLabel = ['(',num2str(auxIndex),')'];
end
set(handles.menuOrbitAxes1, 'Label', auxLabel);
set(handles.menuOrbitAxes2, 'Callback',@(h,e)SetAxes(h,e, hObject));
set(handles.menuOrbitAxes3, 'Callback',@(h,e)set(hObject,'YLimMode','auto'));


function SetAxes(h, e, myAxes)
% little callback used to set axes limits
prompt={'Enter the yMin','Enter the yMax'};
name='Input yLim';
numlines=1;
currentValues = get(myAxes,'yLim');
defaultanswer={num2str(currentValues(1)),num2str(currentValues(2))};
auxReply = inputdlg(prompt,name,numlines,defaultanswer);
if ~isempty(auxReply)
    set(myAxes, 'yLim',str2num(char(auxReply)))
end

function SetSliderUpLimit(h, e, mySlider)
% little callback used to set upper limits of a slider
prompt={'Max slider value:'};
name='Update slider upper limit';
numlines=1;
defaultanswer={num2str(get(mySlider,'Max'))};
auxReply = inputdlg(prompt,name,numlines,defaultanswer);
if ~isempty(auxReply)
    auxValue = get(mySlider,'Value');
    auxMax = str2double(auxReply{1});
    if auxValue < auxMax
        set(mySlider,'Max',auxMax);
    else
        errordlg('Impossible to set upper limit < current value');
    end
end

function SetSliderValue(h, e, mySlider)
% little callback used to set upper limits of a slider
prompt={'Arbitrary value:'};
name='Set value';
numlines=1;
defaultanswer={num2str(get(mySlider,'Value'))};
auxReply = inputdlg(prompt,name,numlines,defaultanswer);
if ~isempty(auxReply)
    auxValue = str2double(auxReply{1});
    auxMaxSlider = get(mySlider,'Max');
    if auxValue > auxMaxSlider
        set(mySlider,'Max',auxValue);
    end
    set(mySlider,'Value',auxValue);
    % fire call back of the selected slider slider
    auxCallback = get(mySlider,'Callback');
    auxCallback(mySlider, e);
end

% --- Executes on button press in setZerosWeightsButton.
function setZerosWeightsButton_Callback(hObject, eventdata, handles)
% hObject    handle to setZerosWeightsButton (see GCBO)
% eventdata  reserved - to be defined in setZerosWeightsButton future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.myFeedbackObject.observablesWeights = zeros(handles.myFeedbackObject.nObservables,1);

% --- Executes on button press in setZerosPatternButton.
function setZerosPatternButton_Callback(hObject, eventdata, handles)
% hObject    handle to setZerosPatternButton (see GCBO)
% eventdata  reserved - to be defined in setZerosWeightsButton future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.myFeedbackObject.randomExcitationPattern = zeros(handles.myFeedbackObject.nExcitators,1);

% --- Executes on button press in setOnesPatternButton.
function setOnesPatternButton_Callback(hObject, eventdata, handles)
% hObject    handle to setOnesPatternButton (see GCBO)
% eventdata  reserved - to be defined in setZerosWeightsButton future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.myFeedbackObject.randomExcitationPattern = ones(handles.myFeedbackObject.nExcitators,1);


% --- Executes on button press in smoothPatternButton.
function smoothPatternButton_Callback(hObject, eventdata, handles)
% hObject    handle to smoothPatternButton (see GCBO)
% eventdata  reserved - to be defined in setZerosWeightsButton future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.myFeedbackObject.randomExcitationPattern = smooth(handles.myFeedbackObject.randomExcitationPattern);

% --- Executes on button press in zeroLowerLimitButton.
function zeroLowerLimitButton_Callback(hObject, eventdata, handles)
% hObject    handle to zeroLowerLimitButton (see GCBO)
% eventdata  reserved - to be defined in setZerosWeightsButton future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.myFeedbackObject.excitatorsLowerLimits = handles.myFeedbackObject.currentTmpExcitationSetReading;


% --- Executes on button press in oneUpperLimitButton.
function oneUpperLimitButton_Callback(hObject, eventdata, handles)
% hObject    handle to oneUpperLimitButton (see GCBO)
% eventdata  reserved - to be defined in setZerosWeightsButton future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.myFeedbackObject.excitatorsUpperLimits = handles.myFeedbackObject.excitatorsUpperLimits +1;


% --- Executes on button press in oneLowerLimitButton.
function oneLowerLimitButton_Callback(hObject, eventdata, handles)
% hObject    handle to oneLowerLimitButton (see GCBO)
% eventdata  reserved - to be defined in setZerosWeightsButton future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.myFeedbackObject.excitatorsLowerLimits = handles.myFeedbackObject.excitatorsLowerLimits -1;


% --- Executes on button press in zeroUpperLimitButton.
function zeroUpperLimitButton_Callback(hObject, eventdata, handles)
% hObject    handle to zeroUpperLimitButton (see GCBO)
% eventdata  reserved - to be defined in setZerosWeightsButton future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.myFeedbackObject.excitatorsUpperLimits = handles.myFeedbackObject.currentTmpExcitationSetReading;


% --- Executes on button press in setOnesWeightsButton.
function setOnesWeightsButton_Callback(hObject, eventdata, handles)
% hObject    handle to setOnesWeightsButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.myFeedbackObject.observablesWeights = ones(handles.myFeedbackObject.nObservables,1);


% --- Executes on button press in smoothWeightsButton.
function smoothWeightsButton_Callback(hObject, eventdata, handles)
% hObject    handle to smoothWeightsButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.myFeedbackObject.observablesWeights = smooth(handles.myFeedbackObject.observablesWeights);


% --- Executes on button press in smoothCurrentRequestButton.
function smoothCurrentRequestButton_Callback(hObject, eventdata, handles)
% hObject    handle to smoothCurrentRequestButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.myFeedbackObject.wantedObservation = smooth(handles.myFeedbackObject.wantedObservation);

% --- Executes on button press in useCurrentOrbitButton.
function useCurrentOrbitButton_Callback(hObject, eventdata, handles)
% hObject    handle to useCurrentOrbitButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.myFeedbackObject.wantedObservation = handles.myFeedbackObject.currentTmpObservation;


% --- Executes on button press in useZeroOrbitButton.
function useZeroOrbitButton_Callback(hObject, eventdata, handles)
% hObject    handle to useZeroOrbitButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.myFeedbackObject.wantedObservation = zeros(handles.myFeedbackObject.nObservables,1);


% --- Executes during object creation, after setting all properties.
function OrbitAxes_CreateFcn(hObject, eventdata, handles)
% hObject    handle to OrbitAxes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate OrbitAxes


% --- Executes on button press in keepSignButton.
function keepSignButton_Callback(hObject, eventdata, handles)
% hObject    handle to keepSignButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
aux = handles.myFeedbackObject.currentTmpExcitationSetReading;
for i=1:numel(aux)
    if (aux(i) > 0)
        handles.myFeedbackObject.excitatorsLowerLimits(i) = 0;
    else
        handles.myFeedbackObject.excitatorsUpperLimits(i) = 0;
    end
end

% --- Executes on slider movement.
function minCorrectionSlider_Callback(hObject, eventdata, handles)
% hObject    handle to minCorrectionSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
handles.myFeedbackObject.mySolver.excitationOptimizationWeight = get(hObject,'Value');
set(handles.labelMinCorrection,'String', ['Opt Correction Weight: ' sprintf('%.2f',get(hObject,'Value'))]);


% --- Executes during object creation, after setting all properties.
function minCorrectionSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to minCorrectionSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function regularizationWeightSlider_Callback(hObject, eventdata, handles)
% hObject    handle to regularizationWeightSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
handles.myFeedbackObject.mySolver.trustResponseMatrixWeight = get(hObject,'Value');
set(handles.labelRegularizationWeight,'String', ['Old RM Improve Weight: ' sprintf('%.2f',get(hObject,'Value'))]);


% --- Executes during object creation, after setting all properties.
function regularizationWeightSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to regularizationWeightSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in setRefOrbitButton.
function setRefOrbitButton_Callback(hObject, eventdata, handles)
% hObject    handle to setRefOrbitButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.myFeedbackObject.setWantedOrbitFromReference();




% --- Executes on button press in targetExcitationButton.
function targetExcitationButton_Callback(hObject, eventdata, handles)
% hObject    handle to targetExcitationButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of startstopbutton
if get(hObject,'Value')
    handles.myFeedbackObject.targetWantedExcitation = true;
    set(hObject,'BackgroundColor',[0 0.7 0]);
else
    handles.myFeedbackObject.targetWantedExcitation = false;
    set(hObject,'BackgroundColor',[1 0.6 0.1]);
end


% --- Executes on button press in useCurrentExcitationButton.
function useCurrentExcitationButton_Callback(hObject, eventdata, handles)
% hObject    handle to useCurrentExcitationButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.myFeedbackObject.wantedExcitation = handles.myFeedbackObject.currentTmpExcitationSetReading;


% --- Executes on button press in useZeroExcitationButton.
function useZeroExcitationButton_Callback(hObject, eventdata, handles)
% hObject    handle to useZeroExcitationButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.myFeedbackObject.wantedExcitation = zeros(handles.myFeedbackObject.nExcitators,1);


% --- Executes on button press in resetInitialExcitationButton.
function resetInitialExcitationButton_Callback(hObject, eventdata, handles)
% hObject    handle to resetInitialExcitationButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ButtonName =questdlg('Should I reset the initial excitation?', ...
    'Are you sure?', ...
    'Yes', 'No', 'No');
switch ButtonName,
    case 'Yes'
        handles.myFeedbackObject.resetInitialExcitatorStatus();
end


% --- Executes on button press in excitRandGausTog.
function excitRandGausTog_Callback(hObject, eventdata, handles)
% hObject    handle to excitRandGausTog (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.myFeedbackObject.randomExcitationStrategy = handles.myFeedbackObject.randomExcitationStrategyGaussian;
%
set(handles.excitRandGausTog,'BackgroundColor',[0 0.7 0]);
set(handles.excitRandSquareTog,'BackgroundColor',[1 0.6 0.1]);
set(handles.excitPlusMinusTog,'BackgroundColor',[1 0.6 0.1]);
set(handles.obsRandGausTog,'BackgroundColor',[1 0.6 0.1]);
set(handles.obsRandSquareTog,'BackgroundColor',[1 0.6 0.1]);
set(handles.obsPlusMinusTog,'BackgroundColor',[1 0.6 0.1]);



% --- Executes on button press in excitRandSquareTog.
function excitRandSquareTog_Callback(hObject, eventdata, handles)
% hObject    handle to excitRandSquareTog (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.myFeedbackObject.randomExcitationStrategy = handles.myFeedbackObject.randomExcitationStrategySquare;
%
set(handles.excitRandGausTog,'BackgroundColor',[1 0.6 0.1]);
set(handles.excitRandSquareTog,'BackgroundColor',[0 0.7 0]);
set(handles.excitPlusMinusTog,'BackgroundColor',[1 0.6 0.1]);
set(handles.obsRandGausTog,'BackgroundColor',[1 0.6 0.1]);
set(handles.obsRandSquareTog,'BackgroundColor',[1 0.6 0.1]);
set(handles.obsPlusMinusTog,'BackgroundColor',[1 0.6 0.1]);

% --- Executes on button press in excitPlusMinusTog.
function excitPlusMinusTog_Callback(hObject, eventdata, handles)
% hObject    handle to excitPlusMinusTog (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.myFeedbackObject.randomExcitationStrategy = handles.myFeedbackObject.randomExcitationStrategyPlusMinus;
%
set(handles.excitRandGausTog,'BackgroundColor',[1 0.6 0.1]);
set(handles.excitRandSquareTog,'BackgroundColor',[1 0.6 0.1]);
set(handles.excitPlusMinusTog,'BackgroundColor',[0 0.7 0]);
set(handles.obsRandGausTog,'BackgroundColor',[1 0.6 0.1]);
set(handles.obsRandSquareTog,'BackgroundColor',[1 0.6 0.1]);
set(handles.obsPlusMinusTog,'BackgroundColor',[1 0.6 0.1]);

% --- Executes on button press in obsRandGausTog.
function obsRandGausTog_Callback(hObject, eventdata, handles)
% hObject    handle to obsRandGausTog (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.myFeedbackObject.randomExcitationStrategy = handles.myFeedbackObject.randomExcitationStrategyGaussianObservation;
%
set(handles.excitRandGausTog,'BackgroundColor',[1 0.6 0.1]);
set(handles.excitRandSquareTog,'BackgroundColor',[1 0.6 0.1]);
set(handles.excitPlusMinusTog,'BackgroundColor',[1 0.6 0.1]);
set(handles.obsRandGausTog,'BackgroundColor',[0 0.7 0]);
set(handles.obsRandSquareTog,'BackgroundColor',[1 0.6 0.1]);
set(handles.obsPlusMinusTog,'BackgroundColor',[1 0.6 0.1]);

% --- Executes on button press in obsRandSquareTog.
function obsRandSquareTog_Callback(hObject, eventdata, handles)
% hObject    handle to obsRandSquareTog (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.myFeedbackObject.randomExcitationStrategy = handles.myFeedbackObject.randomExcitationStrategySquareObservation;
%
set(handles.excitRandGausTog,'BackgroundColor',[1 0.6 0.1]);
set(handles.excitRandSquareTog,'BackgroundColor',[1 0.6 0.1]);
set(handles.excitPlusMinusTog,'BackgroundColor',[1 0.6 0.1]);
set(handles.obsRandGausTog,'BackgroundColor',[1 0.6 0.1]);
set(handles.obsRandSquareTog,'BackgroundColor',[0 0.7 0]);
set(handles.obsPlusMinusTog,'BackgroundColor',[1 0.6 0.1]);

% --- Executes on button press in obsPlusMinusTog.
function obsPlusMinusTog_Callback(hObject, eventdata, handles)
% hObject    handle to obsPlusMinusTog (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.myFeedbackObject.randomExcitationStrategy = handles.myFeedbackObject.randomExcitationStrategyPlusMinusObservation;
%
set(handles.excitRandGausTog,'BackgroundColor',[1 0.6 0.1]);
set(handles.excitRandSquareTog,'BackgroundColor',[1 0.6 0.1]);
set(handles.excitPlusMinusTog,'BackgroundColor',[1 0.6 0.1]);
set(handles.obsRandGausTog,'BackgroundColor',[1 0.6 0.1]);
set(handles.obsRandSquareTog,'BackgroundColor',[1 0.6 0.1]);
set(handles.obsPlusMinusTog,'BackgroundColor',[0 0.7 0]);

% --- Executes when selected object is changed in uipanel9.
function uipanel9_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in uipanel9 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function File_menu_Callback(hObject, eventdata, handles)
% hObject    handle to File_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function LoadOld_menu_Callback(hObject, eventdata, handles)
% hObject    handle to LoadOld_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.myFeedbackObject.isAcquiringData
    handles.myFeedbackObject.stopDataAcquisition();
end
% if strcmp(handles.myFeedbackObject.saveDataPath,'')
%     warndlg('Pleas specify the saveDataPath','Warning')
%     return
% end
[myFile, myPathName]=uigetfile([handles.myFeedbackObject.saveDataPath, '*.mat'],'Select your file');
if isequal(myFile, 0)
    return;
end
handles.myPathName=myPathName;
handles.myFiles=dir([handles.myPathName, '*.mat']);

handles.myFile=myFile;
if myFile
   refreshGUIfromFile(handles)
end
set(handles.forwardButton,'Enable','on')
set(handles.backButton,'Enable','on')
set(handles.send2HW,'Enable','on')
for i=1:length(handles.myFiles)
   if strcmp(handles.myFiles(i).name,handles.myFile)
       break
   end
end
handles.fileIndex=i;
set(handles.loadSlider,'Enable','on')
set(handles.loadSlider,'Min',1)
set(handles.loadSlider,'Max',length(handles.myFiles))
set(handles.loadSlider,'Value',i)

guidata(hObject,handles)

% --------------------------------------------------------------------
function SaveRM_menu_Callback(hObject, eventdata, handles)
% hObject    handle to SaveRM_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data = struct;
data.responseMatrix = handles.myFeedbackObject.mySolver.responseMatrix;
data.responseMatrixShape = handles.myFeedbackObject.mySolver.responseMatrixShape;
data.responseMatrixErrors = handles.myFeedbackObject.mySolver.responseMatrixErrors;
data.observablesLabels = handles.myFeedbackObject.observablesLabels;
data.excitatorsLabels = handles.myFeedbackObject.excitatorsLabels;

try
    uisave('data');
catch e
    errordlg(['Unable to save current Response Matrix: ',e.message],'Error');
end

% --------------------------------------------------------------------
function LoadRM_menu_Callback(hObject, eventdata, handles)
% hObject    handle to LoadRM_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[myFile, myPathName]=uigetfile('*.mat','Select .mat file with new Response Matrix.');
try
    myRMstruct = load(fullfile(myPathName, myFile));
    newRM = myRMstruct.data.responseMatrix;
    newRMerrors = myRMstruct.data.responseMatrixErrors;
    newRMshape = myRMstruct.data.responseMatrixShape;
    if ~isempty(newRM) && (sum(size(newRM) == size(handles.myFeedbackObject.mySolver.responseMatrix))==2)
        handles.myFeedbackObject.mySolver.responseMatrix = newRM;
        handles.myFeedbackObject.mySolver.responseMatrixErrors = newRMerrors;
        handles.myFeedbackObject.mySolver.responseMatrixShape = newRMshape;
    else
        errordlg('Found Response Matrix with wrong size. I will continue with the old one.','Error');
    end
catch e
    errordlg(['Unable to load new Response Matrix: ',e.message],'Error');
end


% --------------------------------------------------------------------
function verbose_menu_Callback(hObject, eventdata, handles)
% hObject    handle to verbose_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if isequal(get(hObject, 'Checked'),'on')
    handles.myFeedbackObject.myMonitor.verbose = false;
    handles.myFeedbackObject.myJapc.verbose = false;
    handles.myFeedbackObject.verbose = false;
    set(hObject, 'Checked', 'off')
else
    handles.myFeedbackObject.myMonitor.verbose = true;
    handles.myFeedbackObject.myJapc.verbose = true;
    handles.myFeedbackObject.verbose = true;
    set(hObject, 'Checked', 'on')
end

% --------------------------------------------------------------------
function Savedata_menu_Callback(hObject, eventdata, handles)
% hObject    handle to Savedata_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if isequal(get(hObject, 'Checked'),'on')
    handles.myFeedbackObject.saveData = false;
    set(hObject, 'Checked', 'off');
else
    folder_name = uigetdir(handles.myFeedbackObject.saveDataPath,'Select folder where to save data.');
    if folder_name ~= 0
        handles.myFeedbackObject.saveDataPath = folder_name;
        handles.myFeedbackObject.saveData = true;
        set(hObject, 'Checked', 'on');
    else
        handles.myFeedbackObject.saveData = false;
        set(hObject, 'Checked', 'off');
    end
end


% --- Executes on button press in useRefExcitationButton.
function useRefExcitationButton_Callback(hObject, eventdata, handles)
% hObject    handle to useRefExcitationButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.myFeedbackObject.setWantedExcitationFromReference();

% --------------------------------------------------------------------
function Untitled_1_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function ResetTraining_menu_Callback(hObject, eventdata, handles)
% hObject    handle to ResetTraining_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.myFeedbackObject.mySolver.resetTrainingHistory();


% --------------------------------------------------------------------
function ZeroMatrix_menu_Callback(hObject, eventdata, handles)
% hObject    handle to ZeroMatrix_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.myFeedbackObject.mySolver.responseMatrix(:,:) = 0;
handles.myFeedbackObject.mySolver.responseMatrixErrors(:,:) = 0;


% --------------------------------------------------------------------
function Untitled_2_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function tryGet_menu_Callback(hObject, eventdata, handles)
% hObject    handle to tryGet_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if isequal(get(hObject, 'Checked'),'on')
    handles.myFeedbackObject.myMonitor.isTryingToGetValues = false;
    set(hObject, 'Checked', 'off');
else
    handles.myFeedbackObject.myMonitor.isTryingToGetValues = true;
    set(hObject, 'Checked', 'on');
end


% --- Executes on button press in resetPreviousExcitationButton.
function resetPreviousExcitationButton_Callback(hObject, eventdata, handles)
% hObject    handle to resetPreviousExcitationButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ButtonName =questdlg('Should I reset the previous excitation?', ...
    'Are you sure?', ...
    'Yes', 'No', 'No');
switch ButtonName,
    case 'Yes'
        handles.myFeedbackObject.resetPreviousExcitatorStatus();
end


% --------------------------------------------------------------------
function Untitled_3_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function rmComparison_menu_Callback(hObject, eventdata, handles)
% hObject    handle to rmComparison_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% I want to open a new figure with difference 
    
localRM = handles.myFeedbackObject.mySolver.responseMatrix;
localRMErrors = handles.myFeedbackObject.mySolver.responseMatrixErrors;

try
    comparisonRM = handles.comparisonRM;
catch e
    comparisonRM = [];
end

if isempty(comparisonRM)
    errordlg('No comparison RM given. I''ll compare it with zero', 'Error');
    %comparisonRM = 2*randn(size(localRM));
    comparisonRM = zeros(size(localRM));
elseif sum(size(comparisonRM) == size(localRM)) ~= 2
    errordlg('The comparison Response Matrix now stored has wrong size', 'Error');
    comparisonRM = zeros(size(localRM));
end
figure
auxax = subplot(2,1,1);
auxLinesHandles = errorbar(auxax, localRM, localRMErrors);
% extract colors from 
auxColors = get(auxLinesHandles,'Color');
hold on
auxLinesHandles = plot(auxax,comparisonRM, '--');
% try to set same colors as before
try
    for i=1:length(auxLinesHandles)
        set(auxLinesHandles(i),'Color',auxColors{i});
    end
catch e
    disp('I could not set the same colors for compararison matrix :(');
end
hold off
set(auxax, 'FontSize',14)
xlabel(auxax,'Observables idx', 'FontSize',14)
ylabel(auxax,'Response matrix', 'FontSize',14)
title(auxax,'Current RM')

auxax = subplot(2,2,3);
imagesc(localRM - comparisonRM)
set(auxax, 'FontSize',14)
ylabel(auxax,'Observables idx', 'FontSize',14)
xlabel(auxax,'Actuator idx', 'FontSize',14)
title(auxax,'current RM - given RM')

auxax = subplot(2,2,4);
auxLinesHandles = plot(auxax,localRM - comparisonRM);
% try to set same colors as before
try
    for i=1:length(auxLinesHandles)
        set(auxLinesHandles(i),'Color',auxColors{i});
    end
catch e
    disp('I could not set the same colors for (current matrix) - (compararison matrix) :(');
end

set(auxax, 'FontSize',14)
xlabel(auxax,'Observables idx', 'FontSize',14)
ylabel(auxax,'\Delta Response matrix', 'FontSize',14)
title(auxax,'current RM - given RM')



% --------------------------------------------------------------------
function setRmComparison_menu_Callback(hObject, eventdata, handles)
% hObject    handle to setRmComparison_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[myFile, myPathName]=uigetfile('*.mat','Select .mat file with new Response Matrix to compare with.');
try
    auxRMstruct = load(fullfile(myPathName, myFile));
    auxRM = auxRMstruct.data.responseMatrix;
    
    if sum(size(auxRM) == size(handles.myFeedbackObject.mySolver.responseMatrix)) == 2
        handles.comparisonRM = auxRM;
        guidata(hObject, handles);
    else
        error('The selected file contains a RM of wrong size.')
    end
catch e
    errordlg(['Unable to load comparison Response Matrix: ',e.message],'Error');
end


% --------------------------------------------------------------------
function menuMatrixAxes1_Callback(hObject, eventdata, handles)
% hObject    handle to menuMatrixAxes1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function menuMatrixAxes2_Callback(hObject, eventdata, handles)
% hObject    handle to menuMatrixAxes2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function menuMatrixAxes3_Callback(hObject, eventdata, handles)
% hObject    handle to menuMatrixAxes3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function menuMatrixAxes_Callback(hObject, eventdata, handles)
% hObject    handle to menuMatrixAxes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on mouse press over axes background.
function SurfMatrixAxes_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to SurfMatrixAxes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
aux=get(hObject,'CurrentPoint');
auxIndexX = round(aux(1,1));
auxIndexY = round(aux(1,2));

% x
try
    auxLabelX = ['X(',num2str(auxIndexX),') ', handles.myFeedbackObject.excitatorsLabels{auxIndexX}];
catch e
    disp(['Impossible to set label for this X ', num2str(auxIndexX),':', e.message]);
    auxLabelX = ['X(',num2str(auxIndexX),')'];
end
% y
try
    auxLabelY = ['Y(',num2str(auxIndexY),') ', handles.myFeedbackObject.observablesLabels{auxIndexY}];
catch e
    disp(['Impossible to set label for this Y ', num2str(auxIndexY),':', e.message]);
    auxLabelY = ['Y(',num2str(auxIndexY),')'];
end
% value RM
try
    auxValue = handles.myFeedbackObject.mySolver.responseMatrix(auxIndexY, auxIndexX);
    auxLabelValue = ['Value = ', num2str(auxValue)];
catch e
    disp(['Impossible to set value for this point of RM: ', e.message]);
    auxLabelValue = 'Value = n.a.';
end
set(handles.menuMatrixAxes1, 'Label', auxLabelX);
set(handles.menuMatrixAxes2, 'Label', auxLabelY);
set(handles.menuMatrixAxes3, 'Label', auxLabelValue);


% --- Executes on mouse press over axes background.
function SurfHistoryAxes_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to SurfHistoryAxes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
aux=get(hObject,'CurrentPoint');
auxIndexX = round(aux(1,1));
auxIndexY = round(aux(1,2));

% x
auxLabelX = ['Acquisition # ',num2str(auxIndexX)];
% y
try
    auxLabelY = ['Y(',num2str(auxIndexY),') ', handles.myFeedbackObject.observablesLabels{auxIndexY}];
catch e
    disp(['Impossible to set label for this Y ', num2str(auxIndexY),':', e.message]);
    auxLabelY = ['Y(',num2str(auxIndexY),')'];
end
% value observation
try
    auxValue = handles.myFeedbackObject.observationsHistory(auxIndexY, auxIndexX);
    auxLabelValue = ['Value = ', num2str(auxValue)];
catch e
    disp(['Impossible to set value for this point of history: ', e.message]);
    auxLabelValue = 'Value = n.a.';
end
set(handles.menuMatrixAxes1, 'Label', auxLabelX);
set(handles.menuMatrixAxes2, 'Label', auxLabelY);
set(handles.menuMatrixAxes3, 'Label', auxLabelValue);


% --- Executes on mouse press over axes background.
function SurfHistoryExcitAxes_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to SurfHistoryExcitAxes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
aux=get(hObject,'CurrentPoint');
auxIndexX = round(aux(1,1));
auxIndexY = round(aux(1,2));

% x
auxLabelX = ['Acquisition # ',num2str(auxIndexX)];
% y
try
    auxLabelY = ['Y(',num2str(auxIndexY),') ', handles.myFeedbackObject.excitatorsLabels{auxIndexY}];
catch e
    disp(['Impossible to set label for this Y ', num2str(auxIndexY),':', e.message]);
    auxLabelY = ['Y(',num2str(auxIndexY),')'];
end
% value observation
try
    auxValue = handles.myFeedbackObject.observationsExcitationHistory(auxIndexY, auxIndexX);
    auxLabelValue = ['Value = ', num2str(auxValue)];
catch e
    disp(['Impossible to set value for this point of history: ', e.message]);
    auxLabelValue = 'Value = n.a.';
end
set(handles.menuMatrixAxes1, 'Label', auxLabelX);
set(handles.menuMatrixAxes2, 'Label', auxLabelY);
set(handles.menuMatrixAxes3, 'Label', auxLabelValue);





    
% --------------------------------------------------------------------
function randomPatternScan_menu_Callback(hObject, eventdata, handles)
% hObject    handle to randomPatternScan_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if isequal(get(hObject, 'Checked'),'on')
    % stop scanning random excitation pattern
    handles.myFeedbackObject.stopScanRandomPattern();
    set(hObject, 'Checked', 'off')
else
    % ask in a little GUI from where to where and how many acquisition per point.
    try
        prompt = {'Enter scan start idx:',...
            'Enter scan end idx:',...
            'Enter number of acquisition per point:',...
            };
        dlg_title = 'Random pattern scan settings';
        num_lines = 1;
        def = {'1',num2str(handles.myFeedbackObject.nExcitators),'3'};
        answer = inputdlg(prompt,dlg_title,num_lines,def);
        
        if ~isempty(answer)
            startIdx = round(str2double(answer{1}));
            stopIdx = round(str2double(answer{2}));
            nshotsPerPoint = round(str2double(answer{3}));
            if nshotsPerPoint < 1
                nshotsPerPoint = 1;
            end
            if startIdx < 1
                startIdx = 1;
            end
            if stopIdx > handles.myFeedbackObject.nExcitators
                stopIdx = handles.myFeedbackObject.nExcitators;
            end
            
            % if everything is ok, start it.
            handles.myFeedbackObject.startScanRandomPattern(nshotsPerPoint, startIdx, stopIdx);
            set(hObject, 'Checked', 'on')
        else
            handles.myFeedbackObject.stopScanRandomPattern();
            set(hObject, 'Checked', 'off')
        end
    catch e
        disp(['Impossible to start scanning random excitation pattern: ',e.message]);
        set(hObject, 'Checked', 'off')
    end
end

% --------------------------------------------------------------------
function Untitled_4_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function expertPanel_menu_Callback(hObject, eventdata, handles)
% hObject    handle to expertPanel_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if isa(handles.expertPanelOpeningFunction, 'function_handle')
    try
        handles.expertPanelHandler = handles.expertPanelOpeningFunction(handles.myFeedbackObject);
        % keep trace of panel handler
        guidata(handles.output, handles);
    catch e
        errordlg(['Impossible to execute expertPanel opening function: ', e.message]);
    end
else
    msgbox('No expertPanel opening function provided.', 'Warning');
end


% --- Executes on button press in autoResetPreviousTag.
function autoResetPreviousTag_Callback(hObject, eventdata, handles)
% hObject    handle to autoResetPreviousTag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of autoResetPreviousTag
if get(hObject,'Value')
    handles.myFeedbackObject.autoResetPreviousEnabled = true;
    set(hObject,'BackgroundColor',[0 0.7 0]);
else
    handles.myFeedbackObject.autoResetPreviousEnabled = false;
    set(hObject,'BackgroundColor',[1 0.6 0.1]);
end


% --------------------------------------------------------------------
function helpTag_Callback(hObject, eventdata, handles)
% hObject    handle to helpTag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function menuSlider_Callback(hObject, eventdata, handles)
% hObject    handle to menuSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function menuUpperLimits_Callback(hObject, eventdata, handles)
% hObject    handle to menuUpperLimits (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over regularizationWeightSlider.
function regularizationWeightSlider_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to regularizationWeightSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.menuUpperLimits, 'Callback',@(h,e)SetSliderUpLimit(h,e, hObject));
set(handles.menuSetValueSlider, 'Callback',@(h,e)SetSliderValue(h, e, hObject));


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over gainSlider.
function gainSlider_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to gainSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.menuUpperLimits, 'Callback',@(h,e)SetSliderUpLimit(h,e, hObject));
set(handles.menuSetValueSlider, 'Callback',@(h,e)SetSliderValue(h,e, hObject));


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over randomExcitationSlider.
function randomExcitationSlider_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to randomExcitationSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.menuUpperLimits, 'Callback',@(h,e)SetSliderUpLimit(h,e, hObject));
set(handles.menuSetValueSlider, 'Callback',@(h,e)SetSliderValue(h,e, hObject));


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over minCorrectionSlider.
function minCorrectionSlider_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to minCorrectionSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.menuUpperLimits, 'Callback',@(h,e)SetSliderUpLimit(h,e, hObject));
set(handles.menuSetValueSlider, 'Callback',@(h,e)SetSliderValue(h,e, hObject));


% --------------------------------------------------------------------
function onlineDoc_menu_Callback(hObject, eventdata, handles)
% hObject    handle to onlineDoc_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% open default browser to help page
web(handles.helpWebPage, '-browser')


% --------------------------------------------------------------------
function menuSetValueSlider_Callback(hObject, eventdata, handles)
% hObject    handle to menuSetValueSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
